﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.ResourceManagement
{
    [Table("HeadcountLog")]
    public class HeadcountRecord
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int HeadcountID { get; set; }

        //personal
        [StringLength(150)]
        public string FirstName { get; set; }
        [StringLength(150)]
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        [StringLength(200)]
        public string EMail { get; set; }
        [StringLength(50)]
        public string ContactNumber { get; set; }
        [StringLength(50)]
        public string CemexID { get; set; }
        [StringLength(100)]
        public string PayrollNumber { get; set; }
        [StringLength(150)]
        public string LoginName { get; set; }
        [StringLength(4000)]
        public string Skills { get; set; }

        //contract
        [StringLength(100)]
        public string ContractNumber { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public decimal? HourRate { get; set; }
        public decimal? MonthlyCostUSD { get; set; }
        public decimal? HoursPerDay { get; set; }
        [StringLength(150)]
        public string ResourceManagerLoginName { get; set; }
        public decimal? Rating { get; set; }

        //choices
        [StringLength(50)]
        public string ResourceStatus { get; set; }
        [StringLength(50)]
        public string ContractType { get; set; }
        [StringLength(50)]
        public string EmployeeType { get; set; }
        [StringLength(50)]
        public string HeadcountType { get; set; }
        [StringLength(50)]
        public string FinancialType { get; set; }

        //lookups
        public int? SourceCompanyID { get; set; }
        [StringLength(100)]
        public string SourceCompanyValue { get; set; }

        public int? HeadCountPositionID { get; set; }
        [StringLength(150)]
        public string HeadCountPositionValue { get; set; }

        public int? PitAreaID { get; set; }
        [StringLength(150)]
        public string PitAreaValue { get; set; }

        public int? RelatedRoleID { get; set; }
        [StringLength(150)]
        public string RelatedRoleValue { get; set; }

        public int? LocationCountryID { get; set; }
        [StringLength(50)]
        public string LocationCountryValue { get; set; }

        public int? SeniorityLevelID { get; set; }
        [StringLength(50)]
        public string SeniorityLevelValue { get; set; }    

        //Configuration
        public bool MustReportHours { get; set; }
        public bool MustReceiveNotifications { get; set; }

        [StringLength(150)]
        public string ModifiedBy { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime? ValidUntil { get; set; }

        //extra
        [StringLength(100)]
        public string ResourceAnexo { get; set; }
        [StringLength(100)]
        public string ResourceFCC { get; set; }
        [StringLength(100)]
        public string ResourceRITM { get; set; }
        [StringLength(100)]
        public string ResourceCNTR { get; set; }

    }
}
