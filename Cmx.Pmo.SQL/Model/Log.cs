﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model
{
    [Table("ServicesLog")]
    public class LogEntry
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int ServiceID { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public string Message { get; set; }
        public string AdditionalData { get; set; }
    }
}