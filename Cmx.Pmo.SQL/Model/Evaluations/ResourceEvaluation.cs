﻿using Cmx.Pmo.SQL.Enums.Evaluations;
using Cmx.Pmo.SQL.Model.Timesheet;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Evaluations
{
    [Table("ResourceEvaluations")]
    public class ResourceEvaluation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int AllocationListItemID { get; set; }
        public int HeadcountID { get; set; }
        public int? ProjectID { get; set; }
        public int? ProgramID { get; set; }
        public int? AreaID { get; set; }

        public int? Quantity { get; set; }
        public int? Quality { get; set; }
        public int? Knowhow { get; set; }
        public int? Relations { get; set; }
        public int? Cooperation { get; set; }
        public int? AtendanceReliability { get; set; }
        public int? InitiativeCreativity { get; set; }
        public int? Capacity { get; set; }
        public decimal Rating { get; set; }

        public string Comments { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime? LastEditedOn { get; set; }

        public DateTime? EvaluatedOn { get; set; }
        [StringLength(250)]
        public string Evaluator { get; set; }
        [StringLength(250)]
        public string EvaluatedBy { get; set; }
        [StringLength(250)]
        public string ReaderGroups { get; set; }
        public EvaluationStatus Status { get; set; }
    }

}
