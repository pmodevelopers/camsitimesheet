﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cmx.Pmo.SQL.Model.Ventures
{
    [Table("ProjectActivationTracks")]
    public class ProjectActivationTrack
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ItemId { get; set; }
        public string Name { get; set; }
        [Column(TypeName = "Date")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime? EndDate { get; set; }

        public virtual ProjectActivation Project { get; set; }

        public virtual ICollection<ProjectActivationMilestone> Milestones { get; set; }
    }
}

