﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cmx.Pmo.SQL.Model.Ventures
{
    [Table("ProjectActivationMilestones")]
    public class ProjectActivationMilestone
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ItemId { get; set; }
        public string Name { get; set; }
        public int? CountryID { get; set; }
        [Column(TypeName = "Date")]
        public DateTime? StartDate { get; set; }

        public virtual ProjectActivationTrack Track { get; set; }
    }
}

