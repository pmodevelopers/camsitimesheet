﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cmx.Pmo.SQL.Model.Ventures
{
    [Table("ProjectActivationBudgetItemYears")]
    public class ProjectActivationBudgetItemYear
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Year { get; set; }
        [NotMapped]
        public decimal[] Months { get; set; }
        public string MonthsString { get; set; }

        public virtual ProjectActivationBudgetItem BudgetItem { get; set; }
    }
}
