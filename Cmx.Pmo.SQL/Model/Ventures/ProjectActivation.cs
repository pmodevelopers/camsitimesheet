﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cmx.Pmo.SQL.Model.Ventures
{
    [Table("ProjectActivations")]
    public class ProjectActivation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string ProjectLongID { get; set; }
        public int? ProjectLeaderUserIdInWeb { get; set; }
        public int? ProjectManagerUserIdInWeb { get; set; }
        //mantis: add Status?

        public virtual ICollection<ProjectActivationTrack> Tracks { get; set; }
        public virtual ICollection<ProjectActivationDocument> Documents { get; set; }
        public virtual ICollection<ProjectActivationBudgetItem> BudgetItems { get; set; }
    }
}

