﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cmx.Pmo.SQL.Model.Ventures
{
    [Table("ProjectActivationBudgetItems")]
    public class ProjectActivationBudgetItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }

        public virtual ProjectActivation Project { get; set; }

        public virtual ICollection<ProjectActivationBudgetItemYear> Years { get; set; }
    }
}
