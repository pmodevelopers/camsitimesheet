﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cmx.Pmo.SQL.Model.Ventures
{
    [Table("ProjectActivationDocuments")]
    public class ProjectActivationDocument
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public byte[] Contents { get; set; }

        public virtual ProjectActivation Project { get; set; }
    }
}

