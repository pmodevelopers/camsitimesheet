﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    public class TimeTrackingWeekHours
    {
        public DateTime StartDate { get; set; }
        public decimal TotalHours { get; set; }
        public int HeadcountID { get; set; }
    }
}
