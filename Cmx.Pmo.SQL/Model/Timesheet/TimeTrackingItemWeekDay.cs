﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    public class TimeTrackingItemWeekDay
    {
        public int HeadcountID { get; set; }
        //public int AreaID { get; set; }
        public string Manager { get; set; }


        public int ItemDbID { get; set; }
        public TrackingType TrackingType { get; set; }
        public int ListItemID { get; set; }
        public int? ProgramID { get; set; }
        public int? ProjectID { get; set; }
        public int? AreaID { get; set; }
        public int? CountryID { get; set; }


        //public int TrackingItem_Id { get; set; }
        public int WeekDbID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedKey { get; set; }
        public string Comments { get; set; }
        public WeekStatus Status { get; set; }

        public int DayDbID { get; set; }
        //public int Week_Id { get; set; }
        public DateTime TrackedDate { get; set; }
        public decimal Hours { get; set; }
    }
}
