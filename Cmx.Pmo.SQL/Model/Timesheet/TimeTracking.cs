﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    [Table("TimeTracking")]
    public class TimeTracking
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public TrackingType TrackingType { get; set; }
        public int TrackingTypeID { get; set; }
        public int CountryID { get; set; }
        public int ProjectID { get; set; }
        public int HeadcountID { get; set; }
        public int ProjectName { get; set; }
        public int HeadcountName { get; set; }

        public virtual ICollection<TimeTrackingWeek> TimeTrackingWeeks { get; set; }

    }
}
