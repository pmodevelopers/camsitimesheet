﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    [Table("TimeTrackingHeadcount")]
    public class TimeTrackingHeadcount
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int HeadcountID { get; set; }
        public int? AreaID { get; set; }
        public string Manager { get; set; }
        public string FullName { get; set; }

        public virtual ICollection<TimeTrackingItem> TimeTrackingItems { get; set; }
        public virtual ICollection<TimeTrackingMissingWeek> TimeTrackingMissingWeeks { get; set; }
    }
}
