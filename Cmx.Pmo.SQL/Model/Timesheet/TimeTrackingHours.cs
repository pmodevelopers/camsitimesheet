﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    public class TimeTrackingHours
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public int HeadcountID { get; set; }
        //public int AreaID { get; set; }
        public int? ProjectID { get; set; }
        public int? AreaID { get; set; }
        public int? ProgramID { get; set; }
        public int? CountryID { get; set; }
        public decimal TotalHours { get; set; }
        public TrackingType TrackingType { get; set; }
        public int ListItemID { get; set; }
    }
}
