﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    [Table("TimeTrackingWeeks")]
    public class TimeTrackingWeek
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Column(TypeName = "Date")]
        public DateTime StartDate { get; set; }
        [Column(TypeName = "Date")]
        public DateTime EndDate { get; set; }        
        public string ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
        public WeekStatus Status { get; set; }
        [StringLength(150)]
        public string ModifiedKey { get; set; }
        public string Comments { get; set; }
        public string Approver { get; set; }
        public DateTime? ApprovedOn { get; set; }

        //public int TeamId { get; set; }
        //public string TeamName { get; set; }
        public string HeadcountType { get; set; }

        public virtual TimeTrackingItem TrackingItem { get; set; }

        public virtual ICollection<TimeTrackingDay> TimeTrackingDays { get; set; }
    }
}
