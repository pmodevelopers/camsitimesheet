﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using Cmx.Pmo.SQL.Model.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    [Table("TimeTrackingItems")]
    //project/area or service
    public class TimeTrackingItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public TrackingType TrackingType { get; set; }
        public int? ProgramID { get; set; }
        public int? ProjectID { get; set; }
        public int? AreaID { get; set; }
        public int ListItemID { get; set; }
        public int? CountryID { get; set; }

        //public int TeamId { get; set; }
        //public string TeamName { get; set; }
        public string HeadcountType { get; set; }

        public virtual TimeTrackingHeadcount Headcount { get; set; }

        public virtual ICollection<TimeTrackingWeek> TimeTrackingWeeks { get; set; }
        //public virtual ICollection<ResourceEvaluation> Evaluations { get; set; }
    }
}
