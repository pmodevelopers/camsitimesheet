﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    public class TimeTrackingMissingWeekItem
    {
        public int HeadcountID { get; set; }

        public int ItemDbID { get; set; }
        public TrackingType TrackingType { get; set; }
        public int ListItemID { get; set; }
        public int? CountryID { get; set; }

        public int WeekDbID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
