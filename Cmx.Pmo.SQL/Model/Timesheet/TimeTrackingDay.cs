﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    [Table("TimeTrackingDays")]
    public class TimeTrackingDay
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTime TrackedDate { get; set; }
        public decimal Hours { get; set; }

        public virtual TimeTrackingWeek Week { get; set; }
    }
}
