﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    public class TimeTrackingMissingWeekFilteredItem : TimeTrackingMissingWeekItem
    {
        public int? ItemDbIDFiltered { get; set; }
        public TrackingType? TrackingTypeFiltered { get; set; }
        public int? ListItemIDFiltered { get; set; }
    }
}
