﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Model.Timesheet
{
    public class HeadcountWeek
    {
        public int HeadcountID { get; set; }
        public DateTime StartDate { get; set; }
    }
}
