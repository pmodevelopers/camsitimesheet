﻿using Cmx.Pmo.SQL.Model;
using Cmx.Pmo.SQL.Model.Evaluations;
using Cmx.Pmo.SQL.Model.ResourceManagement;
using Cmx.Pmo.SQL.Model.Timesheet;
using Cmx.Pmo.SQL.Model.Ventures;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Context
{
    public class CmxPmoContext : DbContext
    {
        public CmxPmoContext()
            : base("CmxPmoAzureContextCAMSI")
        {
        }

        public CmxPmoContext(string connectionString)
            : base(connectionString)
        {
            //Database.SetInitializer(new CreateDatabaseIfNotExists<CmxPmoContext>());
        }

        public DbSet<LogEntry> Logs { get; set; }

        public DbSet<HeadcountRecord> HeadcountRecords { get; set; }

        public DbSet<TimeTrackingHeadcount> TimeTrackingHeadcounts { get; set; }
        public DbSet<TimeTrackingItem> TimeTrackingItems { get; set; }
        public DbSet<TimeTrackingWeek> TimeTrackingWeeks { get; set; }
        public DbSet<TimeTrackingDay> TimeTrackingDays { get; set; }
        public DbSet<TimeTrackingMissingWeek> TimeTrackingMissingWeeks { get; set; }

        public DbSet<ResourceEvaluation> Evaluations { get; set; }

        //activation
        public DbSet<ProjectActivation> ProjectActivations { get; set; }
        public DbSet<ProjectActivationDocument> ProjectActivationDocuments { get; set; }
        public DbSet<ProjectActivationBudgetItem> ProjectActivationBudgetItems { get; set; }
        public DbSet<ProjectActivationBudgetItemYear> ProjectActivationBudgetItemYears { get; set; }
        public DbSet<ProjectActivationTrack> ProjectActivationTracks { get; set; }
        public DbSet<ProjectActivationMilestone> ProjectActivationMilestones { get; set; }
    }
}
