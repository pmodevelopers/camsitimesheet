﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Enums.Timesheet
{
    public enum TrackingType
    {
        Allocation = 1,
        Service,
        NonProductive,
        Administrative,
        OrderTaking,
        DeliveryTracking,
        Others,
        CemexOnline,
        CrossTracks,
        AuthorizedInitiatives,
        Special
    }
}
