﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Enums.Timesheet
{
    public enum WeekStatus
    {
        Open = 1,
        Rejected,
        Submitted,
        Approved
    }
}
