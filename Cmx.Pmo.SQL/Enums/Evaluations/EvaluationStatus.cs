﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Enums.Evaluations
{
    public enum EvaluationStatus
    {
        Created = 1,
        Submitted
    }
}
