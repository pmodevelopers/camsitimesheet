﻿using Cmx.Pmo.SQL.Context;
using Cmx.Pmo.SQL.Model;
using Cmx.Pmo.SQL.Model.ResourceManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Repository
{
    public class HeadcountLogRepository
    {
        public void AddLogEntry(HeadcountRecord HeadcountLogEntry)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                db.HeadcountRecords.Add(HeadcountLogEntry);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void UpdatePreviousHeadcountLogEntry(int HeadcountID, DateTime ValidUntil)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                HeadcountRecord logItem = db.HeadcountRecords.Where(x => x.HeadcountID == HeadcountID && x.ValidUntil == null).FirstOrDefault();

                if (logItem != null)
                {
                    logItem.ValidUntil = ValidUntil;

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

    }
}
