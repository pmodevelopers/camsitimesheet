﻿using Cmx.Pmo.SQL.Context;
using Cmx.Pmo.SQL.Model.Ventures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Repository
{
    public class ProjectActivationRepository
    {
        public void SaveProjectActivation(ProjectActivation item)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                //if not exists, add first
                var itemOnDb = db.ProjectActivations.Where(x => x.ProjectLongID == item.ProjectLongID).FirstOrDefault();

                if (itemOnDb == null)
                {
                    db.ProjectActivations.Add(item);
                }
                else
                {
                    if (item.ProjectLeaderUserIdInWeb.HasValue)
                    {
                        itemOnDb.ProjectLeaderUserIdInWeb = item.ProjectLeaderUserIdInWeb;
                    }
                    if (item.ProjectManagerUserIdInWeb.HasValue)
                    {
                        itemOnDb.ProjectManagerUserIdInWeb = item.ProjectManagerUserIdInWeb;
                    }
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void SaveProjectActivationDocument(ProjectActivationDocument item, string ProjectLongID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                //if already exists, do nothing
                var itemOnDb = db.ProjectActivationDocuments.Where(x => x.Name == item.Name && x.Project.ProjectLongID == ProjectLongID).FirstOrDefault();

                if (itemOnDb == null)
                {
                    db.ProjectActivationDocuments.Add(item);

                    db.SaveChanges();
                }
                //else
                //{
                //    throw new Exception("Document already exists");
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void SaveProjectActivationTrack(ProjectActivationTrack item, string ProjectLongID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                //if not exists, add first
                var itemOnDb = db.ProjectActivationTracks.Where(x => x.ItemId == item.ItemId && x.Project.ProjectLongID == ProjectLongID).FirstOrDefault();

                if (itemOnDb == null)
                {
                    db.ProjectActivationTracks.Add(item);
                }
                else
                {
                    itemOnDb.Name = item.Name;
                    itemOnDb.StartDate = item.StartDate;
                    itemOnDb.EndDate = item.EndDate;
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void SaveProjectActivationTrackMilestone(ProjectActivationMilestone item, int TrackDbId)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                //if not exists, add first
                var itemOnDb = db.ProjectActivationMilestones.Where(x => x.ItemId == item.ItemId && item.Track.Id == TrackDbId).FirstOrDefault();

                if (itemOnDb == null)
                {
                    db.ProjectActivationMilestones.Add(item);
                }
                else
                {
                    itemOnDb.Name = item.Name;
                    itemOnDb.CountryID = item.CountryID;
                    itemOnDb.StartDate = item.StartDate;
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void SaveProjectActivationBudgetItem(ProjectActivationBudgetItem item, string ProjectLongID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                //if already exists, update
                var itemOnDb = db.ProjectActivationBudgetItems.Where(x => x.Type == item.Type && x.Name == item.Name && x.Project.ProjectLongID == ProjectLongID).FirstOrDefault();

                if (itemOnDb == null)
                {
                    db.ProjectActivationBudgetItems.Add(item);
                }
                else
                {
                    itemOnDb.Name = item.Name;
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void SaveProjectActivationBudgetItemYear(int year, string monthsString, string type, string name, string ProjectLongID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                //if not exists, do nothing
                var itemParentOnDb = db.ProjectActivationBudgetItems.Include("ProjectActivationBudgetItemYears").Where(x => x.Type == type && x.Name == name && x.Project.ProjectLongID == ProjectLongID).FirstOrDefault();

                if (itemParentOnDb != null)
                {
                    foreach (var itemYear in itemParentOnDb.Years)
                    {
                        if(itemYear.Year == year)
                        {
                            var itemOnDb = db.ProjectActivationBudgetItemYears.Where(x => x.Id == itemYear.Id).FirstOrDefault();
                            itemOnDb.MonthsString = monthsString;

                            db.SaveChanges();

                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }


    }
}
