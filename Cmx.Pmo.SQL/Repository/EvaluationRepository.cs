﻿using Cmx.Pmo.SQL.Context;
using Cmx.Pmo.SQL.Model.Evaluations;
using Cmx.Pmo.SQL.Model.Timesheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;

namespace Cmx.Pmo.SQL.Repository
{
    public class EvaluationRepository
    {
        public static int Add(ResourceEvaluation evaluation)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();
                db.Evaluations.Add(evaluation);
                db.SaveChanges();

                return evaluation.Id;
                /* var trackingItemResult = db.TimeTrackingItems.Where(x => x.ListItemID == allocationListItemID).FirstOrDefault();

                 if (trackingItemResult != null)
                 {
                     trackingItemResult.Evaluations.Add(evaluation);
                     db.SaveChanges();
                 }
                 else
                 {
                     throw new Exception(string.Format("Tracking Item not found. ListItemID: {0}", allocationListItemID));
                 }*/
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public List<ResourceEvaluationItemFromDB> GetEvaluations(string Evaluator)
        {
            var result = new List<ResourceEvaluationItemFromDB>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@Evaluator", Evaluator));

                result = db.Database.SqlQuery<ResourceEvaluationItemFromDB>("EXEC dbo.GetEvaluations @Evaluator", parameters.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public List<ResourceEvaluationItemFromDB> GetEvaluations(int? EvaluationID, string LoginName, DateTime? StartDate, DateTime? EndDate, int? HeadcountID = null, int[] ProgramID = null, int[] ProjectListItemID = null, int[] LocationID = null, int[] StatusID = null)
        {
            var result = new List<ResourceEvaluationItemFromDB>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(EvaluationID != null && EvaluationID.HasValue ? new SqlParameter("@EvaluationId", EvaluationID.Value) : new SqlParameter("@EvaluationId", SqlInt32.Null));
                parameters.Add(!string.IsNullOrEmpty(LoginName) ? new SqlParameter("@LoginName", LoginName) : new SqlParameter("@LoginName", SqlInt32.Null));
                parameters.Add(StartDate != null && StartDate.HasValue ? new SqlParameter("@StartDate", StartDate.Value) : new SqlParameter("@StartDate", SqlDateTime.Null));
                parameters.Add(EndDate != null && EndDate.HasValue ? new SqlParameter("@EndDate", EndDate.Value) : new SqlParameter("@EndDate", SqlDateTime.Null));
                parameters.Add(HeadcountID != null && HeadcountID.HasValue ? new SqlParameter("@HeadcountId", HeadcountID.Value) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                //parameters.Add(!string.IsNullOrEmpty(Manager) ? new SqlParameter("@Manager", Manager) : new SqlParameter("@Manager", SqlInt32.Null));

                //Program IDs
                var programIDsTable = new DataTable();
                programIDsTable.Columns.Add("col1", typeof(int));
                if (ProgramID != null)
                {
                    ProgramID.ToList().ForEach(x => programIDsTable.Rows.Add(x));
                }
                var programParameter = new SqlParameter("@ProgramIds", SqlDbType.Structured);
                programParameter.Value = programIDsTable;
                programParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(programParameter);

                //ProjectListItem IDs
                var projectIDsTable = new DataTable();
                projectIDsTable.Columns.Add("col1", typeof(int));
                if (ProjectListItemID != null)
                {
                    ProjectListItemID.ToList().ForEach(x => projectIDsTable.Rows.Add(x));
                }
                var projectParameter = new SqlParameter("@ProjectListItemIds", SqlDbType.Structured);
                projectParameter.Value = projectIDsTable;
                projectParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(projectParameter);

                //Location IDs
                var locationIDsTable = new DataTable();
                locationIDsTable.Columns.Add("col1", typeof(int));
                if (LocationID != null)
                {
                    LocationID.ToList().ForEach(x => locationIDsTable.Rows.Add(x));
                }
                var locationParameter = new SqlParameter("@LocationIds", SqlDbType.Structured);
                locationParameter.Value = locationIDsTable;
                locationParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(locationParameter);

                //Status IDs
                var statusIDsTable = new DataTable();
                statusIDsTable.Columns.Add("col1", typeof(int));
                if (StatusID != null)
                {
                    StatusID.ToList().ForEach(x => statusIDsTable.Rows.Add(x));
                }
                var statusParameter = new SqlParameter("@StatusIds", SqlDbType.Structured);
                statusParameter.Value = statusIDsTable;
                statusParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(statusParameter);

                result = db.Database.SqlQuery<ResourceEvaluationItemFromDB>("EXEC dbo.GetEvaluations @EvaluationId, @LoginName, @StartDate, @EndDate, @HeadcountId, @ProgramIds, @ProjectListItemIds, @LocationIds, @StatusIds", parameters.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public void UpdateEvaluation(int EvaluationID, int? Quantity, int? Quality, int? Knowhow, int? Relations, int? Cooperation, int? AtendanceReliability, int? InitiativeCreativity, int? Capacity, 
            string Comments, string EvaluatedBy)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                var evaluationItem = db.Evaluations.Where(x => x.Id == EvaluationID).FirstOrDefault();

                if (evaluationItem != null)
                {
                    if(evaluationItem.Status == Enums.Evaluations.EvaluationStatus.Submitted)
                    {
                        throw new Exception("Evaluation cannot be further edited.");
                    }

                    DateTime now = DateTime.Now;
                    evaluationItem.Quantity = Quantity;
                    evaluationItem.Quality = Quality;
                    evaluationItem.Knowhow = Knowhow;
                    evaluationItem.Relations = Relations;
                    evaluationItem.Cooperation = Cooperation;
                    evaluationItem.AtendanceReliability = AtendanceReliability;
                    evaluationItem.InitiativeCreativity = InitiativeCreativity;
                    evaluationItem.Capacity = Capacity;
                    evaluationItem.Comments = Comments;
                    evaluationItem.EvaluatedBy = EvaluatedBy;
                    evaluationItem.LastEditedOn = now;
                    evaluationItem.EvaluatedOn = now;

                    decimal rating = 0;
                    rating += Quantity != null && Quantity.HasValue ? Quantity.Value : 0;
                    rating += Quality != null && Quality.HasValue ? Quality.Value : 0;
                    rating += Knowhow != null && Knowhow.HasValue ? Knowhow.Value : 0;
                    rating += Relations != null && Relations.HasValue ? Relations.Value : 0;
                    rating += Cooperation != null && Cooperation.HasValue ? Cooperation.Value : 0;
                    rating += AtendanceReliability != null && AtendanceReliability.HasValue ? AtendanceReliability.Value : 0;
                    rating += InitiativeCreativity != null && InitiativeCreativity.HasValue ? InitiativeCreativity.Value : 0;
                    rating += Capacity != null && Capacity.HasValue ? Capacity.Value : 0;

                    evaluationItem.Rating = rating / 8m;

                    evaluationItem.Status = Enums.Evaluations.EvaluationStatus.Submitted;

                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("Evaluation not found.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }


    }
}