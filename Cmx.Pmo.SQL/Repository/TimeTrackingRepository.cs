﻿using Cmx.Pmo.SQL.Context;
using Cmx.Pmo.SQL.Model.Timesheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;

namespace Cmx.Pmo.SQL.Repository
{
    public class TimeTrackingRepository
    {
        public void AddHeadcount(TimeTrackingHeadcount Headcount)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                db.TimeTrackingHeadcounts.Add(Headcount);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void AddTrackingItem(TimeTrackingItem trackingItem, int headcountID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                var headcountResult = db.TimeTrackingHeadcounts.Where(x => x.HeadcountID == headcountID).FirstOrDefault();

                if (headcountResult != null)
                {
                    headcountResult.TimeTrackingItems.Add(trackingItem);
                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("Headcount not found.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void AddTrackingItemWeek(TimeTrackingWeek week, int trackingItemID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                var trackingItemResult = db.TimeTrackingItems.Where(x => x.Id == trackingItemID).FirstOrDefault();

                if (trackingItemResult != null)
                {
                    trackingItemResult.TimeTrackingWeeks.Add(week);
                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("Tracking Item not found.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void AddMissingWeek(TimeTrackingMissingWeek MissingWeek, int HeadcountID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                var headcountItemResult = db.TimeTrackingHeadcounts.Where(x => x.HeadcountID == HeadcountID).FirstOrDefault();

                if (headcountItemResult != null)
                {
                    headcountItemResult.TimeTrackingMissingWeeks.Add(MissingWeek);
                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("Headcount Item not found.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void UpdateTrackingItemWeek(TimeTrackingWeek updatedWeek)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                var weekResult = db.TimeTrackingWeeks.Where(w => w.Id == updatedWeek.Id).FirstOrDefault();
                if (weekResult != null)
                {
                    weekResult.Modified = updatedWeek.Modified;
                    weekResult.ModifiedBy = updatedWeek.ModifiedBy;
                    weekResult.Status = updatedWeek.Status;
                    weekResult.ModifiedKey = updatedWeek.ModifiedKey;
                    weekResult.Comments = updatedWeek.Comments;

                    for (int z = 0; z < weekResult.TimeTrackingDays.Count; z++)
                    {
                        weekResult.TimeTrackingDays.ElementAt(z).Hours = updatedWeek.TimeTrackingDays.ElementAt(z).Hours;
                    }

                    db.SaveChanges();
                }
                else
                {
                    throw new Exception("TimeTrackingWeek not found.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void DeleteTrackingItemWeek(int trackingItemWeekID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                var weekResult = db.TimeTrackingWeeks.Include("TrackingItem").Where(w => w.Id == trackingItemWeekID).FirstOrDefault();

                //its only possible to delete preliminary data
                if (weekResult != null && (weekResult.Status == Enums.Timesheet.WeekStatus.Open || weekResult.Status == Enums.Timesheet.WeekStatus.Rejected))
                {
                    int TrackingItemDbId = weekResult.TrackingItem.Id;

                    db.TimeTrackingWeeks.Remove(weekResult);
                    db.SaveChanges();

                    //remove tracking item if it has no weeks left, regarless of weeks status
                    var weeksResult = db.TimeTrackingWeeks.Where(w => w.TrackingItem.Id == TrackingItemDbId).Count();
                    if(weeksResult == 0)
                    {
                        var trackingItemResult = db.TimeTrackingItems.Where(w => w.Id == TrackingItemDbId).FirstOrDefault();

                        if (trackingItemResult != null)
                        {
                            db.TimeTrackingItems.Remove(trackingItemResult);
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    throw new Exception("TimeTrackingWeek not found.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public TimeTrackingHeadcount GetHeadcount(int HeadcountID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                var headcountResult = db.TimeTrackingHeadcounts.Where(x => x.HeadcountID == HeadcountID).FirstOrDefault();

                if (headcountResult != null)
                {
                    return headcountResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return null;
        }

        public void UpdateHeadcount(int HeadcountID, string Manager, string FullName, int? AreaID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                var headcountResult = db.TimeTrackingHeadcounts.Where(x => x.HeadcountID == HeadcountID).FirstOrDefault();

                if (headcountResult != null)
                {
                    headcountResult.Manager = Manager;
                    headcountResult.FullName = FullName;
                    headcountResult.AreaID = AreaID;

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public TimeTrackingItem GetTrackingItem(int HeadcountID, int TrackingItemID, int? CountryID)
        {
            CmxPmoContext db = null;
            TimeTrackingItem trackingItemResult = null;

            try
            {
                db = new CmxPmoContext();


                if (CountryID != null && CountryID.HasValue)
                {
                    trackingItemResult = db.TimeTrackingItems.Include("TimeTrackingWeeks.TimeTrackingDays").Where(x =>
                        x.Headcount.HeadcountID == HeadcountID && x.ListItemID == TrackingItemID && x.CountryID == CountryID).FirstOrDefault();
                }
                else
                {
                    trackingItemResult = db.TimeTrackingItems.Include("TimeTrackingWeeks.TimeTrackingDays").Where(x =>
                        x.Headcount.HeadcountID == HeadcountID && x.ListItemID == TrackingItemID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return trackingItemResult;
        }

        public TimeTrackingItem GetTrackingItem(int HeadcountID, int ProjectID)
        {
            CmxPmoContext db = null;
            TimeTrackingItem trackingItemResult = null;

            try
            {
                db = new CmxPmoContext();

                trackingItemResult = db.TimeTrackingItems.Where(x => x.Headcount.HeadcountID == HeadcountID && x.ProjectID == ProjectID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return trackingItemResult;
        }

        public List<TimeTrackingItemWeekDay> GetWeek(int HeadcountID, DateTime StartDate)
        {
            List<TimeTrackingItemWeekDay> result = new List<TimeTrackingItemWeekDay>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                result = db.Database.SqlQuery<TimeTrackingItemWeekDay>("EXEC dbo.GetHeadcountWeek @HeadcountId, @StartDate",
                    new SqlParameter("HeadcountId", HeadcountID),
                    new SqlParameter("StartDate", StartDate)
                    ).ToList<TimeTrackingItemWeekDay>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public List<TimeTrackingItemWeekDay> GetWeeks(DateTime? StartDate, DateTime? EndDate, int? HeadcountID = null, int[] ProgramID = null, int[] ProjectListItemID = null, int[] LocationID = null, int[] StatusID = null, string Manager = null)
        {
            List<TimeTrackingItemWeekDay> result = new List<TimeTrackingItemWeekDay>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(StartDate != null && StartDate.HasValue ? new SqlParameter("@StartDate", StartDate.Value) : new SqlParameter("@StartDate", SqlDateTime.Null));
                parameters.Add(EndDate != null && EndDate.HasValue ? new SqlParameter("@EndDate", EndDate.Value) : new SqlParameter("@EndDate", SqlDateTime.Null));
                //parameters.Add(new SqlParameter("@StartDate", StartDate));
                //parameters.Add(new SqlParameter("@EndDate", EndDate));
                parameters.Add(HeadcountID != null && HeadcountID.HasValue ? new SqlParameter("@HeadcountId", HeadcountID.Value) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                parameters.Add(!string.IsNullOrEmpty(Manager) ? new SqlParameter("@Manager", Manager) : new SqlParameter("@Manager", SqlInt32.Null));

                //Program IDs
                var programIDsTable = new DataTable();
                programIDsTable.Columns.Add("col1", typeof(int));
                if (ProgramID != null)
                {
                    ProgramID.ToList().ForEach(x => programIDsTable.Rows.Add(x));
                }
                var programParameter = new SqlParameter("@ProgramIds", SqlDbType.Structured);
                programParameter.Value = programIDsTable;
                programParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(programParameter);

                //ProjectListItem IDs
                var projectIDsTable = new DataTable();
                projectIDsTable.Columns.Add("col1", typeof(int));
                if (ProjectListItemID != null)
                {
                    ProjectListItemID.ToList().ForEach(x => projectIDsTable.Rows.Add(x));
                }
                var projectParameter = new SqlParameter("@ProjectListItemIds", SqlDbType.Structured);
                projectParameter.Value = projectIDsTable;
                projectParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(projectParameter);

                //Location IDs
                var locationIDsTable = new DataTable();
                locationIDsTable.Columns.Add("col1", typeof(int));
                if (LocationID != null)
                {
                    LocationID.ToList().ForEach(x => locationIDsTable.Rows.Add(x));
                }
                var locationParameter = new SqlParameter("@LocationIds", SqlDbType.Structured);
                locationParameter.Value = locationIDsTable;
                locationParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(locationParameter);

                //Status IDs
                var statusIDsTable = new DataTable();
                statusIDsTable.Columns.Add("col1", typeof(int));
                if (StatusID != null)
                {
                    StatusID.ToList().ForEach(x => statusIDsTable.Rows.Add(x));
                }
                var statusParameter = new SqlParameter("@StatusIds", SqlDbType.Structured);
                statusParameter.Value = statusIDsTable;
                statusParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(statusParameter);

                result = db.Database.SqlQuery<TimeTrackingItemWeekDay>("EXEC dbo.GetHeadcountWeeks @StartDate, @EndDate, @HeadcountId,  @Manager, @ProgramIds, @ProjectListItemIds, @LocationIds, @StatusIds", parameters.ToArray()).ToList<TimeTrackingItemWeekDay>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        //SP does not joins TrackingItemsTable
        public List<TimeTrackingMissingWeekItem> GetMissingWeeks(DateTime? StartDate, DateTime? EndDate, int? HeadcountID = null, string Manager = null)
        {
            List<TimeTrackingMissingWeekItem> result = new List<TimeTrackingMissingWeekItem>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(StartDate != null && StartDate.HasValue ? new SqlParameter("@StartDate", StartDate.Value) : new SqlParameter("@StartDate", SqlDateTime.Null));
                parameters.Add(EndDate != null && EndDate.HasValue ? new SqlParameter("@EndDate", EndDate.Value) : new SqlParameter("@EndDate", SqlDateTime.Null));
                parameters.Add(HeadcountID != null && HeadcountID.HasValue ? new SqlParameter("@HeadcountId", HeadcountID.Value) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                parameters.Add(!string.IsNullOrEmpty(Manager) ? new SqlParameter("@Manager", Manager) : new SqlParameter("@Manager", SqlInt32.Null));

                result = db.Database.SqlQuery<TimeTrackingMissingWeekItem>("EXEC dbo.GetMissingWeeks @StartDate, @EndDate, @HeadcountId,  @Manager", parameters.ToArray()).ToList<TimeTrackingMissingWeekItem>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public List<TimeTrackingMissingWeekFilteredItem> GetFilteredMissingWeeks(DateTime? StartDate, DateTime? EndDate, int? HeadcountID = null, int[] ProgramID = null, int[] ProjectListItemID = null, int[] LocationID = null, string Manager = null)
        {
            List<TimeTrackingMissingWeekFilteredItem> result = new List<TimeTrackingMissingWeekFilteredItem>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(StartDate != null && StartDate.HasValue ? new SqlParameter("@StartDate", StartDate.Value) : new SqlParameter("@StartDate", SqlDateTime.Null));
                parameters.Add(EndDate != null && EndDate.HasValue ? new SqlParameter("@EndDate", EndDate.Value) : new SqlParameter("@EndDate", SqlDateTime.Null));
                parameters.Add(HeadcountID != null && HeadcountID.HasValue ? new SqlParameter("@HeadcountId", HeadcountID.Value) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                parameters.Add(!string.IsNullOrEmpty(Manager) ? new SqlParameter("@Manager", Manager) : new SqlParameter("@Manager", SqlInt32.Null));
                //parameters.Add(new SqlParameter("@Manager", SqlInt32.Null));//mantis temp

                //Program IDs
                var programIDsTable = new DataTable();
                programIDsTable.Columns.Add("col1", typeof(int));
                if (ProgramID != null)
                {
                    ProgramID.ToList().ForEach(x => programIDsTable.Rows.Add(x));
                }
                var programParameter = new SqlParameter("@ProgramIds", SqlDbType.Structured);
                programParameter.Value = programIDsTable;
                programParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(programParameter);

                //ProjectListItem IDs
                var projectIDsTable = new DataTable();
                projectIDsTable.Columns.Add("col1", typeof(int));
                if (ProjectListItemID != null)
                {
                    ProjectListItemID.ToList().ForEach(x => projectIDsTable.Rows.Add(x));
                }
                var projectParameter = new SqlParameter("@ProjectListItemIds", SqlDbType.Structured);
                projectParameter.Value = projectIDsTable;
                projectParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(projectParameter);

                //Location IDs
                var locationIDsTable = new DataTable();
                locationIDsTable.Columns.Add("col1", typeof(int));
                if (LocationID != null)
                {
                    LocationID.ToList().ForEach(x => locationIDsTable.Rows.Add(x));
                }
                var locationParameter = new SqlParameter("@LocationIds", SqlDbType.Structured);
                locationParameter.Value = locationIDsTable;
                locationParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(locationParameter);

                result = db.Database.SqlQuery<TimeTrackingMissingWeekFilteredItem>("EXEC dbo.GetMissingWeeksForFilter @StartDate, @EndDate, @HeadcountId,  @Manager, @ProgramIds, @ProjectListItemIds, @LocationIds", parameters.ToArray()).ToList<TimeTrackingMissingWeekFilteredItem>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public List<TimeTrackingMissingWeekItem> GetLastSubmittedWeek(int HeadcountID)
        {
            List<TimeTrackingMissingWeekItem> result = new List<TimeTrackingMissingWeekItem>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@HeadcountId", HeadcountID));

                result = db.Database.SqlQuery<TimeTrackingMissingWeekItem>("EXEC dbo.GetLastSubmittedWeek @HeadcountId", parameters.ToArray()).ToList<TimeTrackingMissingWeekItem>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public TimeTrackingWeek GetPopulateWeek(int HeadcountID, DateTime StartDate)
        {
            TimeTrackingWeek result = new TimeTrackingWeek();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                result = db.Database.SqlQuery<TimeTrackingWeek>("EXEC dbo.GetHeadcountPopulateWeek @HeadcountId, @StartDate",
                    new SqlParameter("HeadcountId", HeadcountID),
                    new SqlParameter("StartDate", StartDate)).ToList<TimeTrackingWeek>().FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public List<TimeTrackingHours> GetHeadcountHours(int HeadcountID, int Year)
        {
            List<TimeTrackingHours> result = new List<TimeTrackingHours>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                result = db.Database.SqlQuery<TimeTrackingHours>("EXEC dbo.GetHeadcountHrsByYear @HeadcountId, @Year",
                    new SqlParameter("HeadcountId", HeadcountID),
                    new SqlParameter("Year", Year)).ToList<TimeTrackingHours>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        //Not in use as of Oct24,2018
        public List<TimeTrackingHours> GetHours(int Year, int? HeadcountID, int? ProgramID, int? ProjectListItemID)
        {
            List<TimeTrackingHours> result = new List<TimeTrackingHours>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List <object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@Year", Year));
                parameters.Add(HeadcountID.HasValue ? new SqlParameter("@HeadcountId", HeadcountID.Value) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                parameters.Add(ProgramID.HasValue ? new SqlParameter("@ProgramId", ProgramID.Value) : new SqlParameter("@ProgramId", SqlInt32.Null));
                parameters.Add(ProjectListItemID.HasValue ? new SqlParameter("@ProjectListItemId", ProjectListItemID.Value) : new SqlParameter("@ProjectListItemId", SqlInt32.Null));

                result = db.Database.SqlQuery<TimeTrackingHours>("EXEC dbo.GetHours @Year, @HeadcountId, @ProgramId, @ProjectListItemId", parameters.ToArray()).ToList<TimeTrackingHours>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        //Not in use as of Oct24,2018
        public List<TimeTrackingHours> GetHoursByDates(DateTime StartDate, DateTime EndDate, int? HeadcountID, int? ProgramID, int? ProjectListItemID)
        {
            List<TimeTrackingHours> result = new List<TimeTrackingHours>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@EndDate", EndDate));
                parameters.Add(HeadcountID.HasValue ? new SqlParameter("@HeadcountId", HeadcountID.Value) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                parameters.Add(ProgramID.HasValue ? new SqlParameter("@ProgramId", ProgramID.Value) : new SqlParameter("@ProgramId", SqlInt32.Null));
                parameters.Add(ProjectListItemID.HasValue ? new SqlParameter("@ProjectListItemId", ProjectListItemID.Value) : new SqlParameter("@ProjectListItemId", SqlInt32.Null));

                result = db.Database.SqlQuery<TimeTrackingHours>("EXEC dbo.GetHoursByDates @StartDate, @EndDate, @HeadcountId, @ProgramId, @ProjectListItemId", parameters.ToArray()).ToList<TimeTrackingHours>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        //Not in use as of Oct24,2018
        public List<TimeTrackingWeekHours> GetHoursByWeek(DateTime StartDate, DateTime EndDate, int? HeadcountID, int[] ProgramID, int[] ProjectListItemID, int[] LocationID)
        {
            List<TimeTrackingWeekHours> result = new List<TimeTrackingWeekHours>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@EndDate", EndDate));
                //parameters.Add(HeadcountID.HasValue ? new SqlParameter("@HeadcountId", HeadcountID.Value) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                //parameters.Add(ProgramID.HasValue ? new SqlParameter("@ProgramId", ProgramID.Value) : new SqlParameter("@ProgramId", SqlInt32.Null));
                //parameters.Add(ProjectListItemID.HasValue ? new SqlParameter("@ProjectListItemId", ProjectListItemID.Value) : new SqlParameter("@ProjectListItemId", SqlInt32.Null));

                result = db.Database.SqlQuery<TimeTrackingWeekHours>("EXEC dbo.GetHoursByWeek @StartDate, @EndDate, @HeadcountId, @ProgramId, @ProjectListItemId", parameters.ToArray()).ToList<TimeTrackingWeekHours>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }        

        public List<TimeTrackingHours> GetHoursByDatesRange(DateTime StartDate, DateTime EndDate, int HeadcountID, int Status, int[] ProgramID, int[] ProjectListItemID, int[] LocationID)
        {
            List<TimeTrackingHours> result = new List<TimeTrackingHours>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@EndDate", EndDate));
                parameters.Add(HeadcountID > 0 ? new SqlParameter("@HeadcountId", HeadcountID) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                parameters.Add(new SqlParameter("@Status", Status));
                //Program IDs
                var programIDsTable = new DataTable();
                programIDsTable.Columns.Add("col1", typeof(int));
                if (ProgramID != null)
                {
                    ProgramID.ToList().ForEach(x => programIDsTable.Rows.Add(x));
                }
                var programParameter = new SqlParameter("@ProgramIds", SqlDbType.Structured);
                programParameter.Value = programIDsTable;
                programParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(programParameter);

                //ProjectListItem IDs
                var projectIDsTable = new DataTable();
                projectIDsTable.Columns.Add("col1", typeof(int));
                if (ProjectListItemID != null)
                {
                    ProjectListItemID.ToList().ForEach(x => projectIDsTable.Rows.Add(x));
                }
                var projectParameter = new SqlParameter("@ProjectListItemIds", SqlDbType.Structured);
                projectParameter.Value = projectIDsTable;
                projectParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(projectParameter);

                //Location IDs
                var locationIDsTable = new DataTable();
                locationIDsTable.Columns.Add("col1", typeof(int));
                if (LocationID != null)
                {
                    LocationID.ToList().ForEach(x => locationIDsTable.Rows.Add(x));
                }
                var locationParameter = new SqlParameter("@LocationIds", SqlDbType.Structured);
                locationParameter.Value = locationIDsTable;
                locationParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(locationParameter);

                result = db.Database.SqlQuery<TimeTrackingHours>("EXEC dbo.GetHoursByDatesRange @StartDate, @EndDate, @HeadcountId, @Status, @ProgramIds, @ProjectListItemIds, @LocationIds", parameters.ToArray()).ToList<TimeTrackingHours>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public List<TimeTrackingItemWeekDay> GetHoursPerDayByDatesRange(DateTime StartDate, DateTime EndDate, int HeadcountID, int[] ProgramID, int[] ProjectListItemID, int[] LocationID)
        {
            List<TimeTrackingItemWeekDay> result = new List<TimeTrackingItemWeekDay>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@EndDate", EndDate));
                parameters.Add(HeadcountID > 0 ? new SqlParameter("@HeadcountId", HeadcountID) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                //Program IDs
                var programIDsTable = new DataTable();
                programIDsTable.Columns.Add("col1", typeof(int));
                if (ProgramID != null)
                {
                    ProgramID.ToList().ForEach(x => programIDsTable.Rows.Add(x));
                }
                var programParameter = new SqlParameter("@ProgramIds", SqlDbType.Structured);
                programParameter.Value = programIDsTable;
                programParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(programParameter);

                //ProjectListItem IDs
                var projectIDsTable = new DataTable();
                projectIDsTable.Columns.Add("col1", typeof(int));
                if (ProjectListItemID != null)
                {
                    ProjectListItemID.ToList().ForEach(x => projectIDsTable.Rows.Add(x));
                }
                var projectParameter = new SqlParameter("@ProjectListItemIds", SqlDbType.Structured);
                projectParameter.Value = projectIDsTable;
                projectParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(projectParameter);

                //Location IDs
                var locationIDsTable = new DataTable();
                locationIDsTable.Columns.Add("col1", typeof(int));
                if (LocationID != null)
                {
                    LocationID.ToList().ForEach(x => locationIDsTable.Rows.Add(x));
                }
                var locationParameter = new SqlParameter("@LocationIds", SqlDbType.Structured);
                locationParameter.Value = locationIDsTable;
                locationParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(locationParameter);

                //SP narrows results to Status 4
                result = db.Database.SqlQuery<TimeTrackingItemWeekDay>("EXEC dbo.GetHoursPerDayByDatesRange @StartDate, @EndDate, @HeadcountId, @ProgramIds, @ProjectListItemIds, @LocationIds", parameters.ToArray()).ToList<TimeTrackingItemWeekDay>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public List<TimeTrackingWeekHours> GetHoursByWeekRange(DateTime StartDate, DateTime EndDate, int HeadcountID, int[] ProgramID, int[] ProjectListItemID, int[] LocationID)
        {
            List<TimeTrackingWeekHours> result = new List<TimeTrackingWeekHours>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@EndDate", EndDate));
                parameters.Add(HeadcountID > 0 ? new SqlParameter("@HeadcountId", HeadcountID) : new SqlParameter("@HeadcountId", SqlInt32.Null));
                //Program IDs
                var programIDsTable = new DataTable();
                programIDsTable.Columns.Add("col1", typeof(int));
                if (ProgramID != null)
                {
                    ProgramID.ToList().ForEach(x => programIDsTable.Rows.Add(x));
                }
                var programParameter = new SqlParameter("@ProgramIds", SqlDbType.Structured);
                programParameter.Value = programIDsTable;
                programParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(programParameter);

                //ProjectListItem IDs
                var projectIDsTable = new DataTable();
                projectIDsTable.Columns.Add("col1", typeof(int));
                if (ProjectListItemID != null)
                {
                    ProjectListItemID.ToList().ForEach(x => projectIDsTable.Rows.Add(x));
                }
                var projectParameter = new SqlParameter("@ProjectListItemIds", SqlDbType.Structured);
                projectParameter.Value = projectIDsTable;
                projectParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(projectParameter);

                //Location IDs
                var locationIDsTable = new DataTable();
                locationIDsTable.Columns.Add("col1", typeof(int));
                if (LocationID != null)
                {
                    LocationID.ToList().ForEach(x => locationIDsTable.Rows.Add(x));
                }
                var locationParameter = new SqlParameter("@LocationIds", SqlDbType.Structured);
                locationParameter.Value = locationIDsTable;
                locationParameter.TypeName = "dbo.CmxIntArray";
                parameters.Add(locationParameter);

                result = db.Database.SqlQuery<TimeTrackingWeekHours>("EXEC dbo.GetHoursByWeekRange @StartDate, @EndDate, @HeadcountId, @ProgramIds, @ProjectListItemIds, @LocationIds", parameters.ToArray()).ToList<TimeTrackingWeekHours>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public List<TimeTrackingWeekHours> GetAllHoursPerWeek(DateTime StartDate, DateTime EndDate, int AreaID = 0)
        {
            List<TimeTrackingWeekHours> result = new List<TimeTrackingWeekHours>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@EndDate", EndDate));
                parameters.Add(AreaID > 0 ? new SqlParameter("@AreaID", AreaID) : new SqlParameter("@AreaID", SqlInt32.Null));

                result = db.Database.SqlQuery<TimeTrackingWeekHours>("EXEC dbo.GetAllHoursPerWeek @StartDate, @EndDate, @AreaID", parameters.ToArray()).ToList<TimeTrackingWeekHours>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public List<TimeTrackingHours> GetAllHoursPerMonth(DateTime StartDate, DateTime EndDate, int AreaID = 0)
        {
            List<TimeTrackingHours> result = new List<TimeTrackingHours>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@EndDate", EndDate));
                parameters.Add(AreaID > 0 ? new SqlParameter("@AreaID", AreaID) : new SqlParameter("@AreaID", SqlInt32.Null));

                result = db.Database.SqlQuery<TimeTrackingHours>("EXEC dbo.GetAllHoursPerMonth @StartDate, @EndDate, @AreaID", parameters.ToArray()).ToList<TimeTrackingHours>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public void SetWeekStatus(int HeadcountID, DateTime StartDate, int Status, string Approver, string Comments)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@HeadcountId", HeadcountID));
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@Status", Status));
                parameters.Add(new SqlParameter("@Approver", Approver));
                parameters.Add(!string.IsNullOrEmpty(Comments) ? new SqlParameter("@Comments", Comments) : new SqlParameter("@Comments", SqlInt32.Null));

                db.Database.ExecuteSqlCommand("EXEC dbo.SetWeekStatus @HeadcountId, @StartDate, @Status, @Approver, @Comments", parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void SetOnlyWeekStatus(int HeadcountID, DateTime StartDate, int Status)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@HeadcountId", HeadcountID));
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@Status", Status));

                db.Database.ExecuteSqlCommand("EXEC dbo.SetOnlyWeekStatus @HeadcountId, @StartDate, @Status", parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public List<HeadcountWeek> GetSubmittedWeeks(int HeadcountID, DateTime StartDate, DateTime EndDate)
        {
            List<HeadcountWeek> result = new List<HeadcountWeek>();
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@StartDate", StartDate));
                parameters.Add(new SqlParameter("@EndDate", EndDate));
                parameters.Add(new SqlParameter("@HeadcountId", HeadcountID));

                result = db.Database.SqlQuery<HeadcountWeek>("EXEC dbo.GetHeadcountSubmittedWeeks @StartDate, @EndDate, @HeadcountId", parameters.ToArray()).ToList<HeadcountWeek>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return result;
        }

        public void DeleteMissingWeeks(int HeadcountID)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@HeadcountId", HeadcountID));

                db.Database.ExecuteSqlCommand("EXEC dbo.DeleteMissingWeeks @HeadcountId", parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void DeleteAllMissingWeeks()
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                db.Database.ExecuteSqlCommand("EXEC dbo.DeleteAllMissingWeeks");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        public void SetTeamId(int weekDbId, int teamId)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                List<object> parameters = new List<object>();
                parameters.Add(new SqlParameter("@weekDbId", weekDbId));
                parameters.Add(new SqlParameter("@teamId", teamId));

                db.Database.ExecuteSqlCommand("EXEC dbo.SetTeamId @weekDbId, @teamId", parameters.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

    }
}
