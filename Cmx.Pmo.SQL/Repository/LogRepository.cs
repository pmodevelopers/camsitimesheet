﻿using Cmx.Pmo.SQL.Context;
using Cmx.Pmo.SQL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Repository
{
    public class LogRepository
    {
        public static void AddLogEntry(LogEntry LogEntry)
        {
            CmxPmoContext db = null;

            try
            {
                db = new CmxPmoContext();

                db.Logs.Add(LogEntry);

                db.SaveChanges();
            }
            catch (Exception ex)
            {
                //throw ex;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }
    }
}
