﻿USE [USCLDPMODBP01]
GO
/****** Object:  UserDefinedTableType [dbo].[CmxIntArray]    Script Date: 4/30/2019 1:18:47 PM ******/
CREATE TYPE [dbo].[CmxIntArray] AS TABLE(
	[col1] [int] NOT NULL
)
GO
/****** Object:  StoredProcedure [dbo].[DeleteAllMissingWeeks]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteAllMissingWeeks]

AS

BEGIN

	DELETE FROM 
		[dbo].[TimeTrackingMissingWeeks]
END

/*

EXEC dbo.DeleteAllMissingWeeks

*/
GO
/****** Object:  StoredProcedure [dbo].[DeleteMissingWeeks]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteMissingWeeks]

@HeadcountId int

AS

BEGIN

	DECLARE @CurrentTime as date
	SET @CurrentTime = GetDate()

	DELETE FROM 
		[dbo].[TimeTrackingMissingWeeks]
	 WHERE 
		[Headcount_Id] IN 
		(
			SELECT
				Head.[Id]
			FROM [dbo].[TimeTrackingHeadcount] Head

			WHERE 
				Head.[HeadcountID] = @HeadcountId
		)
END

/*

EXEC dbo.DeleteMissingWeeks @HeadcountId = 302

*/
GO
/****** Object:  StoredProcedure [dbo].[GetAllHoursPerMonth]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllHoursPerMonth]

@StartDate date,
@EndDate date,
@AreaID int = null

AS

BEGIN

	SELECT 
		MONTH(Days.[TrackedDate]) [Month]
		,YEAR(Days.[TrackedDate]) [Year]
		,Head.[HeadcountID]
		,SUM(Days.[Hours]) [TotalHours]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Days.[TrackedDate] >= @StartDate AND Days.[TrackedDate] <= @EndDate
	AND Weeks.[Status] = 4 --Approved
	AND (@AreaID IS NULL OR Head.AreaID  = @AreaID)

	GROUP BY MONTH(Days.[TrackedDate]), YEAR(Days.[TrackedDate]), Head.[HeadcountID] 
	ORDER BY MONTH(Days.[TrackedDate]), YEAR(Days.[TrackedDate]), Head.[HeadcountID]

END

/*

EXEC dbo.GetAllHoursPerMonth @StartDate = '2017-01-01', @EndDate = '2018-11-30'

*/
GO
/****** Object:  StoredProcedure [dbo].[GetAllHoursPerWeek]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllHoursPerWeek]

@StartDate date,
@EndDate date,
@AreaID int = null

AS

BEGIN

	SELECT 
		Weeks.[StartDate]
		,SUM(Days.[Hours]) [TotalHours]
		,Head.HeadcountID

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Days.[TrackedDate] >= @StartDate AND Days.[TrackedDate] <= @EndDate
	AND Weeks.[Status] = 4
	AND (@AreaID IS NULL OR Head.AreaID  = @AreaID)

	GROUP BY Weeks.[StartDate], Head.HeadcountID
	ORDER BY Weeks.[StartDate], Head.HeadcountID

END

/*

EXEC dbo.GetAllHoursPerWeek @StartDate = '2017-01-01', @EndDate = '2018-11-30'

*/
GO
/****** Object:  StoredProcedure [dbo].[GetAverageHoursByWeekRange]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAverageHoursByWeekRange]

@StartDate date,
@EndDate date

AS

BEGIN

	SELECT 
		Weeks.[StartDate]
		,SUM(Days.[Hours]) [TotalHours]
		,Head.HeadcountID

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Days.[TrackedDate] >= @StartDate AND Days.[TrackedDate] <= @EndDate
	AND Weeks.[Status] = 4

	GROUP BY Weeks.[StartDate], Head.HeadcountID
	ORDER BY Weeks.[StartDate], Head.HeadcountID

END

/*

EXEC dbo.GetAverageHoursByWeekRange @StartDate = '2018-02-01', @EndDate = '2018-07-31'

*/
GO
/****** Object:  StoredProcedure [dbo].[GetEvaluations]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEvaluations]

@EvaluationId int = null,
@LoginName nvarchar(400) = null,
@StartDate date = null,
@EndDate date = null,
@HeadcountId int = null,
--@Manager nvarchar(200) = null,
@ProgramIds dbo.CmxIntArray READONLY,  --custom-type defined separately
@ProjectListItemIds dbo.CmxIntArray READONLY,
@LocationIds dbo.CmxIntArray READONLY,
@StatusIds dbo.CmxIntArray READONLY

AS

BEGIN

	DECLARE @ProgramIdsRowsCount int
	DECLARE @ProjectListItemIdsRowsCount int
	DECLARE @LocationIdsRowsCount int
	DECLARE @StatusIdsRowsCount int

	SET @ProgramIdsRowsCount = (Select  count(col1) from @ProgramIds)
	SET @ProjectListItemIdsRowsCount = (Select  count(col1) from @ProjectListItemIds)
	SET @LocationIdsRowsCount = (Select  count(col1) from @LocationIds)
	SET @StatusIdsRowsCount = (Select  count(col1) from @StatusIds)

	SELECT 		
		Eval.[Id]
		,Head.HeadcountID
		,Head.FullName
		,Eval.[ProjectID]
		,Eval.[ProgramID]
		,Eval.[AreaID]
		,Eval.[Quantity]
		,Eval.[Quality]
		,Eval.[Knowhow]
		,Eval.[Relations]
		,Eval.[Cooperation]
		,Eval.[AtendanceReliability]
		,Eval.[InitiativeCreativity]
		,Eval.[Capacity]
		,Eval.[Comments]
		,Eval.[EvaluatedOn]
		,Eval.[EvaluatedBy]
		,Eval.[Status]
		,Eval.[AllocationListItemId]
		,Eval.[CreatedOn]
		,Eval.[LastEditedOn]
		,Eval.[Evaluator]
		,Eval.[ReaderGroups]
		,Eval.[Rating]
		--,Items.ListItemID
		--,Items.CountryID

	FROM [dbo].[ResourceEvaluations] Eval
	--INNER JOIN [dbo].[TimeTrackingItems] Items
	--ON Eval.Allocation_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingHeadcount] Head
	ON Head.HeadcountID = Eval.HeadcountID

	WHERE 
		--Items.TrackingType <= 2 --area or project
		(@EvaluationId IS NULL OR Eval.[Id] = @EvaluationId)
		AND (@LoginName IS NULL OR (Eval.[Evaluator] = @LoginName OR Head.[Manager] = @LoginName))
		AND (@StartDate IS NULL OR Eval.[CreatedOn] >= @StartDate)
		AND (@EndDate IS NULL OR Eval.[CreatedOn]  <= @EndDate)
		AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)

		AND (@ProgramIdsRowsCount = 0 OR (Eval.[ProgramID] IN (Select Col1 from @ProgramIds)))
		AND (@ProjectListItemIdsRowsCount = 0 OR (Eval.[ProjectID] IN (Select Col1 from @ProjectListItemIds)))
		--AND (@LocationIdsRowsCount = 0 OR (Items.[CountryID] IN (Select Col1 from @LocationIds)))
		AND (@StatusIdsRowsCount = 0 OR (Eval.[Status] IN (Select Col1 from @StatusIds)))
		--AND (@LoginName IS NULL OR Head.[Manager] = @LoginName)

	ORDER BY 
		Eval.[CreatedOn] , Head.[HeadcountID]
END

/*

EXEC dbo.GetEvaluations @LoginName = 'E-MGZAVALA@ext.cemex.com'

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (788)
--INSERT INTO @ids(col1)
--VALUES (4)
EXEC dbo.GetEvaluations @ProjectListItemIds = @ids, @LoginName = 'E-MGZAVALA@ext.cemex.com'

*/
GO
/****** Object:  StoredProcedure [dbo].[GetEvaluationsOLD]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetEvaluationsOLD]

@EvaluationId int = null,
@Evaluator nvarchar(400) = null,
@StartDate date = null,
@EndDate date = null,
@HeadcountId int = null,
@Manager nvarchar(200) = null,
@ProgramIds dbo.CmxIntArray READONLY,  --custom-type defined separately
@ProjectListItemIds dbo.CmxIntArray READONLY,
@LocationIds dbo.CmxIntArray READONLY,
@StatusIds dbo.CmxIntArray READONLY

AS

BEGIN

	DECLARE @ProgramIdsRowsCount int
	DECLARE @ProjectListItemIdsRowsCount int
	DECLARE @LocationIdsRowsCount int
	DECLARE @StatusIdsRowsCount int

	SET @ProgramIdsRowsCount = (Select  count(col1) from @ProgramIds)
	SET @ProjectListItemIdsRowsCount = (Select  count(col1) from @ProjectListItemIds)
	SET @LocationIdsRowsCount = (Select  count(col1) from @LocationIds)
	SET @StatusIdsRowsCount = (Select  count(col1) from @StatusIds)

	SELECT 		
		Eval.[Id]
		,Eval.[Quantity]
		,Eval.[Quality]
		,Eval.[Knowhow]
		,Eval.[Relations]
		,Eval.[Cooperation]
		,Eval.[AtendanceReliability]
		,Eval.[InitiativeCreativity]
		,Eval.[Capacity]
		,Eval.[Comments]
		,Eval.[EvaluatedOn]
		,Eval.[EvaluatedBy]
		,Eval.[Status]
		,Eval.[Allocation_Id]
		,Eval.[CreatedOn]
		,Eval.[LastEditedOn]
		,Eval.[Evaluator]
		,Eval.[ReaderGroups]
		,Eval.[Rating]
		,Head.HeadcountID
		,Head.FullName
		,Items.ListItemID
		,Items.CountryID

	FROM [dbo].[ResourceEvaluation] Eval
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Eval.Allocation_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingHeadcount] Head
	ON Head.Id = Items.Headcount_Id

	WHERE 
		Items.TrackingType = 1
		AND (@EvaluationId IS NULL OR Eval.[Id] = @EvaluationId)
		AND (@Evaluator IS NULL OR Eval.[Evaluator] = @Evaluator)
		AND (@StartDate IS NULL OR Eval.[CreatedOn] >= @StartDate)
		AND (@EndDate IS NULL OR Eval.[CreatedOn]  <= @EndDate)
		AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)

		AND (@ProgramIdsRowsCount = 0 OR (Items.[ProgramID] IN (Select Col1 from @ProgramIds)))
		AND (@ProjectListItemIdsRowsCount = 0 OR (Items.[ProjectID] IN (Select Col1 from @ProjectListItemIds) AND Items.[TrackingType] = 1))
		AND (@LocationIdsRowsCount = 0 OR (Items.[CountryID] IN (Select Col1 from @LocationIds)))
		AND (@StatusIdsRowsCount = 0 OR (Eval.[Status] IN (Select Col1 from @StatusIds)))
		AND (@Manager IS NULL OR Head.[Manager] = @Manager)

	ORDER BY 
		Eval.[CreatedOn] , Head.[HeadcountID], Items.[TrackingType], Items.[ListItemID], Items.[ProgramID], Items.[ProjectID], Items.[CountryID]
END

/*

EXEC dbo.GetEvaluations @Evaluator = 'E-MGZAVALA@ext.cemex.com'

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (84)
INSERT INTO @ids(col1)
VALUES (4)
EXEC dbo.GetEvaluations @LocationIds = @ids, @HeadcountId = 302

*/
GO
/****** Object:  StoredProcedure [dbo].[GetHeadcountHrsByYear]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHeadcountHrsByYear]
@HeadcountId int,
@Year int
AS

BEGIN

	SELECT 
		--Head.HeadcountID
		--Items.ID ItemDbID
		MONTH(Days.[TrackedDate]) [Month]
		,SUM(Days.[Hours]) [TotalHours]
		,Items.[TrackingType]
		,Items.[ListItemID]
		--,Items.[CountryID]  uncomment if By Country Breakdown is required
		--,Weeks.ID WeekDbID
		--,Weeks.[StartDate]
		--,Weeks.[EndDate]
		--,Weeks.[ModifiedBy]
		--,Weeks.[Modified]
		--,Weeks.[ModifiedKey]
		--,Weeks.[Comments]
		--,Weeks.[Status]
		--,Days.ID DayDbID
		--,Days.[TrackedDate]
		--,Days.[Hours]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id
	WHERE Year(Days.[TrackedDate]) = @Year
	AND Head.HeadcountID = @HeadcountId
	AND Weeks.[Status] = 4
	GROUP BY MONTH(Days.[TrackedDate]), Items.[TrackingType], Items.[ListItemID]
	ORDER BY MONTH(Days.[TrackedDate]), Items.[TrackingType], Items.[ListItemID]

END

/*

EXEC dbo.GetHeadcountHrsByYear @HeadcountId = 302, @Year = 2018

*/
GO
/****** Object:  StoredProcedure [dbo].[GetHeadcountPopulateWeek]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHeadcountPopulateWeek]
@HeadcountId int,
@StartDate date

AS

BEGIN

	SELECT 
		Top 1
		Weeks.ID
		,Weeks.[StartDate]
		,Weeks.[EndDate]
		,Weeks.[ModifiedBy]
		,Weeks.[Modified]
		,Weeks.[ModifiedKey]
		,Weeks.[Comments]
		,Weeks.[Approver]
		,Weeks.[ApprovedOn]
		,Weeks.[Status]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	WHERE Weeks.Status > 0 and Weeks.EndDate  < @StartDate
	AND Head.HeadcountID = @HeadcountId
	ORDER BY Weeks.[StartDate] DESC

END

/*

EXEC dbo.GetHeadcountPopulateWeek @HeadcountId = 331, @StartDate = '2018-10-14'

*/
GO
/****** Object:  StoredProcedure [dbo].[GetHeadcountSubmittedWeeks]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHeadcountSubmittedWeeks]
@StartDate date,
@EndDate date,
@HeadcountId int

AS

BEGIN

	SELECT 
		Head.HeadcountID
		,Weeks.[StartDate]
		,Weeks.[EndDate]

	FROM 
		[dbo].[TimeTrackingHeadcount] Head
		INNER JOIN [dbo].[TimeTrackingItems] Items
		ON Head.Id = Items.Headcount_Id
		INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
		ON Weeks.TrackingItem_Id = Items.Id

	WHERE 
		Weeks.StartDate >= @StartDate
		AND Weeks.EndDate  <= @EndDate
		AND Head.[HeadcountID] = @HeadcountId
		AND Weeks.Status >= 3

	ORDER BY
		Weeks.StartDate, Head.[HeadcountID]

END

/*

EXEC dbo.GetHeadcountSubmittedWeeks @StartDate = '2018-01-01', @EndDate = '2018-12-31', @HeadcountId = 20

*/
GO
/****** Object:  StoredProcedure [dbo].[GetHeadcountWeek]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHeadcountWeek]
@HeadcountId int,
@StartDate date
AS

BEGIN

	SELECT 
		Head.HeadcountID
		,Items.ID ItemDbID
		,Items.[TrackingType]
		,Items.[ListItemID]
		,Items.[CountryID]
		,Weeks.ID WeekDbID
		,Weeks.[StartDate]
		,Weeks.[EndDate]
		,Weeks.[ModifiedBy]
		,Weeks.[Modified]
		,Weeks.[ModifiedKey]
		,Weeks.[Comments]
		,Weeks.[Status]
		,Days.ID DayDbID
		,Days.[TrackedDate]
		,Days.[Hours]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Weeks.StartDate = @StartDate
	AND Head.HeadcountID = @HeadcountId

END

/*

EXEC dbo.GetHeadcountWeek @HeadcountId = 302, @StartDate = '2018-07-08'

*/
GO
/****** Object:  StoredProcedure [dbo].[GetHeadcountWeeks]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHeadcountWeeks]
@StartDate date = null,
@EndDate date = null,
@HeadcountId int = null,
@Manager nvarchar(200) = null,
@ProgramIds dbo.CmxIntArray READONLY,  --custom-type defined separately
@ProjectListItemIds dbo.CmxIntArray READONLY,
@LocationIds dbo.CmxIntArray READONLY,
@StatusIds dbo.CmxIntArray READONLY

AS

BEGIN

	DECLARE @ProgramIdsRowsCount int
	DECLARE @ProjectListItemIdsRowsCount int
	DECLARE @LocationIdsRowsCount int
	DECLARE @StatusIdsRowsCount int

	SET @ProgramIdsRowsCount = (Select  count(col1) from @ProgramIds)
	SET @ProjectListItemIdsRowsCount = (Select  count(col1) from @ProjectListItemIds)
	SET @LocationIdsRowsCount = (Select  count(col1) from @LocationIds)
	SET @StatusIdsRowsCount = (Select  count(col1) from @StatusIds)

	SELECT 
		Head.HeadcountID
		,Head.Manager
		,Items.ID ItemDbID
		,Items.[TrackingType]
		,Items.[ListItemID]
		,Items.[CountryID]
		,Weeks.ID WeekDbID
		,Weeks.[StartDate]
		,Weeks.[EndDate]
		,Weeks.[ModifiedBy]
		,Weeks.[Modified]
		,Weeks.[ModifiedKey]
		,Weeks.[Comments]
		,Weeks.[Status]
		,Days.ID DayDbID
		,Days.[TrackedDate]
		,Days.[Hours]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE 
	(@StartDate IS NULL OR Weeks.StartDate >= @StartDate)
	AND (@EndDate IS NULL OR Weeks.EndDate  <= @EndDate)
	AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)
	--AND (@Status IS NULL OR Weeks.[Status] = @Status)

	AND (@ProgramIdsRowsCount = 0 OR (Items.[ProgramID] IN (Select Col1 from @ProgramIds)))
	AND (@ProjectListItemIdsRowsCount = 0 OR (Items.[ProjectID] IN (Select Col1 from @ProjectListItemIds) AND Items.[TrackingType] = 1))
	AND (@LocationIdsRowsCount = 0 OR (Items.[CountryID] IN (Select Col1 from @LocationIds)))
	AND (@StatusIdsRowsCount = 0 OR (Weeks.[Status] IN (Select Col1 from @StatusIds)))
	AND (@Manager IS NULL OR Head.[Manager] = @Manager)

	ORDER BY Weeks.StartDate, Head.[HeadcountID], Items.[TrackingType], Items.[ListItemID], Items.[ProgramID], Items.[ProjectID], Items.[CountryID], Days.[TrackedDate]

END

/*

EXEC dbo.GetHeadcountWeeks @StartDate = '2018-01-01', @EndDate = '2018-12-31'
EXEC dbo.GetHeadcountWeeks @HeadcountId = 302, @StartDate = '2018-07-29', @EndDate = '2018-09-01'
EXEC dbo.GetHeadcountWeeks @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = 1
EXEC dbo.GetHeadcountWeeks @HeadcountId = 302, @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = 3
EXEC dbo.GetHeadcountWeeks @HeadcountId = null, @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = null

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (708)
EXEC dbo.GetHeadcountWeeks @StartDate = '2018-07-29', @EndDate = '2018-09-01', @ProjectListItemIds = @ids, @HeadcountId = 302

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (84)
INSERT INTO @ids(col1)
VALUES (4)
EXEC dbo.GetHeadcountWeeks @StartDate = '2018-07-29', @EndDate = '2018-09-01', @LocationIds = @ids, @HeadcountId = 302


DECLARE @prgids dbo.CmxIntArray
INSERT INTO @prgids(col1)
VALUES (61)
DECLARE @prjids dbo.CmxIntArray
INSERT INTO @prjids(col1)
VALUES (708)
DECLARE @locids dbo.CmxIntArray
INSERT INTO @locids(col1)
VALUES (84)
EXEC dbo.GetHeadcountWeeks @StartDate = '2018-07-29', @EndDate = '2018-09-01', @ProgramIds = @prgids, @ProjectListItemIds = @prjids, @LocationIds = @locids, @HeadcountId = 302

DECLARE @stids dbo.CmxIntArray
INSERT INTO @stids(col1)
VALUES (3)
EXEC dbo.GetHeadcountWeeks @StartDate = '2017-10-01', @EndDate = '2018-09-30', @Manager = 'E-MGZAVALA@ext.cemex.com', @StatusIds = @stids

DECLARE @stids dbo.CmxIntArray
INSERT INTO @stids(col1)
VALUES (3)
EXEC dbo.GetHeadcountWeeks @StatusIds = @stids
*/
GO
/****** Object:  StoredProcedure [dbo].[GetHeadcountWeeks1]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHeadcountWeeks1]
@HeadcountId int = null,
@StartDate date,
@EndDate date,
@Status int = null

AS

BEGIN

	SELECT 
		Head.HeadcountID
		,Items.ID ItemDbID
		,Items.[TrackingType]
		,Items.[ListItemID]
		,Items.[CountryID]
		,Weeks.ID WeekDbID
		,Weeks.[StartDate]
		,Weeks.[EndDate]
		,Weeks.[ModifiedBy]
		,Weeks.[Modified]
		,Weeks.[ModifiedKey]
		,Weeks.[Comments]
		,Weeks.[Status]
		,Days.ID DayDbID
		,Days.[TrackedDate]
		,Days.[Hours]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Weeks.StartDate >= @StartDate and Weeks.EndDate  <= @EndDate
	AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)
	AND (@Status IS NULL OR Weeks.[Status] = @Status)

END

/*

EXEC dbo.GetHeadcountWeeks1 @HeadcountId = 302, @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = null

*/
GO
/****** Object:  StoredProcedure [dbo].[GetHours]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHours]

@Year int,
@HeadcountId int = null,
@ProgramId int = null,
@ProjectListItemId int = null

AS

BEGIN

	SELECT 
		MONTH(Days.[TrackedDate]) [Month]
		,Head.[HeadcountID]
		,SUM(Days.[Hours]) [TotalHours]
		,Items.[TrackingType]
		,Items.[ProgramID]
		,Items.[ListItemID]
		--,Items.[CountryID]  uncomment if By Country Breakdown is required

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Year(Days.[TrackedDate]) = @Year
	AND Weeks.[Status] = 4
	AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)
	AND (@ProgramId IS NULL OR (Items.[ProgramID] = @ProgramId))
	AND (@ProjectListItemId IS NULL OR (Items.[ListItemID] = @ProjectListItemId AND Items.[TrackingType] = 1))

	GROUP BY MONTH(Days.[TrackedDate]), Head.[HeadcountID], Items.[TrackingType], Items.[ProgramID], Items.[ListItemID]
	ORDER BY MONTH(Days.[TrackedDate]), Items.[TrackingType], Items.[ListItemID], Head.[HeadcountID]

END

/*
EXEC dbo.GetHours @Year = 2018
EXEC dbo.GetHours @Year = 2018, @AreaId = 10
EXEC dbo.GetHours @Year = 2018, @HeadcountId = 1
EXEC dbo.GetHours @Year = 2018, @ProjectListItemId = 29
*/
GO
/****** Object:  StoredProcedure [dbo].[GetHoursByDates]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHoursByDates]

@StartDate date,
@EndDate date,
@HeadcountId int = null,
@ProgramId int = null,
@ProjectListItemId int = null

AS

BEGIN

	SELECT 
		MONTH(Days.[TrackedDate]) [Month]
		,YEAR(Days.[TrackedDate]) [Year]
		,Head.[HeadcountID]
		,SUM(Days.[Hours]) [TotalHours]
		,Items.[TrackingType]
		,Items.[ProgramID]
		,Items.[ListItemID]
		--,Items.[CountryID]  uncomment if By Country Breakdown is required

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Days.[TrackedDate] >= @StartDate AND Days.[TrackedDate] <= @EndDate
	AND Weeks.[Status] = 4
	AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)
	AND (@ProgramId IS NULL OR (Items.[ProgramID] = @ProgramId))
	AND (@ProjectListItemId IS NULL OR (Items.[ListItemID] = @ProjectListItemId AND Items.[TrackingType] = 1))

	GROUP BY MONTH(Days.[TrackedDate]), YEAR(Days.[TrackedDate]), Head.[HeadcountID], Items.[TrackingType], Items.[ProgramID], Items.[ListItemID]
	ORDER BY MONTH(Days.[TrackedDate]), YEAR(Days.[TrackedDate]), Items.[TrackingType], Items.[ListItemID], Head.[HeadcountID]

END

/*
EXEC dbo.GetHoursByDates @StartDate = '2018-02-01', @EndDate = '2018-07-31'
EXEC dbo.GetHoursByDates @StartDate = '2018-01-01', @EndDate = '2018-12-31', @AreaId = 10
EXEC dbo.GetHoursByDates @StartDate = '2018-01-01', @EndDate = '2018-12-31', @HeadcountId = 1
EXEC dbo.GetHoursByDates @StartDate = '2018-01-01', @EndDate = '2018-12-31', @ProjectListItemId = 29
*/
GO
/****** Object:  StoredProcedure [dbo].[GetHoursByDatesRange]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHoursByDatesRange]

@StartDate date,
@EndDate date,
@HeadcountId int = null,
@Status int,
@ProgramIds dbo.CmxIntArray READONLY,  --custom-type defined separately
@ProjectListItemIds dbo.CmxIntArray READONLY,
@LocationIds dbo.CmxIntArray READONLY
--@AreaId int = null --for future use

AS

BEGIN

	DECLARE @ProgramIdsRowsCount int
	DECLARE @ProjectListItemIdsRowsCount int
	DECLARE @LocationIdsRowsCount int

	SET @ProgramIdsRowsCount = (Select  count(col1) from @ProgramIds)
	SET @ProjectListItemIdsRowsCount = (Select  count(col1) from @ProjectListItemIds)
	SET @LocationIdsRowsCount = (Select  count(col1) from @LocationIds)

	SELECT 
		MONTH(Days.[TrackedDate]) [Month]
		,YEAR(Days.[TrackedDate]) [Year]
		,Head.[HeadcountID]
		,Items.[TrackingType]
		,Items.[ProgramID]
		,Items.[ListItemID]
		,Items.[ProjectID]
		,Items.[AreaID]
		,Items.[CountryID]
		,SUM(Days.[Hours]) [TotalHours]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Days.[TrackedDate] >= @StartDate AND Days.[TrackedDate] <= @EndDate
	AND Weeks.[Status] = @Status
	AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)
	AND (@ProgramIdsRowsCount = 0 OR (Items.[ProgramID] IN (Select Col1 from @ProgramIds)))
	AND (@ProjectListItemIdsRowsCount = 0 OR (Items.[ProjectID] IN (Select Col1 from @ProjectListItemIds) AND Items.[TrackingType] = 1))
	AND (@LocationIdsRowsCount = 0 OR (Items.[CountryID] IN (Select Col1 from @LocationIds)))

	GROUP BY MONTH(Days.[TrackedDate]), YEAR(Days.[TrackedDate]), Head.[HeadcountID], Items.[TrackingType], Items.[ProgramID], Items.[ListItemID], Items.[ProjectID], Items.[AreaID], Items.[CountryID] 
	ORDER BY MONTH(Days.[TrackedDate]), YEAR(Days.[TrackedDate]), Head.[HeadcountID], Items.[TrackingType], Items.[ListItemID], Items.[ProgramID], Items.[ProjectID], Items.[AreaID], Items.[CountryID] 

END

/*
EXEC dbo.GetHoursByDatesRange @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = 4

EXEC dbo.GetHoursByDatesRange @StartDate = '2018-08-19', @EndDate = '2018-08-25', @HeadcountId = 302

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (7)
EXEC dbo.GetHoursByDatesRange @StartDate = '2018-01-01', @EndDate = '2018-12-31', @ProgramIds = @ids, @HeadcountId = 1

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (708)
INSERT INTO @ids(col1)
VALUES (36)
EXEC dbo.GetHoursByDatesRange @StartDate = "2018-01-01", @EndDate = "2018-12-31", @ProjectListItemIds = @ids, @HeadcountId = 166

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (84)
INSERT INTO @ids(col1)
VALUES (4)
EXEC dbo.GetHoursByDatesRange @StartDate = "2018-01-01", @EndDate = "2018-12-31", @LocationIds = @ids, @HeadcountId = 166

DECLARE @prgids dbo.CmxIntArray
INSERT INTO @prgids(col1)
VALUES (6)
DECLARE @prjids dbo.CmxIntArray
INSERT INTO @prjids(col1)
VALUES (708)
DECLARE @locids dbo.CmxIntArray
INSERT INTO @locids(col1)
VALUES (84)
EXEC dbo.GetHoursByDatesRange @StartDate = "2018-01-01", @EndDate = "2018-12-31", @ProgramIds = @prgids, @ProjectListItemIds = @prjids, @LocationIds = @locids, @HeadcountId = 302

*/
GO
/****** Object:  StoredProcedure [dbo].[GetHoursByWeek]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHoursByWeek]

@StartDate date,
@EndDate date,
@HeadcountId int = null,
@ProgramId int = null,
@ProjectListItemId int = null

AS

BEGIN

	SELECT 
		Weeks.[StartDate]
		,SUM(Days.[Hours]) [TotalHours]
		--,Head.[AreaID]  uncomment if breakdowns are needed
		--,Head.[HeadcountID]
		--,Items.[TrackingType]
		--,Items.[ProgramID]
		--,Items.[ListItemID]
		--,Items.[CountryID]  uncomment if By Country Breakdown is required

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Days.[TrackedDate] >= @StartDate AND Days.[TrackedDate] <= @EndDate
	AND Weeks.[Status] = 4
	AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)
	AND (@ProgramId IS NULL OR (Items.[ProgramID] = @ProgramId))
	AND (@ProjectListItemId IS NULL OR (Items.[ListItemID] = @ProjectListItemId AND Items.[TrackingType] = 1))

	--GROUP BY Weeks.[StartDate], Head.[AreaID], Head.[HeadcountID], Items.[TrackingType], Items.[ProgramID], Items.[ListItemID]  uncomment if breakdowns are needed
	--ORDER BY Weeks.[StartDate], Head.[AreaID], Head.[HeadcountID], Items.[TrackingType], Items.[ProgramID], Items.[ListItemID]  uncomment if breakdowns are needed
	GROUP BY Weeks.[StartDate]
	ORDER BY Weeks.[StartDate]

END

/*
EXEC dbo.GetHoursByWeek @StartDate = '2018-02-01', @EndDate = '2018-07-31'
EXEC dbo.GetHoursByWeek @StartDate = '2018-01-01', @EndDate = '2018-12-31', @AreaId = 12
EXEC dbo.GetHoursByWeek @StartDate = '2018-01-01', @EndDate = '2018-12-31', @HeadcountId = 1
EXEC dbo.GetHoursByWeek @StartDate = '2018-01-01', @EndDate = '2018-12-31', @ProjectListItemId = 29
*/
GO
/****** Object:  StoredProcedure [dbo].[GetHoursByWeekRange]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHoursByWeekRange]

@StartDate date,
@EndDate date,
@HeadcountId int = null,
@ProgramIds dbo.CmxIntArray READONLY,  --custom-type defined separately
@ProjectListItemIds dbo.CmxIntArray READONLY,
@LocationIds dbo.CmxIntArray READONLY

AS

BEGIN

	DECLARE @ProgramIdsRowsCount int
	DECLARE @ProjectListItemIdsRowsCount int
	DECLARE @LocationIdsRowsCount int

	SET @ProgramIdsRowsCount = (Select  count(col1) from @ProgramIds)
	SET @ProjectListItemIdsRowsCount = (Select  count(col1) from @ProjectListItemIds)
	SET @LocationIdsRowsCount = (Select  count(col1) from @LocationIds)

	SELECT 
		Weeks.[StartDate]
		,SUM(Days.[Hours]) [TotalHours]
		--,Head.[HeadcountID]
		--,Items.[TrackingType]
		--,Items.[ProgramID]
		--,Items.[ListItemID]
		--,Items.[CountryID]  uncomment if By Country Breakdown is required

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Days.[TrackedDate] >= @StartDate AND Days.[TrackedDate] <= @EndDate
	AND Weeks.[Status] = 4
	AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)
	AND (@ProgramIdsRowsCount = 0 OR (Items.[ProgramID] IN (Select Col1 from @ProgramIds)))
	AND (@ProjectListItemIdsRowsCount = 0 OR (Items.[ProjectID] IN (Select Col1 from @ProjectListItemIds) AND Items.[TrackingType] = 1))
	AND (@LocationIdsRowsCount = 0 OR (Items.[CountryID] IN (Select Col1 from @LocationIds)))

	--GROUP BY Weeks.[StartDate], Head.[HeadcountID], Items.[TrackingType], Items.[ProgramID], Items.[ListItemID]  uncomment if breakdowns are needed
	--ORDER BY Weeks.[StartDate], Head.[HeadcountID], Items.[TrackingType], Items.[ProgramID], Items.[ListItemID]  uncomment if breakdowns are needed
	GROUP BY Weeks.[StartDate]
	ORDER BY Weeks.[StartDate]

END

/*
EXEC dbo.GetHoursByWeekRange @StartDate = '2018-02-01', @EndDate = '2018-07-31'

EXEC dbo.GetHoursByWeekRange @StartDate = '2018-01-01', @EndDate = '2018-12-31', @HeadcountId = 1

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (6)
EXEC dbo.GetHoursByWeekRange @StartDate = '2018-01-01', @EndDate = '2018-12-31', @ProgramIds = @ids, @HeadcountId = 302

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (708)
INSERT INTO @ids(col1)
VALUES (36)
EXEC dbo.GetHoursByWeekRange @StartDate = "2018-01-01", @EndDate = "2018-12-31", @ProjectListItemIds = @ids, @HeadcountId = 166

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (84)
INSERT INTO @ids(col1)
VALUES (4)
EXEC dbo.GetHoursByWeekRange @StartDate = "2018-01-01", @EndDate = "2018-12-31", @LocationIds = @ids, @HeadcountId = 166

DECLARE @prgids dbo.CmxIntArray
INSERT INTO @prgids(col1)
VALUES (6)
DECLARE @prjids dbo.CmxIntArray
INSERT INTO @prjids(col1)
VALUES (708)
DECLARE @locids dbo.CmxIntArray
INSERT INTO @locids(col1)
VALUES (84)
EXEC dbo.GetHoursByWeekRange @StartDate = "2018-01-01", @EndDate = "2018-12-31", @ProgramIds = @prgids, @ProjectListItemIds = @prjids, @LocationIds = @locids, @HeadcountId = 302

*/
GO
/****** Object:  StoredProcedure [dbo].[GetHoursPerDayByDatesRange]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetHoursPerDayByDatesRange]

@StartDate date,
@EndDate date,
@HeadcountId int = null,
@ProgramIds dbo.CmxIntArray READONLY,  --custom-type defined separately
@ProjectListItemIds dbo.CmxIntArray READONLY,
@LocationIds dbo.CmxIntArray READONLY
--@AreaId int = null --for future use

AS

BEGIN

	DECLARE @ProgramIdsRowsCount int
	DECLARE @ProjectListItemIdsRowsCount int
	DECLARE @LocationIdsRowsCount int

	SET @ProgramIdsRowsCount = (Select  count(col1) from @ProgramIds)
	SET @ProjectListItemIdsRowsCount = (Select  count(col1) from @ProjectListItemIds)
	SET @LocationIdsRowsCount = (Select  count(col1) from @LocationIds)

	SELECT
		Head.HeadcountID
		--,Items.ID ItemDbID
		,Items.[TrackingType]
		,Items.[ListItemID]
		,Items.[ProgramID]
		,Items.[ProjectID]
		,Items.[AreaID]
		,Items.[CountryID]
		--,Weeks.ID WeekDbID
		--,Weeks.[StartDate]
		--,Weeks.[EndDate]
		--,Weeks.[ModifiedBy]
		--,Weeks.[Modified]
		--,Weeks.[ModifiedKey]
		--,Weeks.[Comments]
		--,Weeks.[Status]
		--,Days.ID DayDbID
		,Days.[TrackedDate]
		,Days.[Hours]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	INNER JOIN [dbo].[TimeTrackingDays] Days
	ON Weeks.Id = Days.Week_Id

	WHERE Days.[TrackedDate] >= @StartDate AND Days.[TrackedDate] <= @EndDate
	AND Weeks.[Status] = 4 --Approved
	AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)
	AND (@ProgramIdsRowsCount = 0 OR (Items.[ProgramID] IN (Select Col1 from @ProgramIds)))
	AND (@ProjectListItemIdsRowsCount = 0 OR (Items.[ProjectID] IN (Select Col1 from @ProjectListItemIds) AND Items.[TrackingType] = 1))
	AND (@LocationIdsRowsCount = 0 OR (Items.[CountryID] IN (Select Col1 from @LocationIds)))

	ORDER BY Head.[HeadcountID], Items.[TrackingType], Items.[ListItemID], Items.[ProgramID], Items.[ProjectID], Items.[CountryID], Days.[TrackedDate]

END

/*
EXEC dbo.GetHoursPerDayByDatesRange @StartDate = '2016-02-10', @EndDate = '2018-07-27'

EXEC dbo.GetHoursPerDayByDatesRange @StartDate = '2018-01-01', @EndDate = '2018-12-31', @HeadcountId = 1

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (15)
EXEC dbo.GetHoursPerDayByDatesRange @StartDate = '2018-01-01', @EndDate = '2018-12-31', @ProjectListItemIds = @ids, @HeadcountId = 1

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (708)
INSERT INTO @ids(col1)
VALUES (36)
EXEC dbo.GetHoursPerDayByDatesRange @StartDate = "2018-01-01", @EndDate = "2018-12-31", @ProjectListItemIds = @ids, @HeadcountId = 166

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (84)
INSERT INTO @ids(col1)
VALUES (4)
EXEC dbo.GetHoursPerDayByDatesRange @StartDate = "2018-01-01", @EndDate = "2018-12-31", @LocationIds = @ids, @HeadcountId = 166

DECLARE @prgids dbo.CmxIntArray
INSERT INTO @prgids(col1)
VALUES (6)
DECLARE @prjids dbo.CmxIntArray
INSERT INTO @prjids(col1)
VALUES (708)
DECLARE @locids dbo.CmxIntArray
INSERT INTO @locids(col1)
VALUES (84)
EXEC dbo.GetHoursPerDayByDatesRange @StartDate = "2018-01-01", @EndDate = "2018-12-31", @ProgramIds = @prgids, @ProjectListItemIds = @prjids, @LocationIds = @locids, @HeadcountId = 302

*/
GO
/****** Object:  StoredProcedure [dbo].[GetLastSubmittedWeek]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLastSubmittedWeek]
@HeadcountId int

AS

BEGIN

	SELECT 
		Top 1
		Weeks.ID
		,Weeks.[StartDate]
		,Weeks.[EndDate]
		--,Weeks.[ModifiedBy]
		--,Weeks.[Modified]
		--,Weeks.[ModifiedKey]
		--,Weeks.[Comments]
		--,Weeks.[Approver]
		--,Weeks.[Status]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
	ON Weeks.TrackingItem_Id = Items.Id
	WHERE 
		Weeks.Status > 2 
		AND Head.HeadcountID = @HeadcountId
	ORDER BY 
		Weeks.[StartDate] DESC

END

/*

EXEC dbo.GetLastSubmittedWeek @HeadcountId = 20

*/
GO
/****** Object:  StoredProcedure [dbo].[GetMissingWeeks]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetMissingWeeks]
@StartDate date = null,
@EndDate date = null,
@HeadcountId int = null,
@Manager nvarchar(200) = null--,
--@ProgramIds dbo.CmxIntArray READONLY,  --custom-type defined separately
--@ProjectListItemIds dbo.CmxIntArray READONLY,
--@LocationIds dbo.CmxIntArray READONLY

AS

BEGIN

	--DECLARE @ProgramIdsRowsCount int
	--DECLARE @ProjectListItemIdsRowsCount int
	--DECLARE @LocationIdsRowsCount int

	--SET @ProgramIdsRowsCount = (Select  count(col1) from @ProgramIds)
	--SET @ProjectListItemIdsRowsCount = (Select  count(col1) from @ProjectListItemIds)
	--SET @LocationIdsRowsCount = (Select  count(col1) from @LocationIds)

	SELECT 
		Head.HeadcountID
		--,Items.ID ItemDbID
		--,Items.[TrackingType]
		--,Items.[ListItemID]
		--,Items.[CountryID]
		,Weeks.ID WeekDbID
		,Weeks.[StartDate]
		,Weeks.[EndDate]

	FROM [dbo].[TimeTrackingHeadcount] Head
	--INNER JOIN [dbo].[TimeTrackingItems] Items
	--ON Head.Id = Items.Headcount_Id
	INNER JOIN [dbo].[TimeTrackingMissingWeeks] Weeks
	ON Head.Id = Weeks.Headcount_Id

	WHERE 
		(@StartDate IS NULL OR Weeks.StartDate >= @StartDate)
		AND (@EndDate IS NULL OR Weeks.EndDate  <= @EndDate)
		AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)

		--AND (@ProgramIdsRowsCount = 0 OR (Items.[ProgramID] IN (Select Col1 from @ProgramIds)))
		--AND (@ProjectListItemIdsRowsCount = 0 OR (Items.[ProjectID] IN (Select Col1 from @ProjectListItemIds) AND Items.[TrackingType] = 1))
		--AND (@LocationIdsRowsCount = 0 OR (Items.[CountryID] IN (Select Col1 from @LocationIds)))
		AND (@Manager IS NULL OR Head.[Manager] = @Manager)

	ORDER BY Weeks.StartDate, Head.[HeadcountID]
	--, Items.[TrackingType], Items.[ListItemID], Items.[ProgramID], Items.[ProjectID], Items.[CountryID]

END

/*

EXEC dbo.GetMissingWeeks @StartDate = '2018-01-01', @EndDate = '2018-12-31'
EXEC dbo.GetMissingWeeks @HeadcountId = 302, @StartDate = '2018-07-29', @EndDate = '2018-09-01'
EXEC dbo.GetMissingWeeks @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = 1
EXEC dbo.GetMissingWeeks @HeadcountId = 302, @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = 3
EXEC dbo.GetMissingWeeks @HeadcountId = null, @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = null

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (708)
EXEC dbo.GetMissingWeeks @StartDate = '2018-07-29', @EndDate = '2018-09-01', @ProjectListItemIds = @ids, @HeadcountId = 302

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (84)
INSERT INTO @ids(col1)
VALUES (4)
EXEC dbo.GetMissingWeeks @StartDate = '2018-07-29', @EndDate = '2018-09-01', @LocationIds = @ids, @HeadcountId = 302


DECLARE @prgids dbo.CmxIntArray
INSERT INTO @prgids(col1)
VALUES (61)
DECLARE @prjids dbo.CmxIntArray
INSERT INTO @prjids(col1)
VALUES (708)
DECLARE @locids dbo.CmxIntArray
INSERT INTO @locids(col1)
VALUES (84)
EXEC dbo.GetMissingWeeks @StartDate = '2018-07-29', @EndDate = '2018-09-01', @ProgramIds = @prgids, @ProjectListItemIds = @prjids, @LocationIds = @locids, @HeadcountId = 302

DECLARE @stids dbo.CmxIntArray
INSERT INTO @stids(col1)
VALUES (3)
EXEC dbo.GetMissingWeeks @StartDate = '2017-10-01', @EndDate = '2018-09-30', @Manager = 'E-MGZAVALA@ext.cemex.com', @StatusIds = @stids
*/
GO
/****** Object:  StoredProcedure [dbo].[GetMissingWeeksForFilter]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetMissingWeeksForFilter]
@StartDate date = null,
@EndDate date = null,
@HeadcountId int = null,
@Manager nvarchar(200) = null,
@ProgramIds dbo.CmxIntArray READONLY,  --custom-type defined separately
@ProjectListItemIds dbo.CmxIntArray READONLY,
@LocationIds dbo.CmxIntArray READONLY

AS

BEGIN

	DECLARE @ProgramIdsRowsCount int
	DECLARE @ProjectListItemIdsRowsCount int
	DECLARE @LocationIdsRowsCount int

	SET @ProgramIdsRowsCount = (Select  count(col1) from @ProgramIds)
	SET @ProjectListItemIdsRowsCount = (Select  count(col1) from @ProjectListItemIds)
	SET @LocationIdsRowsCount = (Select  count(col1) from @LocationIds)

	SELECT 
		Head.HeadcountID
		,Items.ID ItemDbIDFiltered
		,Items.[TrackingType] TrackingTypeFiltered
		,Items.[ListItemID] ListItemIDFiltered
		,Items.[CountryID]
		,Weeks.ID WeekDbID
		,Weeks.[StartDate]
		,Weeks.[EndDate]

	FROM [dbo].[TimeTrackingHeadcount] Head
	INNER JOIN [dbo].[TimeTrackingMissingWeeks] Weeks
	ON Head.Id = Weeks.Headcount_Id
	LEFT OUTER JOIN [dbo].[TimeTrackingItems] Items
	ON Head.Id = Items.Headcount_Id

	WHERE 
		(@StartDate IS NULL OR Weeks.StartDate >= @StartDate)
		AND (@EndDate IS NULL OR Weeks.EndDate  <= @EndDate)
		AND (@HeadcountId IS NULL OR Head.[HeadcountID] = @HeadcountId)

		AND (@ProgramIdsRowsCount = 0 OR (Items.[ProgramID] IN (Select Col1 from @ProgramIds)))
		AND (@ProjectListItemIdsRowsCount = 0 OR (Items.[ProjectID] IN (Select Col1 from @ProjectListItemIds) AND Items.[TrackingType] = 1))
		AND (@LocationIdsRowsCount = 0 OR (Items.[CountryID] IN (Select Col1 from @LocationIds)))
		AND (@Manager IS NULL OR Head.[Manager] = @Manager)

	ORDER BY Weeks.StartDate, Head.[HeadcountID], Items.[TrackingType], Items.[ListItemID], Items.[ProgramID], Items.[ProjectID], Items.[CountryID]

END

/*

EXEC dbo.GetMissingWeeksForFilter @StartDate = '2018-01-01', @EndDate = '2018-12-31'
EXEC dbo.GetMissingWeeksForFilter @HeadcountId = 302, @StartDate = '2018-07-29', @EndDate = '2018-09-01'
EXEC dbo.GetMissingWeeksForFilter @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = 1
EXEC dbo.GetMissingWeeksForFilter @HeadcountId = 302, @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = 3
EXEC dbo.GetMissingWeeksForFilter @HeadcountId = null, @StartDate = '2018-07-29', @EndDate = '2018-09-01', @Status = null

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (708)
EXEC dbo.GetMissingWeeksForFilter @StartDate = '2018-07-29', @EndDate = '2018-09-01', @ProjectListItemIds = @ids, @HeadcountId = 302

DECLARE @ids dbo.CmxIntArray
INSERT INTO @ids(col1)
VALUES (84)
INSERT INTO @ids(col1)
VALUES (4)
EXEC dbo.GetMissingWeeksForFilter @StartDate = '2018-07-29', @EndDate = '2018-09-01', @LocationIds = @ids, @HeadcountId = 302


DECLARE @prgids dbo.CmxIntArray
INSERT INTO @prgids(col1)
VALUES (61)
DECLARE @prjids dbo.CmxIntArray
INSERT INTO @prjids(col1)
VALUES (708)
DECLARE @locids dbo.CmxIntArray
INSERT INTO @locids(col1)
VALUES (84)
EXEC dbo.GetMissingWeeksForFilter @StartDate = '2018-07-29', @EndDate = '2018-09-01', @ProgramIds = @prgids, @ProjectListItemIds = @prjids, @LocationIds = @locids, @HeadcountId = 302

DECLARE @stids dbo.CmxIntArray
INSERT INTO @stids(col1)
VALUES (3)
EXEC dbo.GetMissingWeeksForFilter @StartDate = '2017-10-01', @EndDate = '2018-09-30', @Manager = 'E-MGZAVALA@ext.cemex.com', @StatusIds = @stids
*/
GO
/****** Object:  StoredProcedure [dbo].[SetWeekStatus]    Script Date: 4/30/2019 1:18:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetWeekStatus]
@HeadcountId int,
@StartDate date,
@Status int = null,
@Approver nvarchar(200),
@Comments nvarchar(200) = null

AS

BEGIN

	DECLARE @CurrentTime as date
	SET @CurrentTime = GetDate()

	UPDATE 
		[dbo].[TimeTrackingWeeks]
	SET
		  [ApprovedOn] = @CurrentTime,
		  [Status] = @Status,
		  [Comments] = @Comments,
		  [Approver] = @Approver
	 WHERE 
		[StartDate] = @StartDate 
		AND [Id] IN 
		(
			SELECT
				DISTINCT Weeks.[Id]

			FROM [dbo].[TimeTrackingHeadcount] Head
			INNER JOIN [dbo].[TimeTrackingItems] Items
			ON Head.Id = Items.Headcount_Id
			INNER JOIN [dbo].[TimeTrackingWeeks] Weeks
			ON Weeks.TrackingItem_Id = Items.Id
			INNER JOIN [dbo].[TimeTrackingDays] Days
			ON Weeks.Id = Days.Week_Id

			WHERE Weeks.StartDate = @StartDate
			AND Head.[HeadcountID] = @HeadcountId
		)


END

/*

EXEC dbo.SetWeekStatus @HeadcountId = 302, @StartDate = '2018-08-05', @Status = 4, @Approver = "E-MGZAVALA@ext.cemex.com"

*/
GO
