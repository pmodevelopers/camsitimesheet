﻿using Cmx.Pmo.SQL.Model.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.SQL.Helpers
{
    public class EntityHelper
    {
        public static TimeTrackingHeadcount ToHeadCount(List<TimeTrackingItemWeekDay> listSource)
        {
            TimeTrackingHeadcount objResult = new TimeTrackingHeadcount();
            objResult.TimeTrackingItems = new List<TimeTrackingItem>();

            //group by Items
            var items = listSource.GroupBy(x => x.ItemDbID).Select(y => y);

            foreach(var item in listSource)
            {
                objResult.HeadcountID = item.HeadcountID;
            }

            return objResult;

        }
    }
}
