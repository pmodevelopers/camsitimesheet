﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Enums.ResourceManagement
{
    public enum DashboardSection
    {
        HeadcountStatus,
        Top5Projects,
        Top5Requests,
        Utilization
    }
}
