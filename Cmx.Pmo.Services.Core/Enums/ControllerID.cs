﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Enums
{
    public enum ControllerID
    {
        Base,
        Timesheet,
        Log,
        Operations,
        Dashboard,
        Evaluation,
        SharePoint
    }
}
