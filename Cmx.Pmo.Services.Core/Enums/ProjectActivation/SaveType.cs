﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Enums.ProjectActivation
{
    public enum SaveType
    {
        ProjectLeader,
        ProjectManager,
        Document,
        Track,
        Milestone,
        BudgetItem,
        BudgetItemYear
    }
}
