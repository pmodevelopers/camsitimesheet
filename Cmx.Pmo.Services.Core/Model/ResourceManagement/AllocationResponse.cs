﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    public class AllocationResponse
    {
        public int soft { get; set; }
        public int total { get; set; }
        public int avg { get; set; }
        public int r1 { get; set; }
        public int r2 { get; set; }
        public int r3 { get; set; }
        public int r4 { get; set; }
        public int r5 { get; set; }
        public int r6 { get; set; }

        public ProjectResponse[] newProjects { get; set; }
        public ProjectResponse[] updateProjects { get; set; }

        public class ProjectResponse
        {
            public int id { get; set; }
            public string n { get; set; }
            public int t { get; set; }
            public string s { get; set; }
        }
    }
}
