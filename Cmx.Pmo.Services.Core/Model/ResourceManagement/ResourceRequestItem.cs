﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    public class ResourceRequestItem
    {
        public int ID { get; set; }
        public int FTEs { get; set; }
        public string ProgramName { get; set; }        
    }
}
