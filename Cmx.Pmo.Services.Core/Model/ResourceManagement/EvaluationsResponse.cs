﻿using Cmx.Pmo.SQL.Model.Evaluations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    public class EvaluationsResponse
    {
        public List<ResourceEvaluationItemFromDB> Evaluations { get; set; }
    }
}
