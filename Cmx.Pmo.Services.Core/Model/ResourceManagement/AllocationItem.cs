﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    public class AllocationItem
    {
        public int ID { get; set; }
        public string Allocation_x0020_Type { get; set; }
        public string ExpirationNoticeStatus { get; set; }
        public DateTime? Finish_x0020_Date { get; set; }
        //resource full name & id
        public FieldLookupValue Resource_x0020_Name_x003a_Full_x { get; set; }
        //project & project long Id
        public FieldLookupValue Project_x0020_Assigned_x0020_Nam { get; set; }
        //project Name
        public FieldLookupValue Project_x0020_Assigned_x0020_Nam0 { get; set; }
        //project calculated status
        //public string Project_x0020_Assigned_x0020_Nam1 { get; set; }
        //area
        public FieldLookupValue Area_x0020_Assigned { get; set; }        
        //Role
        public FieldLookupValue Role { get; set; }
        //allocation pctg
        public int Allocation { get; set; }
        //Manager
        public FieldUserValue Manager { get; set; }
        public string ManagerLoginName { get; set; }


        private int? _headcountID = null;
        public int? HeadcountID
        {
            get
            {
                if (_headcountID == null)
                {
                    if (this.Resource_x0020_Name_x003a_Full_x != null && this.Resource_x0020_Name_x003a_Full_x.LookupId > 0)
                    {
                        //int val = 0;
                        //if (Int32.TryParse(this.Resource_x0020_Name_x003a_Full_x.LookupValue, out val))
                        //{
                        //    _headcountID = val;
                        //}
                        _headcountID = this.Resource_x0020_Name_x003a_Full_x.LookupId;
                    }
                }

                return _headcountID;
            }
        }

        private int? _projectListItemID = null;
        public int? ProjectListItemID
        {
            get
            {
                if(_projectListItemID == null)
                {
                    if (this.Project_x0020_Assigned_x0020_Nam != null && this.Project_x0020_Assigned_x0020_Nam.LookupId > 0)
                    {
                        _projectListItemID = this.Project_x0020_Assigned_x0020_Nam.LookupId;
                    }
                    else if (this.Project_x0020_Assigned_x0020_Nam0 != null && this.Project_x0020_Assigned_x0020_Nam0.LookupId > 0)
                    {
                        _projectListItemID = this.Project_x0020_Assigned_x0020_Nam0.LookupId;
                    }
                }

                return _projectListItemID;
            }
        }

        public int? ProgramID { get; set; }

        private int? _areaID = null;
        public int? AreaID
        {
            get
            {
                if (_areaID == null)
                {
                    if (this.Area_x0020_Assigned != null && this.Area_x0020_Assigned.LookupId > 0)
                    {
                        _areaID = this.Area_x0020_Assigned.LookupId;
                    }
                }

                return _areaID;
            }
        }

        //project ListItemId
        //public FieldLookupValue Project_x0020_Assigned_x0020_Nam2 { get; set; }
        //area ListItemId
        //public FieldLookupValue Area_x0020_Assigned_x003a_ID { get; set; }

    }
}
