﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    public class SectionItem
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Option { get; set; }
        public int TotalNumber { get; set; }

    }
}
