﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cmx.Pmo.Services.Core.Constants;
using Cmx.Pmo.Services.Core.Controllers;
using Cmx.Pmo.Services.Core.Model.ResourceManagement;

namespace Cmx.Pmo.Services.Core.Model
{
    /// Holds configuration data required by the Dashboard Update Process
    public class ConfigurationResourceManagementServices : ConfigurationBase
    {
        public string HeadcountListTitle { get; set; }
        public string RequestToHireVendorsListTitle { get; set; }
        public string RequestsListTitle { get; set; }
        public string AllocationsListTitle { get; set; }
        public string SectionsListTitle { get; set; }
        public string TimesheetPendingToReportManagementGroup { get; set; }
        public string TimesheetPendingToApproveManagementGroup { get; set; }
        public List<string> EvaluationViewersGroups { get; set; }

        public DateTime TimesheetDeployDate { get; set; }
        public string TimesheetNewUrl { get; set; }
        public string TimesheetApproveUrl { get; set; }
        public string EvaluationSubmitUrl { get; set; }
        public string EvaluationListUrl { get; set; }

        public string MailServer { get; set; }
        public int? MailPort { get; set; }
        public string MailUsername { get; set; }
        public string MailPassword { get; set; }
        public string MailFromAddress { get; set; }

        public bool DebugTimesheetEmails { get; set; }
        public string DebugTimesheetEmailAddress { get; set; }
        public bool DebugEvaluationEmails { get; set; }
        public string DebugEvaluationEmailAddress { get; set; }
        public bool DebugAllocationEmails { get; set; }
        public string DebugAllocationEmailAddress { get; set; }

        /// Checks whether the configuration data is valid
        public override bool IsValid()
        {
            if (base.IsBaseValid())
            {
                if (
                    !string.IsNullOrEmpty(HeadcountListTitle) &&
                    !string.IsNullOrEmpty(RequestToHireVendorsListTitle) &&
                    !string.IsNullOrEmpty(RequestsListTitle) &&
                    !string.IsNullOrEmpty(AllocationsListTitle) &&
                    !string.IsNullOrEmpty(TimesheetPendingToReportManagementGroup) &&
                    !string.IsNullOrEmpty(TimesheetPendingToApproveManagementGroup) &&
                    EvaluationViewersGroups.Count > 0 &&
                    !string.IsNullOrEmpty(SectionsListTitle) &&
                    !string.IsNullOrEmpty(this.MailServer) &&
                    (this.MailPort.HasValue && this.MailPort.Value > 0) &&
                    !string.IsNullOrEmpty(this.MailUsername) &&
                    !string.IsNullOrEmpty(this.MailPassword) &&
                    !string.IsNullOrEmpty(this.MailFromAddress) &&
                    !string.IsNullOrEmpty(this.DebugTimesheetEmailAddress) &&
                    !string.IsNullOrEmpty(this.DebugEvaluationEmailAddress) &&
                    !string.IsNullOrEmpty(this.DebugAllocationEmailAddress) &&
                    !string.IsNullOrEmpty(this.TimesheetNewUrl) &&
                    !string.IsNullOrEmpty(this.TimesheetApproveUrl) &&
                    !string.IsNullOrEmpty(this.EvaluationListUrl) &&
                    !string.IsNullOrEmpty(this.EvaluationSubmitUrl)
                    )
                {
                    return true;
                }
            }
            return false;
        }

        /// Constructor for the class. Tries to retrieve Configuration Data from AppSettings
        public ConfigurationResourceManagementServices()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;

                DebugTimesheetEmails = bool.Parse(appSettings["DebugTimesheetEmails"]);
                DebugEvaluationEmails = bool.Parse(appSettings["DebugEvaluationEmails"]);
                DebugAllocationEmails = bool.Parse(appSettings["DebugAllocationEmails"]);
                HeadcountListTitle = appSettings[GlobalConstants.HeadcountListTitleAppSetting];
                RequestToHireVendorsListTitle = appSettings["RequestToHireVendorsListTitle"];
                RequestsListTitle = appSettings[GlobalConstants.RequestsListTitleAppSetting];
                AllocationsListTitle = appSettings[GlobalConstants.AllocationsListTitleAppSetting];
                SectionsListTitle = appSettings[GlobalConstants.SectionsListTitleAppSetting];
                TimesheetPendingToReportManagementGroup = appSettings[GlobalConstants.TimesheetPendingToReportManagementGroupAppSetting];
                TimesheetPendingToApproveManagementGroup = appSettings["TimesheetPendingToApproveManagementGroup"];
                string _evaluationViewersGroups = appSettings["EvaluationViewersGroups"];
                EvaluationViewersGroups = _evaluationViewersGroups.Split(new char[] { '|' }).ToList();

                TimesheetDeployDate = DateTime.Parse(appSettings[GlobalConstants.TimesheetDeployDateAppSetting]);
                TimesheetNewUrl = appSettings["TimesheetNewUrl"];
                TimesheetApproveUrl = appSettings["TimesheetApproveUrl"];
                EvaluationListUrl = appSettings["EvaluationListUrl"];
                EvaluationSubmitUrl = appSettings["EvaluationSubmitUrl"];

                this.MailServer = appSettings["MailServer"];
                int value;
                if (Int32.TryParse(appSettings["MailPort"], out value))
                {
                    this.MailPort = value;
                }
                this.MailUsername = appSettings["MailUsername"];
                this.MailPassword = appSettings["MailPassword"];
                this.MailFromAddress = appSettings["MailFromAddress"];

                this.DebugTimesheetEmailAddress = appSettings["DebugTimesheetEmailAddress"];
                this.DebugEvaluationEmailAddress = appSettings["DebugEvaluationEmailAddress"];
                this.DebugAllocationEmailAddress = appSettings["DebugAllocationEmailAddress"];
            }
            catch (Exception configEx)
            {
                LogController.LogMessage(1, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("ConfigurationResourceManagementServices {0}", configEx.StackTrace));

                //Log
            }
        }
    }
}
