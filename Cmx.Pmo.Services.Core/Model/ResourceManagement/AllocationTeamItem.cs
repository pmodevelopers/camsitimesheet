﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    public class AllocationTeamItem
    {
        public int ID { get; set; }
        public FieldLookupValue Team { get; set; }
        public FieldLookupValue Project_x0020_Assigned_x0020_Nam { get; set; }
    }
}
