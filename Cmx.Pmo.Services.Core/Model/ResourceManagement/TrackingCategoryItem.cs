﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    public class TrackingCategoryItem
    {
        public int ID { get; set; }
        public FieldLookupValue Category { get; set; }
    }
}
