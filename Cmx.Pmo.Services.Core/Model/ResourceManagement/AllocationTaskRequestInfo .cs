﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    public class AllocationTaskRequestInfo
    {
        public bool ProcessFinishedAllocations { get; set; }
        public bool NotifyEvaluators { get; set; }
        public bool NotifyAllocationsExpirations { get; set; }
    }
}
