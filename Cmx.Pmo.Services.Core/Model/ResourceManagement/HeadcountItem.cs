﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    //Deprecated
    public class HeadcountItem
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string Last_x0020_Name { get; set; }
        public string Full_x0020_Name { get; set; }
        public string Contract_x0020_Type { get; set; }
        public string Employee_x0020_Type { get; set; }
        public string Financial_x0020_Type { get; set; }
        public string Headcount_x0020_Type { get; set; }
        public string Resource_x0020_Status { get; set; }
    }
}
