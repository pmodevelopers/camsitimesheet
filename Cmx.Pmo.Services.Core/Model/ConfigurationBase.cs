﻿using Cmx.Pmo.Services.Core.Constants;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ResourceManagement
{
    public abstract class ConfigurationBase
    {
        public string TenantUrl { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        //_layouts/15/appprincipals.aspx
        //App Identifier
        //i:0i.t|ms.sp.ext|{clientId}@{realmId}
        public string RealmId { get; set; }

        public bool IsBaseValid()
        {
            if (!string.IsNullOrEmpty(TenantUrl)
                && !string.IsNullOrEmpty(ClientId)
                && !string.IsNullOrEmpty(ClientSecret)
                && !string.IsNullOrEmpty(RealmId))
            {
                return true;
            }
            return false;
        }

        public virtual bool IsValid()
        {
            return IsBaseValid();
        }

        public ConfigurationBase()
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                TenantUrl = appSettings[GlobalConstants.TenantUrlAppSetting];
                ClientId = appSettings[GlobalConstants.ClientIdAppSetting];
                ClientSecret = appSettings[GlobalConstants.ClientSecretAppSetting];
                RealmId = appSettings[GlobalConstants.RealmIdAppSetting];
            }
            catch (Exception configEx)
            {
                //Log
            }

        }

    }
}
