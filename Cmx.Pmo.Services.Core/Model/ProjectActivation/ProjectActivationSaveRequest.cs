﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.ProjectActivation
{
    public class ProjectActivationSaveRequest
    {
        public string LoginName { get; set; }

        public string ProjectLongID { get; set; }

        //project
        public int ProjectLeaderUserIdInWeb { get; set; }
        public int ProjectManagerUserIdInWeb { get; set; }

        //document
        public string DocumentCategory { get; set; }
        public string DocumentName { get; set; }
        public byte[] DocumentContents { get; set; }

        //track
        public int TrackItemId { get; set; }
        public string TrackName { get; set; }
        public int[] TrackStartDateUp { get; set; }
        public int[] TrackEndDateUp { get; set; }

        //milestone
        public int MilestoneItemId { get; set; }
        public string MilestoneName { get; set; }
        public int? MilesrtoneCountryID { get; set; }
        public int[] MilestoneStartDateUp { get; set; }

        //budget
        public string BudgetItemType { get; set; }
        public string BudgetItemName { get; set; }
        public int BudgetYear { get; set; }
        public int BudgetMonth { get; set; }
        public decimal BudgetValue { get; set; }
    }
}
