﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class TrackTimeWeeksBasicResponse
    {
        public List<WeekBasicInfo> Weeks { get; set; }
        public List<TrackTimeWeekHoursResponse> WeeksAvg { get; set; }
        public TrackTimeHeadcountResponse MonthsAvg { get; set; }
    }
}
