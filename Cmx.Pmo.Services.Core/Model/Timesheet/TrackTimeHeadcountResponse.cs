﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class TrackTimeHeadcountResponse
    {
        public int HeadcountID { get; set; }
        //public int AreaID { get; set; } //for future use
        public decimal[] MonthHours { get; set; }
        public List<TrackableItemInfo> TrackableItemHours { get; set; }

    }
}
