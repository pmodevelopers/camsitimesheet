﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class WeekTaskRequestInfo
    {
        public bool CalculateMissingWeeks { get; set; }
        public bool NotifyUsers { get; set; }
        public bool NotifyRMs { get; set; }
        public bool IsActive { get; set; }
        public int? StartNotifyingFromHeadcountID { get; set; }
    }
}
