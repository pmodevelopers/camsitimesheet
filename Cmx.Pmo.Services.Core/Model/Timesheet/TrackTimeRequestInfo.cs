﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class TrackTimeRequestInfo
    {
        public int HeadcountID { get; set; }
        public int[] HeadcountsID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int[] StartDateUp { get; set; }
        public int[] EndDateUp { get; set; }
        public int Year { get; set; }
        public int AreaID { get; set; }
        public int[] StatusID { get; set; }
        public int[] ProgramID { get; set; }
        public int[] ProjectListItemID { get; set; }
        public int[] LocationID { get; set; }
        public string Manager { get; set; }
        public bool IncludeManager { get; set; }
    }
}
