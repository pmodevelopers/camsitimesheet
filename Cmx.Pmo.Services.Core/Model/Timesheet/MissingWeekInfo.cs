﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class MissingWeekInfo
    {
        public int HeadcountID { get; set; }
        public int[] LastWeekSubmitted { get; set; }
        public List<int[]> Weeks { get; set; }
    }
}
