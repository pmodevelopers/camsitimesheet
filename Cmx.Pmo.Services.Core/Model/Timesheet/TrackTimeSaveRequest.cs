﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class TrackTimeSaveRequest
    {
        public int HeadcountId { get; set; }
        public string Sender { get; set; }
        public string Manager { get; set; }
        public WeekInfo Week { get; set; }
    }
}
