﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class TrackTimeMissingWeeksResponse
    {
        public bool Status9 { get; set; }
        public List<MissingWeekInfo> Headcounts { get; set; }
    }
}
