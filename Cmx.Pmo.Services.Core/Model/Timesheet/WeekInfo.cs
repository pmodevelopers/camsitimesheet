﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class WeekInfo
    {
        public int HeadcountID { get; set; }
        public WeekStatus Status { get; set; }
        public int[] StartDateDown { get; set; }
        public int[] StartDateUp { get; set; }
        public DateTime StartDateParsed { get; set; }
        public string Comments { get; set; }
        public List<TrackableItemInfo> TrackableItemsInfo { get; set; }
    }
}
