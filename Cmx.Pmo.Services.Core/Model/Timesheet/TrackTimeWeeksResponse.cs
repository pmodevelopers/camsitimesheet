﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class TrackTimeWeeksResponse
    {
        public int[] StartDateADWeek { get; set; }
        public int[] StartDateFutureWeek { get; set; }
        public List<WeekInfo> Weeks { get; set; }
    }
}
