﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class WeekBasicInfo
    {
        public int HeadcountID { get; set; }
        public string Manager { get; set; }
        public WeekStatus Status { get; set; }
        public int[] StartDateDown { get; set; }
        public decimal TotalHours { get; set; }
        public decimal[] Hours { get; set; }
    }
}
