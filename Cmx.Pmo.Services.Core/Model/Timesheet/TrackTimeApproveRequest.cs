﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class TrackTimeApproveRequest
    {
        public bool IsRejected { get; set; }
        public WeekStatus Status { get; set; }
        public string Comments { get; set; }
        public List<int[]> Weeks { get; set; }
        public string Approver { get; set; }
    }
}
