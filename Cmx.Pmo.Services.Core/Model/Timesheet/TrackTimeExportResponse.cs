﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class TrackTimeExportResponse
    {
        public List<TrackTimeExportItemInfo> Items { get; set; }
    }
}
