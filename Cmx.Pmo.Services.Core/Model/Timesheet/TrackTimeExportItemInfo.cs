﻿using Cmx.Pmo.SQL.Enums.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class TrackTimeExportItemInfo
    {
        public int HeadcountID { get; set; }
        //public int AreaID { get; set; }

        public TrackingType Type { get; set; }
        public int ListItemID { get; set; }
        public int? ProgramID { get; set; }
        public int? ProjectID { get; set; }
        public int? AreaID { get; set; }
        public int? CountryID { get; set; }


        public DateTime TrackedDate { get; set; }
        public decimal Hours { get; set; }
    }
}
