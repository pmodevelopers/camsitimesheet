﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class CountryInfo
    {
        public int? CountryID { get; set; }
        public decimal[] Hours { get; set; }
    }
}
