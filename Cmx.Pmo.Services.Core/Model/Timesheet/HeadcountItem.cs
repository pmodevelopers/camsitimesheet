﻿using Cmx.Pmo.SQL.Model.ResourceManagement;
using Cmx.Pmo.SQL.Model.Timesheet;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class HeadcountItem
    {
        public int ID { get; set; }
        public DateTime? Hire_x0020_Date { get; set; }
        public DateTime? Finish_x0020_Date { get; set; }
        public FieldUserValue Resource_x0020_Manager { get; set; }
        public string ResourceManagerLoginName { get; set; }
        public FieldUserValue HeadcountAccount { get; set; }
        public string EmailSecondary { get; set; }
        public string EMail { get; set; }
        public string FirstName { get; set; }
        public string Full_x0020_Name { get; set; }
        public string LoginName { get; set; }

        public string Last_x0020_Name { get; set; }
        public string Cemex_x0020_ID { get; set; }
        public string Contact_x0020_Number { get; set; }
        public string Employee_x0020_Type { get; set; }
        public string Headcount_x0020_Type { get; set; }
        public string Financial_x0020_Type { get; set; }
        public string CEMEX_x0020_or_x0020_NEORIS_x002 { get; set; }
        public double? Monthly_x0020_Cost_x0020_KUSD { get; set; }
        public string Resource_x0020_Status { get; set; }
        public string Contract_x0020_Type { get; set; }

        public string ResourceAnexo { get; set; }
        public string ResourceFCC { get; set; }
        public string ResourceRITM { get; set; }
        public string ResourceCNTR { get; set; }

        public double? Hrs_x0020_per_x0020_day { get; set; }
        public DateTime? DOB { get; set; }
        public string Skills { get; set; }
        public FieldLookupValue Source_x0020_Company { get; set; }
        public FieldLookupValue HeadCount_x0020_Position { get; set; }
        public FieldLookupValue Pit_x0020_Area { get; set; }
        public FieldLookupValue Related_x0020_Role { get; set; }
        public FieldLookupValue Location_x0020_Country { get; set; }
        public FieldLookupValue SeniorityLevel { get; set; }

        public int? Source_x0020_CompanyID { get; set; }
        public int? HeadCount_x0020_PositionID { get; set; }
        public int? Pit_x0020_AreaID { get; set; }
        public int? Related_x0020_RoleID { get; set; }
        public int? Location_x0020_CountryID { get; set; }
        public int? SeniorityLevelID { get; set; }

        public string Source_x0020_CompanyValue { get; set; }
        public string HeadCount_x0020_PositionValue { get; set; }
        public string Pit_x0020_AreaValue { get; set; }
        public string Related_x0020_RoleValue { get; set; }
        public string Location_x0020_CountryValue { get; set; }
        public string SeniorityLevelValue { get; set; }

        public bool MustReportHours { get; set; }
        public bool MustReceiveNotifications { get; set; }
        public double? HourRate { get; set; }
        public string ContractNumber { get; set; }
        public double? Rating { get; set; }

        public FieldUserValue Editor { get; set; }
        public string EditorLoginName { get; set; }

        public HeadcountRecord CreateRecordInstance()
        {
            if (Source_x0020_Company != null)
            {
                this.Source_x0020_CompanyID = Source_x0020_Company.LookupId;
                this.Source_x0020_CompanyValue = Source_x0020_Company.LookupValue;
            }
            if (HeadCount_x0020_Position != null)
            {
                this.HeadCount_x0020_PositionID = HeadCount_x0020_Position.LookupId;
                this.HeadCount_x0020_PositionValue = HeadCount_x0020_Position.LookupValue;
            }
            if (Pit_x0020_Area != null)
            {
                this.Pit_x0020_AreaID = Pit_x0020_Area.LookupId;
                this.Pit_x0020_AreaValue = Pit_x0020_Area.LookupValue;
            }
            if (Related_x0020_Role != null)
            {
                this.Related_x0020_RoleID = Related_x0020_Role.LookupId;
                this.Related_x0020_RoleValue = Related_x0020_Role.LookupValue;
            }
            if (Location_x0020_Country != null)
            {
                this.Location_x0020_CountryID = Location_x0020_Country.LookupId;
                this.Location_x0020_CountryValue = Location_x0020_Country.LookupValue;
            }
            if (SeniorityLevel != null)
            {
                this.SeniorityLevelID = SeniorityLevel.LookupId;
                this.SeniorityLevelValue = SeniorityLevel.LookupValue;
            }

            HeadcountRecord record = new HeadcountRecord();

            record.HeadcountID = this.ID;
            record.FirstName = this.FirstName;
            record.LastName = this.Last_x0020_Name;
            record.DOB = this.DOB;
            record.EMail = this.EMail;
            record.ContactNumber = this.Contact_x0020_Number;
            record.CemexID = this.Cemex_x0020_ID;
            record.PayrollNumber = this.CEMEX_x0020_or_x0020_NEORIS_x002;
            record.LoginName = this.LoginName;

            record.ResourceAnexo = this.ResourceAnexo;
            record.ResourceFCC = this.ResourceFCC;
            record.ResourceRITM = this.ResourceRITM;
            record.ResourceCNTR = this.ResourceCNTR;

            record.Skills = this.Skills;
            record.ContractNumber = this.ContractNumber;
            record.HireDate = this.Hire_x0020_Date;
            record.FinishDate = this.Finish_x0020_Date;
            record.HourRate = (decimal?)this.HourRate;
            record.MonthlyCostUSD = (decimal?)this.Monthly_x0020_Cost_x0020_KUSD;
            record.HoursPerDay = (decimal?)this.Hrs_x0020_per_x0020_day;
            record.ResourceManagerLoginName = this.ResourceManagerLoginName;
            record.Rating = (decimal?)this.Rating;
            record.ResourceStatus = this.Resource_x0020_Status;
            record.ContractType = this.Contract_x0020_Type;

            record.EmployeeType = this.Employee_x0020_Type;
            record.HeadcountType = this.Headcount_x0020_Type;
            record.FinancialType = this.Financial_x0020_Type;

            record.SourceCompanyID = this.Source_x0020_CompanyID;
            record.SourceCompanyValue = this.Source_x0020_CompanyValue;

            record.HeadCountPositionID = this.HeadCount_x0020_PositionID;
            record.HeadCountPositionValue = this.HeadCount_x0020_PositionValue;

            record.PitAreaID = this.Pit_x0020_AreaID;
            record.PitAreaValue = this.Pit_x0020_AreaValue;

            record.RelatedRoleID = this.Related_x0020_RoleID;
            record.RelatedRoleValue = this.Related_x0020_RoleValue;

            record.LocationCountryID = this.Location_x0020_CountryID;
            record.LocationCountryValue = this.Location_x0020_CountryValue;

            record.SeniorityLevelID = this.SeniorityLevelID;
            record.SeniorityLevelValue = this.SeniorityLevelValue;

            record.MustReportHours = this.MustReportHours;
            record.MustReceiveNotifications = this.MustReceiveNotifications;

            record.ModifiedBy = this.EditorLoginName;

            record.ValidFrom = DateTime.Now;

            return record;
        }

    }
}
