﻿using Cmx.Pmo.SQL.Model.ResourceManagement;
using Cmx.Pmo.SQL.Model.Timesheet;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Timesheet
{
    public class RequestToHireVendorItem
    {
        public int ID { get; set; }
        public FieldUserValue Vendor_x0020_Security_x0020_Grou { get; set; }
        public string Title { get; set; }
        public string VendorGroup { get; set; }

        public RequestToHireVendorItem()
        {
            this.VendorGroup = string.Empty;
        }
    }
}
