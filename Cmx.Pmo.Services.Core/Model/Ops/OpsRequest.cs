﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Model.Ops
{
    public class OpsRequest
    {
        public string LoginName { get; set; }
        public string Message { get; set; }
        public string Page { get; set; }
        public string Method { get; set; }
    }
}
