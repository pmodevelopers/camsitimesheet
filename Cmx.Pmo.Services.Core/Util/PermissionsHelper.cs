﻿using Cmx.Pmo.Services.Core.Constants;
using Cmx.Pmo.Services.Core.Controllers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Util
{
    public class PermissionsHelper
    {
        /// <summary>
        /// Gets the members of a sharepoint online group
        /// </summary>
        /// <param name="context">active sharepoint client context</param>
        /// <param name="siteGroups">GroupCollection already loaded</param>
        /// <param name="groupName">target group name</param>
        /// <returns>a list of users</returns>
        public static List<User> GetUsersFromSharepointGroup(ClientContext context, GroupCollection siteGroups, string groupName)
        {
            List<User> users = new List<User>();

            try
            {
                Group group = siteGroups.GetByName(groupName);
                context.Load(group.Users);
                context.ExecuteQuery();

                foreach (User member in group.Users)
                {
                    if (!member.Title.ToLower().Equals(GlobalConstants.SystemAccount.ToLower()))
                    {
                        users.Add(member);
                    }
                }
            }
            catch (Exception ex)
            {
                LogController.LogMessage(1, "GetUsersFromSharepointGroup", null, string.Format("{0}}", ex.Message));
            }

            return users;
        }

    }
}
