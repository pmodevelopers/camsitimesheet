﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Util.Model
{
    public class ExternalServiceResponse<T>
    {
        public bool Success { get; set; }

        public T Response { get; set; }

        public Exception Exception { get; set; }
    }
}
