﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cmx.Pmo.Services.Core.Util.Model;

namespace Cmx.Pmo.Services.Core.Util.Model
{
    public class SPQueryFilter
    {
        public string FieldName { get; set; }
        public string Value { get; set; }
        public SPFieldType FieldType { get; set; }
        public SPFieldComparisonType ComparisonType { get; set; }
    }
}
