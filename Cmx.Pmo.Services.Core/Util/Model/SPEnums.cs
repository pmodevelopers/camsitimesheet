﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Util.Model
{
    public enum SPFieldType
    {
        Integer,
        Number,
        DateTime,
        Choice
    }

    public enum SPFieldComparisonType
    {
        Eq,
        Neq,
        Gt,
        Geq,
        Lt,
        Leq
    }


    public enum QueryCondition
    {
        And,
        Or
    }
}
