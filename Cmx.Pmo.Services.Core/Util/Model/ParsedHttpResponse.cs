﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Util.Model
{
    public class ParsedHttpResponse
    {
        public Dictionary<string, string> Headers { get; set; }
        public string ResponseBody { get; set; }
        //public List<Cookie> Cookies { get; set; }
    }
}
