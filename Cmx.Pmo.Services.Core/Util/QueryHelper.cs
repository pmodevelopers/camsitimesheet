﻿using Cmx.Pmo.Services.Core.Controllers;
using Cmx.Pmo.Services.Core.Util.Model;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Util
{
    public class QueryHelper
    {
        //get a clientContext using clientId & clientSecret
        //requires generating those at appregnew.aspx and assign the necesary permissions at appinv.aspx
        //requires also AppConfig entries for ClientId, ClientSecret and RealmId
        public static ClientContext GetClientContext(string tenantUrl)
        {
            Uri webUri = new Uri(tenantUrl);

            //this hard-coded value won't change
            var SharePointPrincipalId = "00000003-0000-0ff1-ce00-000000000000";
            var token = TokenHelper.GetAppOnlyAccessToken(SharePointPrincipalId, webUri.Authority, null).AccessToken;
            var clientContext = TokenHelper.GetClientContextWithAccessToken(webUri.ToString(), token);

            return clientContext;
        }

        public static ListItemCollection QuerySharepointList(ClientContext context, string listTitle, string queryText, List<string> includeFields)
        {
            ListItemCollection items = null;

            try
            {
                List list = context.Web.Lists.GetByTitle(listTitle);
                CamlQuery query = new CamlQuery();
                query.ViewXml = queryText;
                //query = CamlQuery.CreateAllItemsQuery();
                items = list.GetItems(query);

                foreach (string fieldName in includeFields)
                {
                    context.Load(items, includes => includes.Include(i => i[fieldName]));
                }

                context.ExecuteQuery();
            }
            catch (Exception ex)
            {
                //Log.WriteError("Error while querying list", ex);
                throw ex;
            }

            return items;
        }

        public static User GetUserByLoginName(string tenantUrl, string loginName, bool includeGroups)
        {
            using (ClientContext tenantContext = QueryHelper.GetClientContext(tenantUrl))
            {
                try
                {
                    var sharePointUser = tenantContext.Web.EnsureUser(loginName);
                    tenantContext.Load(sharePointUser, user => user.Id);
                    tenantContext.Load(sharePointUser, user => user.LoginName);
                    tenantContext.Load(sharePointUser, user => user.Title);
                    tenantContext.Load(sharePointUser, user => user.Email);

                    if (includeGroups)
                    {
                        tenantContext.Load(sharePointUser, user => user.Groups);
                    }

                    tenantContext.ExecuteQuery();

                    return sharePointUser;
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(99, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Error: {0}", ex.Message));
                }
            }

            return null;
        }

        public static string BuildQuery(List<SPQueryFilter> filters, QueryCondition condition, int rowLimit)
        {
            string queryCondition = condition == QueryCondition.And ? "And" : "Or";
            string queryFormat = @"<View>
                                    <Query>
                                        <Where>
                                            {0}
                                        </Where>
                                    </Query>
                                    <RowLimit>{1}</RowLimit>
                                </View>";


            if (filters.Count > 0)
            {
                StringBuilder queryFull = new StringBuilder();
                string querySegmentCurrent = null;
                for(int z = 0; z < filters.Count; z++)
                {
                    string queryItem = string.Format(@"<{0}>
                                                <FieldRef Name='{1}' />
                                                <Value Type='{2}'>{3}</Value> 
                                            </{0}>", filters[z].ComparisonType.ToString(), filters[z].FieldName, filters[z].FieldType.ToString(), filters[z].Value);

                    if (querySegmentCurrent == null)
                    {
                        querySegmentCurrent = filters.Count == 1 ? string.Format("{0}", queryItem) : queryItem;
                    }
                    else
                    {
                        querySegmentCurrent = string.Format(@"<{0}>
                                                                {1}
                                                                {2}
                                                            </{0}>", queryCondition, queryItem, querySegmentCurrent);
                    }

                    //last one?
                    if((z + 1) == filters.Count)
                    {
                        queryFull.Append(string.Format(queryFormat, querySegmentCurrent, rowLimit));
                    }
                }

                return queryFull.ToString();
            }

            return string.Empty;
        }
    }
}
