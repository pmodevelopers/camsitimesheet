﻿using Cmx.Pmo.Services.Core.Util.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Cmx.Pmo.Services.Core.Util
{
    public class HttpHelper
    {

        public static ExternalServiceResponse<ParsedHttpResponse> DoRequest(string url, string method,  Dictionary<string, string> requestHeaders = null, Dictionary<string, string> cookies = null, string cookieHost = null, string body = null, List<int> validResponseCodes = null)
        {
            ExternalServiceResponse<ParsedHttpResponse> result = new ExternalServiceResponse<ParsedHttpResponse> { Success = false };

            HttpWebResponse response = null;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method;
                //add credentials
                //request.Credentials = CredentialCache.DefaultCredentials;

                //set Headers
                if (requestHeaders != null && requestHeaders.Count > 0)
                {                    
                    foreach (var header in requestHeaders)
                    {
                        if (header.Key.ToLower().Equals("referer"))
                        {
                            request.Referer = header.Value;
                        }
                        else if (header.Key.ToLower().Equals("user-agent"))
                        {
                            request.UserAgent = header.Value;
                        }
                        else if (header.Key.ToLower().Equals("content-length"))
                        {
                            request.ContentLength = Convert.ToInt64(header.Value);
                        }
                        else
                        {
                            if (Array.Exists(request.Headers.AllKeys, x => x.ToLower().Equals(header.Key.ToLower())))
                            {
                                request.Headers[header.Key] = header.Value;
                            }
                            else
                            {
                                request.Headers.Add(header.Key.ToString(), header.Value);
                            }
                        }
                    }                    
                }

                if (cookies != null && cookies.Count > 0)
                {
                    request.CookieContainer = new CookieContainer();

                    foreach (var cookie in cookies)
                    {
                        if (!string.IsNullOrEmpty(cookieHost))
                        {
                            request.CookieContainer.Add(new Uri(cookieHost), new Cookie(cookie.Key, cookie.Value));
                        }
                        else
                        {
                            request.CookieContainer.Add(new Cookie(cookie.Key, cookie.Value));
                        }
                    }
                }

                if (!string.IsNullOrEmpty(body))
                {
                    var data = Encoding.ASCII.GetBytes(body);

                    request.Method = "POST";
                    request.ContentType = "application/json";
                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }

                //make call
                response = (HttpWebResponse)request.GetResponse();

                if (IsResponseValid(validResponseCodes, response.StatusCode))
                {
                    result.Response = new ParsedHttpResponse();

                    Stream responseStream = null;
                    StreamReader responseReader = null;

                    try
                    {
                        responseStream = response.GetResponseStream();
                        responseReader = new StreamReader(responseStream);
                        result.Response.ResponseBody = responseReader.ReadToEnd();
                        result.Success = true;
                    }
                    catch (Exception httpEx)
                    {
                        result.Exception = httpEx;
                    }
                    finally
                    {
                        if (responseReader != null)
                            responseReader.Dispose();
                        if (responseStream != null)
                            responseStream.Dispose();
                    }
                }
                else
                {
                    result.Exception = new ApplicationException("Server responded with an unexpected status code: " + response.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                result.Exception = ex;
            }
            finally
            {
                if (response != null)
                {
                    response.Dispose();
                }
            }

            return result;
        }

        private static bool IsResponseValid(List<int> validResponseCodes, HttpStatusCode actualResponseCode)
        {
            if (validResponseCodes == null)
            {
                //default value
                validResponseCodes = new List<int>() { (int)HttpStatusCode.OK };
            }

            bool responseConsideredValid = false;

            //compare passed list of valid response codes against actual response code
            foreach (int responseCode in validResponseCodes)
            {
                if ((HttpStatusCode)responseCode == actualResponseCode)
                {
                    responseConsideredValid = true;
                    break;
                }
            }

            return responseConsideredValid;
        }
    }
}
