﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel;

namespace Cmx.Pmo.Services.Core.Util
{
    public class EntityHelper
    {
        //converts a ListItem to an instance of T 
        public static T GetInstance<T>(ListItem listItem, List<string> fields)
        {
            T item = Activator.CreateInstance<T>();

            Type type = item.GetType();
            PropertyInfo[] propiedades = type.GetProperties();

            foreach (PropertyInfo prop in propiedades)
            {
                foreach (string fieldName in fields)
                {
                    if (fieldName == prop.Name)
                    {
                        if (listItem[fieldName] != null)
                        {
                            if (listItem[fieldName] is Microsoft.SharePoint.Client.FieldUserValue)
                            {
                                Microsoft.SharePoint.Client.FieldUserValue lookupValue = (Microsoft.SharePoint.Client.FieldUserValue)listItem[fieldName];
                                prop.SetValue(item, Convert.ChangeType(lookupValue, prop.PropertyType), null);
                            }
                            else if (listItem[fieldName] is Microsoft.SharePoint.Client.FieldLookupValue)
                            {
                                Microsoft.SharePoint.Client.FieldLookupValue lookupValue = (Microsoft.SharePoint.Client.FieldLookupValue)listItem[fieldName];
                                prop.SetValue(item, Convert.ChangeType(lookupValue, prop.PropertyType), null);
                            }
                            else
                            {
                                if (prop.PropertyType.FullName.Contains("Nullable"))
                                {
                                    if (prop.PropertyType.FullName.Contains("Char"))
                                    {
                                        //si el Tipo de la propiedad es Nullable y Char, explicitamente se convierte el valor del row al Tipo especifico de la propiedad
                                        //prop.SetValue(item, System.Char.Parse(row[prop.Name].ToString()), null);
                                    }
                                    else
                                    {
                                        //si el Tipo de la propiedad es Nullable, se asigna directamente el valor. Es seguro porque los Tipos que pueden ser nullable son los Value Types
                                        prop.SetValue(item, listItem[fieldName], null);
                                    }
                                }
                                else
                                {
                                    prop.SetValue(item, Convert.ChangeType(listItem[fieldName], prop.PropertyType), null);
                                }
                            }
                        }
                        break;
                    }
                }
            }

            return item;
        }
    }
}
