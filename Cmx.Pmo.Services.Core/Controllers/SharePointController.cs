﻿using Cmx.Pmo.Services.Core.Constants;
using Cmx.Pmo.Services.Core.Enums;
using Cmx.Pmo.Services.Core.Model.ResourceManagement;
using Cmx.Pmo.Services.Core.Util;
using Cmx.Pmo.Services.Core.Util.Model;
using Cmx.Pmo.SQL.Model;
using Cmx.Pmo.SQL.Repository;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Controllers
{
    public class SharePointController : BaseController
    {
        private new ControllerID ControllerId = ControllerID.SharePoint;
        private int QueryRowLimit = 5000;

        private List<string> allocationDefaultFields = new List<string> { "ID" };

        public List<AllocationItem> GetAllocations(KeyValuePair<string, string> filter, SPFieldType filterType, List<string> fields = null)
        {
            if (ConfigurationData != null)
            {
                var filters = new List<SPQueryFilter> { new SPQueryFilter { FieldName = filter.Key, FieldType = filterType, Value = filter.Value } };

                return this.GetAllocations(filters, fields);
            }

            return null;
        }

        public List<AllocationItem> GetAllocations(List<SPQueryFilter> filters, List<string> fields = null, int? rowLimit = null, bool getManagerDetails = false)
        {
            if (ConfigurationData != null)
            {
                try
                {
                    using (ClientContext tenantContext = QueryHelper.GetClientContext(ConfigurationData.TenantUrl))
                    {
                        string query = QueryHelper.BuildQuery(filters, QueryCondition.And, rowLimit.HasValue ? rowLimit.Value : this.QueryRowLimit);
                        if (!string.IsNullOrEmpty(query))
                        {
                            List<string> queryFields = this.allocationDefaultFields;
                            if (fields != null)
                            {
                                queryFields = fields;
                            }

                            ListItemCollection queryItems = QueryHelper.QuerySharepointList(tenantContext, ConfigurationData.AllocationsListTitle, query, queryFields);

                            if (queryItems != null)
                            {
                                List<AllocationItem> items = new List<AllocationItem>();

                                foreach (ListItem listItem in queryItems)
                                {
                                    AllocationItem item = EntityHelper.GetInstance<AllocationItem>(listItem, queryFields);

                                    if (getManagerDetails && item.Manager != null && !string.IsNullOrEmpty(item.Manager.Email))
                                    {
                                        ClientResult<PrincipalInfo> principal = Utility.ResolvePrincipal(tenantContext, tenantContext.Web, item.Manager.Email, PrincipalType.User, PrincipalSource.All, tenantContext.Web.SiteUsers, true);
                                        tenantContext.ExecuteQuery();

                                        if (principal.Value != null)
                                        {
                                            item.ManagerLoginName = principal.Value.LoginName.Replace(GlobalConstants.LoginNameString, string.Empty);

                                            items.Add(item);
                                        }
                                        else
                                        {
                                            LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                                            string.Format("Could not resolve Manager Login Name {0}", item.Manager.Email));
                                        }
                                    }
                                    else
                                    {
                                        items.Add(item);
                                    }
                                }

                                return items;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Log
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("{0}", ex.Message));
                }
            }

            return null;
        }

        public bool UpdateListItem(string ListTitle, int ListItemID, KeyValuePair<string, string> data)
        {
            if (ConfigurationData != null)
            {
                try
                {
                    using (ClientContext tenantContext = QueryHelper.GetClientContext(ConfigurationData.TenantUrl))
                    {
                        List list = tenantContext.Web.Lists.GetByTitle(ListTitle);

                        ListItem listItem = list.GetItemById(ListItemID);
                        listItem[data.Key] = data.Value;
                        listItem.Update();

                        tenantContext.ExecuteQuery();

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("List: {0}  ID: {1} => {2}", ListTitle, ListItemID, ex.Message));
                }
            }

            return false;
        }

        public bool UpdateListItem(string ListTitle, int ListItemID, List<KeyValuePair<string, string>> data)
        {
            if (ConfigurationData != null)
            {
                try
                {
                    using (ClientContext tenantContext = QueryHelper.GetClientContext(ConfigurationData.TenantUrl))
                    {
                        List list = tenantContext.Web.Lists.GetByTitle(ListTitle);

                        ListItem listItem = list.GetItemById(ListItemID);

                        foreach (var _entry in data)
                        {
                            listItem[_entry.Key] = _entry.Value;
                        }

                        //listItem.Update();
                        listItem.UpdateOverwriteVersion();

                        tenantContext.ExecuteQuery();

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("List: {0}  ID: {1} => {2}", ListTitle, ListItemID, ex.Message));
                }
            }

            return false;
        }


        public bool InsertListItem(string ListTitle, List<KeyValuePair<string, string>> data)
        {
            if (ConfigurationData != null)
            {
                try
                {
                    using (ClientContext tenantContext = QueryHelper.GetClientContext(ConfigurationData.TenantUrl))
                    {
                        List list = tenantContext.Web.Lists.GetByTitle(ListTitle);

                        ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();

                        ListItem listItem = list.AddItem(itemCreateInfo);

                        foreach (var _entry in data)
                        {
                            listItem[_entry.Key] = _entry.Value;
                        }

                        listItem.Update();

                        tenantContext.ExecuteQuery();

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("List: {0} => {1}", ListTitle, ex.Message));
                }
            }

            return false;
        }

        public bool InsertListItems(string ListTitle, List<List<KeyValuePair<string, object>>> data, int msDelayBetweenInsertions = 0)
        {
            if (ConfigurationData != null && data.Count > 0)
            {
                try
                {
                    using (ClientContext tenantContext = QueryHelper.GetClientContext(ConfigurationData.TenantUrl))
                    {
                        List list = tenantContext.Web.Lists.GetByTitle(ListTitle);

                        foreach (var newItemData in data)
                        {
                            try
                            {
                                ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();

                                ListItem listItem = list.AddItem(itemCreateInfo);

                                foreach (var _entry in newItemData)
                                {
                                    listItem[_entry.Key] = _entry.Value;
                                }

                                listItem.Update();

                                tenantContext.ExecuteQuery();

                                if (msDelayBetweenInsertions > 0)
                                {
                                    System.Threading.Thread.Sleep(msDelayBetweenInsertions);
                                }
                            }
                            catch (Exception itemEx)
                            {
                                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Item no fue insertado. List: {0} => {1}", ListTitle, itemEx.Message));
                            }
                        }

                        return true;
                    }
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("List: {0} => {1}", ListTitle, ex.Message));
                }
            }

            return false;
        }

        public bool IsMemberOfSharePointGroup(string groupName, string loginName)
        {
            if (this.ConfigurationData != null)
            {
                using (ClientContext tenantContext = QueryHelper.GetClientContext(this.ConfigurationData.TenantUrl))
                {
                    try
                    {
                        GroupCollection siteGroups = tenantContext.Web.SiteGroups;

                        tenantContext.Load(siteGroups);
                        tenantContext.ExecuteQuery();

                        List<User> users = PermissionsHelper.GetUsersFromSharepointGroup(tenantContext, siteGroups, groupName);

                        if (users.Count > 0)
                        {
                            foreach (User user in users)
                            {
                                if (user.LoginName.ToLower().Equals(string.Format("{0}{1}", GlobalConstants.LoginNameString, loginName.ToLower())))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("{0}", ex.Message));
                    }
                }
            }

            return false;
        }
    }
}
