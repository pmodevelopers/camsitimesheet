﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Enums;
using Cmx.Pmo.Services.Core.Model.ResourceManagement;
using Cmx.Pmo.Services.Core.Util.Model;
using Cmx.Pmo.SQL.Model.Evaluations;
using Cmx.Pmo.SQL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Controllers.ResourceManagement
{
    public class EvaluationController : BaseController
    {
        private new ControllerID ControllerId = ControllerID.Evaluation;

        public static EvaluationRepository _repo = null;
        public static EvaluationRepository Repository
        {
            get
            {
                if (_repo == null)
                {
                    _repo = new EvaluationRepository();
                }

                return _repo;
            }
        }

        #region Emails Formats

        private string EvaluationNoticeEmailFormat = @"
        <html>
            <head>
                <meta http-equiv=""Content-Type"" content=""text/html; charset=us-ascii"">
            </head>
            <style>
                 .email-header {{
                    width: 100%;
                    background-color: #002A59;
                    border-bottom:5px red solid;
                    color:#fff;
                }}
                .email-footer{{
                    border: 0;
                    height: 25px;
                    width: 100%;
                    padding-left:20px;
                    background-color: #002A59;
                }}
                span > a, a:link,   span.MsoHyperlink {{
                    mso-style-noshow: yes;
                    mso-style-priority: 01;
                    text-decoration: none;
                }}
                a:visited, span > a,
                span.MsoHyperlinkFollowed {{
                    mso-style-noshow: yes;
                    mso-style-priority: 01;
                    text-decoration: none;
                }}
            </style>
            <body>
                <table cellpadding = ""0"" cellspacing=""0"" border=""0"" style=""padding:0px;margin:0px;width:100%;"">
                    <tr>
                         <td style = ""padding-left: 0px !important; padding-bottom: 0px !important;height: 48px!important;"" >
                            <div style=""height: 60px""class=""email-header"">
                                <img width = 100 height=50 style=""border:0;""   src=""https://eustdmyo001.blob.core.windows.net/pmo/CEMEX%20White.png"" >
                            </div>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            Good day,
                            <br/>
                            <br/>
                            The allocation of <b>{0}</b> as <b>{1}</b> in the <b>{2}</b> has ended.
                            <br/>
                            <br/>
                            We kindly ask you to provide us some feedback on his/her performance:
                            <br/>
                            <br/>
                            <span><a href=""{3}"">Evaluation for {0}</a></span><br/>
                            <br/>
                            <br/>
                            To review all your evaluations, please visit the <a href=""{4}"">Evaluation</a> section in our suite.
                            <br/>
                            <br/>
                            Best Regards,
                            <br/>
                            <br/>
                            Resource Management Team,
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td style = ""color:#fff!important;padding-left:0px!important;margin:0px;"" >
                            <div class=""email-footer"">
                                <p style = ""padding-top:3px!important;"" >Copyright © 2018, CEMEX International Holding AG.All rights reserved.</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
        ";

        private string EvaluationReminderEmailFormat = @"
        <html>
            <head>
                <meta http-equiv=""Content-Type"" content=""text/html; charset=us-ascii"">
            </head>
            <style>
                 .email-header {{
                    width: 100%;
                    background-color: #002A59;
                    border-bottom:5px red solid;
                    color:#fff;
                }}
                .email-footer{{
                    border: 0;
                    height: 25px;
                    width: 100%;
                    padding-left:20px;
                    background-color: #002A59;
                }}
                span > a, a:link,   span.MsoHyperlink {{
                    mso-style-noshow: yes;
                    mso-style-priority: 01;
                    text-decoration: none;
                }}
                a:visited, span > a,
                span.MsoHyperlinkFollowed {{
                    mso-style-noshow: yes;
                    mso-style-priority: 01;
                    text-decoration: none;
                }}
            </style>
            <body>
                <table cellpadding = ""0"" cellspacing=""0"" border=""0"" style=""padding:0px;margin:0px;width:100%;"">
                    <tr>
                         <td style = ""padding-left: 0px !important; padding-bottom: 0px !important;height: 48px!important;"" >
                            <div style=""height: 60px""class=""email-header"">
                                <img width = 100 height=50 style=""border:0;""   src=""https://eustdmyo001.blob.core.windows.net/pmo/CEMEX%20White.png"" >
                            </div>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            Good day,
                            <br/>
                            <br/>
                            This is a friendly reminder that you have <b>{0}</b> resource evaluations pending.
                            <br/>
                            <br/>
                            We would appreciate if you could submit them at your earliest convenience.
                            <br/>
                            <br/>
                            <br/>
                            Best Regards,
                            <br/>
                            <br/>
                            Resource Management Team,
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td style = ""color:#fff!important;padding-left:0px!important;margin:0px;"" >
                            <div class=""email-footer"">
                                <p style = ""padding-top:3px!important;"" >Copyright © 2018, CEMEX International Holding AG.All rights reserved.</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
        ";

        private string ExpirationFirstNoticeEmailFormat = @"
        <html>
            <head>
                <meta http-equiv=""Content-Type"" content=""text/html; charset=us-ascii"">
            </head>
            <style>
                 .email-header {{
                    width: 100%;
                    background-color: #002A59;
                    border-bottom:5px red solid;
                    color:#fff;
                }}
                .email-footer{{
                    border: 0;
                    height: 25px;
                    width: 100%;
                    padding-left:20px;
                    background-color: #002A59;
                }}
                span > a, a:link,   span.MsoHyperlink {{
                    mso-style-noshow: yes;
                    mso-style-priority: 01;
                    text-decoration: none;
                }}
                a:visited, span > a,
                span.MsoHyperlinkFollowed {{
                    mso-style-noshow: yes;
                    mso-style-priority: 01;
                    text-decoration: none;
                }}
            </style>
            <body>
                <table cellpadding = ""0"" cellspacing=""0"" border=""0"" style=""padding:0px;margin:0px;width:100%;"">
                    <tr>
                         <td style = ""padding-left: 0px !important; padding-bottom: 0px !important;height: 48px!important;"" >
                            <div style=""height: 60px""class=""email-header"">
                                <img width = 100 height=50 style=""border:0;""   src=""https://eustdmyo001.blob.core.windows.net/pmo/CEMEX%20White.png"" >
                            </div>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            Dear user,
                            <br/>
                            <br/>
                            Please note that the allocation of <b>{0}</b> to the <b>{1}</b> will expire in a few weeks, on <b>{2}</b>.
                            <br/>
                            <br/>
                            In case an extension is needed, please reply this email mentioning the new allocation end date.
                            <br/>
                            <br/>
                            You have TWO WEEKS from now to request the extension, otherwise the resource will start being considered for other projects and his contract will not be renewed.
                            <br/>
                            <br/>
                            Best Regards,
                            <br/>
                            <br/>
                            Resource Management Team,
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td style = ""color:#fff!important;padding-left:0px!important;margin:0px;"" >
                            <div class=""email-footer"">
                                <p style = ""padding-top:3px!important;"" >Copyright © 2018, CEMEX International Holding AG.All rights reserved.</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
        ";

        private string ExpirationFinalNoticeEmailFormat = @"
        <html>
            <head>
                <meta http-equiv=""Content-Type"" content=""text/html; charset=us-ascii"">
            </head>
            <style>
                 .email-header {{
                    width: 100%;
                    background-color: #002A59;
                    border-bottom:5px red solid;
                    color:#fff;
                }}
                .email-footer{{
                    border: 0;
                    height: 25px;
                    width: 100%;
                    padding-left:20px;
                    background-color: #002A59;
                }}
                span > a, a:link,   span.MsoHyperlink {{
                    mso-style-noshow: yes;
                    mso-style-priority: 01;
                    text-decoration: none;
                }}
                a:visited, span > a,
                span.MsoHyperlinkFollowed {{
                    mso-style-noshow: yes;
                    mso-style-priority: 01;
                    text-decoration: none;
                }}
            </style>
            <body>
                <table cellpadding = ""0"" cellspacing=""0"" border=""0"" style=""padding:0px;margin:0px;width:100%;"">
                    <tr>
                         <td style = ""padding-left: 0px !important; padding-bottom: 0px !important;height: 48px!important;"" >
                            <div style=""height: 60px""class=""email-header"">
                                <img width = 100 height=50 style=""border:0;""   src=""https://eustdmyo001.blob.core.windows.net/pmo/CEMEX%20White.png"" >
                            </div>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            Dear user,
                            <br/>
                            <br/>
                            As mentioned on the past notification, the allocation of <b>{0}</b> to the <b>{1}</b> will expire in a few weeks, on <b>{2}</b>.
                            <br/>
                            <br/>
                            As an allocation extension wasn't requested, we would like to confirm that the resource's allocation end date will remain the same and that his/her contract will not be extended.
                            <br/>
                            <br/>
                            Best Regards,
                            <br/>
                            <br/>
                            Resource Management Team,
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td style = ""color:#fff!important;padding-left:0px!important;margin:0px;"" >
                            <div class=""email-footer"">
                                <p style = ""padding-top:3px!important;"" >Copyright © 2018, CEMEX International Holding AG.All rights reserved.</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
        ";

        #endregion

        public void RunAllocationTasks(bool processFinishedAllocations, bool notifyEvaluators, bool notifyAllocationsExpirations)
        {
            LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                string.Format("RunEvaluationCreationTask running.  processFinishedAllocations:{0} ,  notifyEvaluators:{1} , notifyAllocationsExpirations:{1}", processFinishedAllocations, notifyEvaluators, notifyAllocationsExpirations));

            List<SPQueryFilter> filters = null;
            SharePointController spController = new SharePointController(); ;

            if (processFinishedAllocations)
            {
                //1. get all completed allocations

                //filter
                filters = new List<SPQueryFilter> {
                new SPQueryFilter {  FieldName = "Allocation_x0020_Type", FieldType = SPFieldType.Choice, Value = "Confirmed", ComparisonType = SPFieldComparisonType.Eq }
                , new SPQueryFilter {  FieldName = "EvaluationStatus", FieldType = SPFieldType.Choice, Value = "Created", ComparisonType = SPFieldComparisonType.Neq }
                , new SPQueryFilter {  FieldName = "EvaluationStatus", FieldType = SPFieldType.Choice, Value = "Submitted", ComparisonType = SPFieldComparisonType.Neq }
                //,new SPQueryFilter {  FieldName = "ID", FieldType = SPFieldType.Integer, Value = "129", ComparisonType = SPFieldComparisonType.Eq } //for testing
                , new SPQueryFilter {  FieldName = "Finish_x0020_Date", FieldType = SPFieldType.DateTime, Value = "<Today/>", ComparisonType = SPFieldComparisonType.Lt }
                };

                List<AllocationItem> allocations = spController.GetAllocations(filters, new List<string> { "ID", "Manager", "Resource_x0020_Name_x003a_Full_x", "Project_x0020_Assigned_x0020_Nam0", "Area_x0020_Assigned", "Role" }, null, true);
                if (allocations != null)
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("{0} allocations found", allocations.Count));

                    int allocationsCreated = 0;
                    int allocationsUpdated = 0;
                    //2. create records for each completed allocation
                    TimeTrackingRepository timeTrackingRepository = new TimeTrackingRepository();
                    foreach (var allocation in allocations)
                    {
                        if (allocation.HeadcountID.HasValue && !string.IsNullOrEmpty(allocation.ManagerLoginName))
                        {
                            //get program if allocation is project
                            int? programID = null;
                            if (allocation.ProjectListItemID.HasValue)
                            {
                                //if an error occurs allocation will be created but it wont be find it when filtered by program
                                try
                                {
                                    var timeTrackingItem = timeTrackingRepository.GetTrackingItem(allocation.HeadcountID.Value, allocation.ProjectListItemID.Value);
                                    if (timeTrackingItem.ProgramID.HasValue)
                                    {
                                        programID = timeTrackingItem.ProgramID;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                                }
                            }

                            int idAfterInsert = this.CreateEvaluation(allocation.ID, allocation.HeadcountID.Value, allocation.ManagerLoginName, allocation.ProjectListItemID, programID, allocation.AreaID);

                            if (idAfterInsert > 0)
                            {
                                allocationsCreated++;

                                //3. Update Evaluation item at SharePoint
                                if (spController.UpdateListItem(ConfigurationData.AllocationsListTitle, allocation.ID, new KeyValuePair<string, string>("EvaluationStatus", "Created")))
                                {
                                    allocationsUpdated++;
                                }

                                //4. Notify Evaluator
                                if (notifyEvaluators)
                                {
                                    SendEvaluationNotice(allocation.Manager.Email, allocation, idAfterInsert);
                                }
                            }

                        }
                        else
                        {
                            LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Allocation ID: {0} does not have Headcount or Manager configured", allocation.ID));
                        }
                    }

                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("{0} allocations were created", allocationsCreated));
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("{0} allocations were updated", allocationsUpdated));
                }
            }

            //5. Allocation Expirations
            if (notifyAllocationsExpirations)
            {
                int noticesSent = 0;
                //filter
                string eightWeeksFromNow = DateTime.Today.AddDays(42).ToString("yyyy-MM-ddTHH:mm:ssZ");//add 6 weeks
                filters = new List<SPQueryFilter> {
                new SPQueryFilter {  FieldName = "Allocation_x0020_Type", FieldType = SPFieldType.Choice, Value = "Confirmed", ComparisonType = SPFieldComparisonType.Eq }
                ,new SPQueryFilter {  FieldName = "Finish_x0020_Date", FieldType = SPFieldType.DateTime, Value = "<Today/>", ComparisonType = SPFieldComparisonType.Gt }
                ,new SPQueryFilter {  FieldName = "Finish_x0020_Date", FieldType = SPFieldType.DateTime, Value = eightWeeksFromNow, ComparisonType = SPFieldComparisonType.Lt }
                //,new SPQueryFilter {  FieldName = "ID", FieldType = SPFieldType.Integer, Value = "2781", ComparisonType = SPFieldComparisonType.Eq } //for testing
                ,new SPQueryFilter {  FieldName = "ExpirationNoticeStatus", FieldType = SPFieldType.Choice, Value = "FinalNoticeSent", ComparisonType = SPFieldComparisonType.Neq }
                };

                var allocations = spController.GetAllocations(filters, new List<string> { "ID", "Manager", "Finish_x0020_Date", "ExpirationNoticeStatus", "Resource_x0020_Name_x003a_Full_x", "Project_x0020_Assigned_x0020_Nam0", "Area_x0020_Assigned", "Role" }, null, true);
                if (allocations != null)
                {
                    DateTime daysFromNow = DateTime.Today.AddDays(28);//28 days
                    foreach (var allocation in allocations)
                    {
                        if (allocation.ExpirationNoticeStatus == null || allocation.ExpirationNoticeStatus.ToLower().Equals("notsent"))
                        {
                            if(allocation.Finish_x0020_Date.HasValue && allocation.Finish_x0020_Date.Value > daysFromNow)
                            {
                                //send first notice
                                if(SendExpirationNotice(allocation.Manager.Email, allocation, false))
                                {
                                    noticesSent++;
                                    //update expiration status
                                    if (!spController.UpdateListItem(ConfigurationData.AllocationsListTitle, allocation.ID, new KeyValuePair<string, string>("ExpirationNoticeStatus", "FirstNoticeSent")))
                                    {
                                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Could not update ExpirationNoticeStatus: {0}", allocation.ID));
                                    }
                                }
                            }
                            else
                            {
                                //send final notice: should not occur
                                if(SendExpirationNotice(allocation.Manager.Email, allocation, true))
                                {
                                    noticesSent++;
                                    //update expiration status
                                    if (!spController.UpdateListItem(ConfigurationData.AllocationsListTitle, allocation.ID, new KeyValuePair<string, string>("ExpirationNoticeStatus", "FinalNoticeSent")))
                                    {
                                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Could not update ExpirationNoticeStatus: {0}", allocation.ID));
                                    }
                                }
                            }
                        }
                        else //FirstNoticeSent
                        {
                            if (allocation.Finish_x0020_Date.HasValue && allocation.Finish_x0020_Date.Value <= daysFromNow)
                            {
                                //send final notice
                                if (SendExpirationNotice(allocation.Manager.Email, allocation, true))
                                {
                                    noticesSent++;
                                    //update expiration status
                                    if (!spController.UpdateListItem(ConfigurationData.AllocationsListTitle, allocation.ID, new KeyValuePair<string, string>("ExpirationNoticeStatus", "FinalNoticeSent")))
                                    {
                                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Could not update ExpirationNoticeStatus: {0}", allocation.ID));
                                    }
                                }
                            }
                        }
                    }
                }

                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("{0} notices were sent", noticesSent));
            }

            LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("RunEvaluationCreationTask DONE."));
        }

        public ServiceResponse<EvaluationsResponse> GetEvaluations(ResourceRequestInfo requestInfo, bool UseLoginName = true)
        {
            EvaluationRepository repository = EvaluationController.Repository;

            var response = new ServiceResponse<EvaluationsResponse> { Status = false };

            if (requestInfo != null && !string.IsNullOrEmpty(requestInfo.LoginName) && this.ConfigurationData.IsValid())
            {
                try
                {
                    string originalUser = requestInfo.LoginName;

                    if (UseLoginName)
                    {
                        //find Group Membership
                        bool canViewAll = false;
                        SharePointController spController = new SharePointController();
                        foreach (string group in this.ConfigurationData.EvaluationViewersGroups)
                        {
                            canViewAll = spController.IsMemberOfSharePointGroup(group, requestInfo.LoginName);
                            if (canViewAll)
                            {
                                break;
                            }
                        }

                        if (canViewAll)
                        {
                            //remove login name from queries
                            originalUser = null;
                        }
                    }
                    else
                    {
                        //remove login name from queries
                        originalUser = null;
                    }

                    //get the range of weeks to return
                    DateTime? startDate = null;

                    DateTime? endDate = null;

                    if (requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3)
                    {
                        startDate = new DateTime(requestInfo.StartDateUp[0], requestInfo.StartDateUp[1], requestInfo.StartDateUp[2]);
                    }
                    if (requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3)
                    {
                        endDate = new DateTime(requestInfo.EndDateUp[0], requestInfo.EndDateUp[1], requestInfo.EndDateUp[2]);
                    }

                    var dbResponse = repository.GetEvaluations((requestInfo.EvaluationID > 0 ? (int?)requestInfo.EvaluationID : null), originalUser, startDate, endDate,
                        (requestInfo.HeadcountID > 0 ? (int?)requestInfo.HeadcountID : null),
                        requestInfo.ProgramID, requestInfo.ProjectListItemID, requestInfo.LocationID, requestInfo.StatusID);

                    if (dbResponse.Count > 0)
                    {
                        //check if a given evaluation can be editable for the current user
                        foreach (var evaluation in dbResponse)
                        {
                            if (evaluation.Status == SQL.Enums.Evaluations.EvaluationStatus.Created && evaluation.Evaluator.ToLower().Equals(requestInfo.LoginName.ToLower()))
                            {
                                evaluation.IsEditable = true;
                            }
                        }
                    }

                    response.Response = new EvaluationsResponse { Evaluations = dbResponse };
                    response.Status = true;
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                }
            }

            return response;
        }

        public ServiceResponse<bool> SubmitEvaluation(ResourceRequestInfo requestInfo)
        {
            EvaluationRepository repository = EvaluationController.Repository;

            var response = new ServiceResponse<bool> { Status = false };

            if (requestInfo != null && requestInfo.EvaluationItems != null && requestInfo.EvaluationItems.Length == 8 && !string.IsNullOrEmpty(requestInfo.LoginName) && requestInfo.EvaluationID > 0
                && requestInfo.ListItemID > 0 && requestInfo.HeadcountID > 0)
            {
                try
                {
                    int[] ei = requestInfo.EvaluationItems;

                    //get Evaluation from storage to take a look
                    var currentEval = this.GetEvaluations(requestInfo, false);

                    if (currentEval.Status)
                    {
                        //validate ListItemId & Evaluator
                        if (currentEval.Response.Evaluations[0].AllocationListItemID == requestInfo.ListItemID && currentEval.Response.Evaluations[0].IsEditable)
                        {
                            //update Storage
                            repository.UpdateEvaluation(requestInfo.EvaluationID, ei[0], ei[1], ei[2], ei[3], ei[4], ei[5], ei[6], ei[7], requestInfo.Comments, requestInfo.LoginName);

                            //calculate Headcount Rating
                            var allEvaluations = this.GetEvaluations(new ResourceRequestInfo { LoginName = requestInfo.LoginName, HeadcountID = requestInfo.HeadcountID, StatusID = new[] { (int)SQL.Enums.Evaluations.EvaluationStatus.Submitted } }, false);
                            if (allEvaluations.Status)
                            {
                                var rating = allEvaluations.Response.Evaluations.Average(i => i.Rating);

                                //update Headcount Rating
                                if (ConfigurationData.IsValid())
                                {
                                    SharePointController spController = new SharePointController();
                                    if (!spController.UpdateListItem(ConfigurationData.HeadcountListTitle, requestInfo.HeadcountID, new KeyValuePair<string, string>("Rating", rating.ToString())))
                                    {
                                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Could not update Rating: {0}", requestInfo.HeadcountID));
                                    }
                                    if (!spController.UpdateListItem(ConfigurationData.AllocationsListTitle, requestInfo.ListItemID, new KeyValuePair<string, string>("EvaluationStatus", "Submitted")))
                                    {
                                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Could not update EvaluationStatus: {0}", requestInfo.ListItemID));
                                    }
                                }
                            }
                            else
                            {
                                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Could not calculate rating: {0}", requestInfo.HeadcountID));
                            }
                            response.Status = true;
                        }
                        else
                        {
                            throw new Exception("Evaluation has inconsistent data");
                        }
                    }
                    else
                    {
                        response.ErrorMessage = currentEval.ErrorMessage;
                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, currentEval.ErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                }
            }

            return response;
        }

        private int CreateEvaluation(int allocationID, int HeadcountID, string Evaluator, int? ProjectID, int? ProgramID, int? AreaID)
        {
            if (allocationID > 0)
            {
                try
                {
                    ResourceEvaluation evaluation = new ResourceEvaluation
                    {
                        AllocationListItemID = allocationID,
                        HeadcountID = HeadcountID,
                        ProjectID = ProjectID,
                        ProgramID = ProgramID,
                        AreaID = AreaID,
                        CreatedOn = DateTime.Now,
                        Status = SQL.Enums.Evaluations.EvaluationStatus.Created,
                        Evaluator = Evaluator,
                        ReaderGroups = "For Future Use"
                    };

                    int idAfterInsert = EvaluationRepository.Add(evaluation);

                    return idAfterInsert;
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                }
            }

            return 0;
        }

        private void UpdateEvaluation(int allocationID)
        {
            /*if (allocationID > 0)
            {
                try
                {
                    ResourceEvaluation evaluation = new ResourceEvaluation
                    {
                        CreatedOn = DateTime.Now,
                        Status = SQL.Enums.Evaluations.EvaluationStatus.Created,
                        Evaluator = "Evaluator here",
                        ReaderGroups = "Reader Groups here"
                    };

                    EvaluationRepository.Add(evaluation, allocationID);
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Allocation ID: {0} - {1}", allocationID, ex.Message));
                }
            }*/
        }

        private bool SendEvaluationNotice(string ManagerEmail, AllocationItem allocation, int evaluationId)
        {
            if (base.ConfigurationData != null && !string.IsNullOrEmpty(ManagerEmail))
            {
                //Logging
                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Attemping to notify: {0}", ManagerEmail));

                try
                {
                    //build addresses list
                    List<string> addresses = new List<string>() { ManagerEmail };

                    string subject = string.Format("End of resource allocation - {0}", 
                        allocation.Resource_x0020_Name_x003a_Full_x != null && allocation.Resource_x0020_Name_x003a_Full_x.LookupValue != null ? allocation.Resource_x0020_Name_x003a_Full_x.LookupValue : "#NO NAME#");

                    string body = string.Format(EvaluationNoticeEmailFormat,
                        allocation.Resource_x0020_Name_x003a_Full_x != null && allocation.Resource_x0020_Name_x003a_Full_x.LookupValue != null ? allocation.Resource_x0020_Name_x003a_Full_x.LookupValue : "",
                        allocation.Role != null && !string.IsNullOrEmpty(allocation.Role.LookupValue) ? " as " + allocation.Role.LookupValue : "",
                        allocation.Project_x0020_Assigned_x0020_Nam0 != null && !string.IsNullOrEmpty(allocation.Project_x0020_Assigned_x0020_Nam0.LookupValue) ? allocation.Project_x0020_Assigned_x0020_Nam0.LookupValue + " project " : ((allocation.Area_x0020_Assigned != null && !string.IsNullOrEmpty(allocation.Area_x0020_Assigned.LookupValue)) ? allocation.Area_x0020_Assigned.LookupValue + " area " : ""),
                        string.Format("{0}{1}", base.ConfigurationData.EvaluationSubmitUrl, evaluationId),
                        base.ConfigurationData.EvaluationListUrl);

                    List<string> bcc = null;
                    if (base.ConfigurationData.DebugEvaluationEmails)
                    {
                        bcc = new List<string> { base.ConfigurationData.DebugEvaluationEmailAddress };
                    }

                    base.SendEmail(base.ConfigurationData.MailFromAddress, addresses, bcc, subject, true, body, allocation.ID);

                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Evaluation Notice for Allocation ID {0} sent to {1}", allocation.ID, ManagerEmail));

                    return true;
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                }
            }
            else if (string.IsNullOrEmpty(ManagerEmail))
            {
                //Logging
                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("ManagerEmail is null for Allocation ID {0}: ", allocation.ID));
            }

            return false;
        }

        private bool SendExpirationNotice(string ManagerEmail, AllocationItem allocation, bool IsFinalNotice)
        {
            if (base.ConfigurationData != null && !string.IsNullOrEmpty(ManagerEmail))
            {
                //Logging
                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Attemping to notify: {0} ", ManagerEmail));

                try
                {
                    //build addresses list
                    List<string> addresses = new List<string>() { ManagerEmail };

                    string subject = string.Format("Resource allocation about to expire ID:{0}", allocation.ID);

                    string body = string.Empty;
                    if (IsFinalNotice)
                    {
                        body = string.Format(ExpirationFinalNoticeEmailFormat,
                            allocation.Resource_x0020_Name_x003a_Full_x != null && allocation.Resource_x0020_Name_x003a_Full_x.LookupValue != null ? allocation.Resource_x0020_Name_x003a_Full_x.LookupValue : "",
                            allocation.Project_x0020_Assigned_x0020_Nam0 != null && !string.IsNullOrEmpty(allocation.Project_x0020_Assigned_x0020_Nam0.LookupValue) ? allocation.Project_x0020_Assigned_x0020_Nam0.LookupValue + " project " : ((allocation.Area_x0020_Assigned != null && !string.IsNullOrEmpty(allocation.Area_x0020_Assigned.LookupValue)) ? allocation.Area_x0020_Assigned.LookupValue + " area " : ""),
                            allocation.Finish_x0020_Date != null ? allocation.Finish_x0020_Date.Value.ToString("MMMM d, yyyy") : "");
                    }
                    else
                    {
                        body = string.Format(ExpirationFirstNoticeEmailFormat,
                            allocation.Resource_x0020_Name_x003a_Full_x != null && allocation.Resource_x0020_Name_x003a_Full_x.LookupValue != null ? allocation.Resource_x0020_Name_x003a_Full_x.LookupValue : "",
                            allocation.Project_x0020_Assigned_x0020_Nam0 != null && !string.IsNullOrEmpty(allocation.Project_x0020_Assigned_x0020_Nam0.LookupValue) ? allocation.Project_x0020_Assigned_x0020_Nam0.LookupValue + " project " : ((allocation.Area_x0020_Assigned != null && !string.IsNullOrEmpty(allocation.Area_x0020_Assigned.LookupValue)) ? allocation.Area_x0020_Assigned.LookupValue + " area " : ""),
                            allocation.Finish_x0020_Date != null ? allocation.Finish_x0020_Date.Value.ToString("MMMM d, yyyy") : "");
                    }

                    List<string> bcc = null;
                    if (base.ConfigurationData.DebugAllocationEmails)
                    {
                        bcc = new List<string> { base.ConfigurationData.DebugAllocationEmailAddress };
                    }

                    base.SendEmail(base.ConfigurationData.MailFromAddress, addresses, bcc, subject, true, body, allocation.ID);

                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Expiration Notice for Allocation ID {0} sent to {1}", allocation.ID, ManagerEmail));

                    return true;
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                }
            }
            else if (string.IsNullOrEmpty(ManagerEmail))
            {
                //Logging
                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("ManagerEmail is null for Allocation ID {0}: ", allocation.ID));
            }

            return false;
        }

    }
}
