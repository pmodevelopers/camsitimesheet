﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Enums.ResourceManagement;
using Cmx.Pmo.Services.Core.Model;
using Cmx.Pmo.Services.Core.Model.ResourceManagement;
using Cmx.Pmo.Services.Core.Util;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Cmx.Pmo.Services.Core.Model.ResourceManagement.AllocationResponse;

namespace Cmx.Pmo.Services.Core.Controllers.ResourceManagement
{
    public class DashboardController
    {
        protected static ConfigurationResourceManagementServices configurationData;

        //fields used by Queries & for Entity Helper
        private List<string> headcountFields = new List<string> { "ID", "Contract_x0020_Type", "Employee_x0020_Type", "Financial_x0020_Type", "Headcount_x0020_Type", "Resource_x0020_Status" };
        private List<string> requestsFields = new List<string> { "ID", "FTEs", "ProgramName" };
        private List<string> allocationsFields = new List<string> { "Resource_x0020_Name_x003a_Full_x", "Project_x0020_Assigned_x0020_Nam", "Project_x0020_Assigned_x0020_Nam1", "Allocation_x0020_Type", "Allocation" };
        private List<string> sectionsFields = new List<string> { "ID", "Title", "TotalNumber", "Option" };

        #region Public Methods

        public ServiceResponse<int[]> GetHeadcountCounters()
        {
            ServiceResponse<int[]> response = new ServiceResponse<int[]> { Status = false };

            configurationData = new ConfigurationResourceManagementServices();

            if (configurationData.IsValid())
            {
                try
                {
                    int[] results = CalculateHeadcountCounters();

                    if (results != null)
                    {
                        response.Status = true;
                        response.Response = results;
                    }
                    else
                    {
                        response.ErrorMessage = "No results";
                    }
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                }
            }
            else
            {
                response.ErrorMessage = "Configuration Invalid";
            }

            return response;
        }

        //Requests FTEs per Program and total FTEs per all Programs
        public ServiceResponse<int[]> GetProgramCounters(string programName)
        {
            ServiceResponse<int[]> response = new ServiceResponse<int[]> { Status = false };

            configurationData = new ConfigurationResourceManagementServices();

            if (configurationData.IsValid())
            {
                try
                {
                    int[] results = CalculateProgramCounters(programName);

                    if (results != null)
                    {
                        response.Status = true;
                        response.Response = results;
                    }
                    else
                    {
                        response.ErrorMessage = "No results";
                    }
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                }
            }
            else
            {
                response.ErrorMessage = "Configuration Invalid";
            }

            return response;
        }

        public ServiceResponse<AllocationResponse> GetAllocationCounters()
        {
            ServiceResponse<AllocationResponse> response = new ServiceResponse<AllocationResponse> { Status = false };

            configurationData = new ConfigurationResourceManagementServices();

            if (configurationData.IsValid())
            {
                try
                {
                    AllocationResponse result = CalculateAllocationCounters();

                    response.Status = true;
                    response.Response = result;
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                }
            }
            else
            {
                response.ErrorMessage = "Configuration Invalid";
            }

            return response;
        }

        #endregion


        #region Private Methods

        //Calculates FTEs per Program and total FTEs per all Programs
        private int[] CalculateProgramCounters(string programName)
        {
            string listTitle = configurationData.RequestsListTitle;
            int[] results = null;

            //get ALL active requests (based on Status)
            using (ClientContext tenantContext = QueryHelper.GetClientContext(configurationData.TenantUrl))
            {
                try
                {
                    string query = string.Format(@"
                                    <View>
                                        <Query>
                                            <Where>
                                                <And>
                                                    <IsNotNull>
                                                        <FieldRef Name='ProgramName' />
                                                    </IsNotNull> 
                                                    <Eq>
                                                        <FieldRef Name='Resource_x0020_Request_x0020_Sta' />
                                                        <Value Type='Choice'>Requested</Value> 
                                                    </Eq> 
                                                </And>
                                            </Where>
                                        </Query>
                                        <RowLimit>{0}</RowLimit>
                                    </View>", 2000);

                    ListItemCollection items = QueryHelper.QuerySharepointList(tenantContext, listTitle, query, requestsFields);

                    if (items != null)
                    {
                        List<ResourceRequestItem> requestItems = new List<ResourceRequestItem>();

                        foreach (ListItem listItem in items)
                        {
                            ResourceRequestItem item = EntityHelper.GetInstance<ResourceRequestItem>(listItem, requestsFields);
                            requestItems.Add(item);
                        }

                        //count all FTEs
                        int totalFTEs = requestItems.Sum(i => i.FTEs);

                        //count targetProgram FTEs
                        var programsGrouped = requestItems.GroupBy(i => i.ProgramName).Select(pg => new { ProgramName = pg.Key, Count = pg.Sum(ip => ip.FTEs) });
                        var targetProgram = programsGrouped.Where(z => z.ProgramName.Equals(programName));
                        var programFTEs = targetProgram.Sum(i => i.Count);

                        //return results
                        int[] temp = { programFTEs, totalFTEs };
                        results = temp;
                    }
                }
                catch (Exception ex)
                {
                    //Log
                    throw ex;
                }
            }

            return results;
        }

        //a project name is not required because when this method is called as a result of Flow triggered by a new/edit listitem event
        //the returned project collection (either new or update) would most likely be just one: the new or updated project
        //disabled as of Feb 20th
        private AllocationResponse CalculateAllocationCounters()
        {
            string listTitle = configurationData.AllocationsListTitle;
            string sectionListTitle = configurationData.SectionsListTitle;

            AllocationResponse response = new AllocationResponse();

            //using (ClientContext tenantContext = QueryHelper.GetClientContext(configurationData.TenantUrl))
            //{
            //    try
            //    {
            //        //get ALL non-finished allocations
            //        string query = string.Format(@"
            //                        <View>
            //                            <Query>
            //                                <Where>
						      //                      <Geq>
								    //                    <FieldRef Name=""Finish_x0020_Date""/>
								    //                    <Value Type=""DateTime"">
									   //                         <Today/>
								    //                    </Value>
						      //                      </Geq>
            //                                </Where>
            //                            </Query>
            //                            <RowLimit>{0}</RowLimit>
            //                        </View>", 5000);

            //        //get ALL ALIVE allocations
            //        //string query = string.Format(@"
            //        //                <View>
            //        //                    <Query>
            //        //                        <Where>
            //        //                            <And>
            //        //              <Leq>
            //        //            <FieldRef Name=""Start_x0020_Date""/>
            //        //            <Value Type=""DateTime"">
            //        //                 <Today/>
            //        //            </Value>
            //        //              </Leq>
            //        //              <Geq>
            //        //            <FieldRef Name=""Finish_x0020_Date""/>
            //        //            <Value Type=""DateTime"">
            //        //                 <Today/>
            //        //            </Value>
            //        //              </Geq>
            //        //                            </And>
            //        //                        </Where>
            //        //                    </Query>
            //        //                    <RowLimit>{0}</RowLimit>
            //        //                </View>", 5000);

            //        ListItemCollection items = QueryHelper.QuerySharepointList(tenantContext, listTitle, query, allocationsFields);

            //        //get all Top5Projects
            //        query = string.Format(@"
            //                        <View>
            //                            <Query>
            //                                <Where>
            //                                    <And>
            //                                        <Neq>
            //                                            <FieldRef Name='Title' />
            //                                            <Value Type='Text'>Total Allocations</Value> 
            //                                        </Neq> 
            //                                        <Eq>
            //                                            <FieldRef Name='Section' />
            //                                            <Value Type='Choice'>Top5Projects</Value> 
            //                                        </Eq> 
            //                                    </And>
            //                                </Where>
            //                            </Query>
            //                            <RowLimit>{0}</RowLimit>
            //                        </View>", 1000);

            //        ListItemCollection itemsSections = QueryHelper.QuerySharepointList(tenantContext, sectionListTitle, query, sectionsFields);

            //        if (items != null && itemsSections !=null)
            //        {
            //            List<AllocationItem> allocationItems = new List<AllocationItem>();
            //            List<SectionItem> sectionItems = new List<SectionItem>();

            //            //fill allocation items
            //            foreach (ListItem listItem in items)
            //            {
            //                AllocationItem item = EntityHelper.GetInstance<AllocationItem>(listItem, allocationsFields);
            //                allocationItems.Add(item);
            //            }

            //            //fill section items
            //            foreach (ListItem listItem in itemsSections)
            //            {
            //                SectionItem item = EntityHelper.GetInstance<SectionItem>(listItem, sectionsFields);
            //                sectionItems.Add(item);
            //            }

            //            //group by Allocation Type
            //            var itemsByAllocationType = allocationItems.GroupBy(i => i.Allocation_x0020_Type).Select(a => new { AllocationType = a.Key, Count = a.Count() });

            //            //calculate Total SoftAllocated
            //            response.soft = itemsByAllocationType.Where(i => i.AllocationType.Equals("Preliminary")).Select(a => a.Count).FirstOrDefault();

            //            //group HARD allocations by Resource //changed Resource_x0020_Name_x003a_Full_x to be FieldLookupValue (previously string) - Feb20th
            //            var itemsByResource = allocationItems.Where(z => z.Allocation_x0020_Type.Equals("Confirmed")).GroupBy(i => i.Resource_x0020_Name_x003a_Full_x).Select(a => new { ResourceName = a.Key, Utilization = a.Sum(b => b.Allocation) });

            //            //calculate Total Resources Allocated
            //            response.total = itemsByResource.Count();

            //            //calculate utilization
            //            double totalUtilizationAvg = itemsByResource.Average(i => i.Utilization);
            //            response.avg = (int)totalUtilizationAvg;
            //            //0
            //            response.r1 = itemsByResource.Where(i => i.Utilization == 0).Count();
            //            //0-50
            //            response.r2 = itemsByResource.Where(i => i.Utilization > 0 && i.Utilization <= 50).Count();
            //            //50-75
            //            response.r3 = itemsByResource.Where(i => i.Utilization > 50 && i.Utilization <= 75).Count();
            //            //75-99
            //            response.r4 = itemsByResource.Where(i => i.Utilization > 75 && i.Utilization < 100).Count();
            //            //100
            //            response.r5 = itemsByResource.Where(i => i.Utilization == 100).Count();
            //            //100+
            //            response.r6 = itemsByResource.Where(i => i.Utilization > 100).Count();

            //            //group by Project (only non-null projects)
            //            var itemsByProject = allocationItems.Where(z => z.Project_x0020_Assigned_x0020_Nam != null).GroupBy(i => i.Project_x0020_Assigned_x0020_Nam).Select(a => new { ProjectName = a.Key, Count = a.Count(), Status = a.First().Project_x0020_Assigned_x0020_Nam1 });
            //            var allExistingProjectNames = sectionItems.Select(i => i.Title).ToArray();

            //            //calculate new projects (projects not in Sections List)
            //            List<ProjectResponse> newProjects = itemsByProject.Where(i => !allExistingProjectNames.Contains(i.ProjectName)).Select(a => new ProjectResponse { n = a.ProjectName, t = a.Count, s = a.Status }).ToList();
            //            response.newProjects = newProjects.ToArray();

            //            //calculate Projects with changes
            //            var existingProjects = itemsByProject.Where(i => allExistingProjectNames.Contains(i.ProjectName));
            //            List<SectionItem> projectsWithChanges = new List<SectionItem>();

            //            foreach (var existingProject in existingProjects)
            //            {
            //                var sectionItem = sectionItems.Where(i => i.Title.Equals(existingProject.ProjectName)).FirstOrDefault();
            //                if (sectionItem != null && (sectionItem.TotalNumber != existingProject.Count || sectionItem.Option != existingProject.Status))
            //                {
            //                    sectionItem.TotalNumber = existingProject.Count;
            //                    sectionItem.Option = existingProject.Status;
            //                    projectsWithChanges.Add(sectionItem);
            //                }
            //            }
            //            List<ProjectResponse> updateProjects = projectsWithChanges.Select(i => new ProjectResponse { n = i.Title, t = i.TotalNumber, s = i.Option, id= i.ID }).ToList();
            //            response.updateProjects = updateProjects.ToArray();
            //        }

            //    }
            //    catch (Exception ex)
            //    {
            //        //Log
            //        throw ex;
            //    }
            //}

            return response;
        }

        private int[] CalculateHeadcountCounters()
        {
            string headcountListTitle = configurationData.HeadcountListTitle;
            int[] results = null;

            //get ALL non-INACTIVE from headcount
            using (ClientContext tenantContext = QueryHelper.GetClientContext(configurationData.TenantUrl))
            {
                try
                {
                    string query = string.Empty;

                    query = string.Format(@"
                                <View>
                                    <Query>
                                        <Where>
                                            <Neq>
                                                <FieldRef Name='Resource_x0020_Status' />
                                                <Value Type='Choice'>Inactive</Value> 
                                            </Neq> 
                                        </Where>
                                    </Query>
                                    <RowLimit>{0}</RowLimit>
                                </View>", 2000);

                    ListItemCollection items = QueryHelper.QuerySharepointList(tenantContext, headcountListTitle, query, headcountFields);

                    if (items != null)
                    {
                        List<HeadcountItem> headcountItems = new List<HeadcountItem>();

                        foreach (ListItem listItem in items)
                        {
                            HeadcountItem headcountItem = EntityHelper.GetInstance<HeadcountItem>(listItem, headcountFields);
                            headcountItems.Add(headcountItem);
                        }

                        //count 
                        int fullTime = headcountItems.Where(i => i.Contract_x0020_Type.Equals("Full Time") && i.Resource_x0020_Status.Equals("Active")).Count();
                        int partTime = headcountItems.Where(i => i.Contract_x0020_Type.Equals("Part Time") && i.Resource_x0020_Status.Equals("Active")).Count();
                        int cemex = headcountItems.Where(i => i.Employee_x0020_Type.Equals("CEMEX") && i.Resource_x0020_Status.Equals("Active")).Count();
                        int neoris = headcountItems.Where(i => i.Employee_x0020_Type.Equals("Neoris") && i.Resource_x0020_Status.Equals("Active")).Count();
                        int noNeoris = headcountItems.Where(i => i.Employee_x0020_Type.Equals("No Neoris") && i.Resource_x0020_Status.Equals("Active")).Count();
                        int capex = headcountItems.Where(i => i.Financial_x0020_Type.Equals("CAPEX") && i.Resource_x0020_Status.Equals("Active")).Count();
                        int opex = headcountItems.Where(i => i.Financial_x0020_Type.Equals("OPEX") && i.Resource_x0020_Status.Equals("Active")).Count();
                        int permanent = headcountItems.Where(i => i.Headcount_x0020_Type.Equals("Permanent") && i.Resource_x0020_Status.Equals("Active")).Count();
                        int temporary = headcountItems.Where(i => i.Headcount_x0020_Type.Equals("Temporary") && i.Resource_x0020_Status.Equals("Active")).Count();
                        int unAssigned = headcountItems.Where(i => i.Resource_x0020_Status.Equals("Unassigned")).Count();
                        int active = headcountItems.Where(i => i.Resource_x0020_Status.Equals("Active")).Count();

                        //return results
                        int[] temp = { fullTime, partTime, cemex,neoris,noNeoris, capex,opex, permanent, temporary, unAssigned, active };
                        results = temp;
                    }

                }
                catch (Exception ex)
                {
                    //Log
                    throw ex;
                }
            }

            return results;
        }

        #endregion

    }
}
