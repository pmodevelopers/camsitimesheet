﻿using Cmx.Pmo.Services.Core.Enums;
using Cmx.Pmo.SQL.Model;
using Cmx.Pmo.SQL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Controllers
{
    public class LogController
    {
        public static void LogMessage(ControllerID ServiceID, object Category, object SubCategory, string Message)
        {
            LogMessage((int)ServiceID, Category, SubCategory, Message);
        }

        public static void LogMessage(ControllerID ServiceID, object Category, object SubCategory, Exception ex, bool digOut = false)
        {
            LogMessage((int)ServiceID, Category, SubCategory, ex, digOut);
        }

        public static void LogMessage(int ServiceID, object Category, object SubCategory, string Message)
        {
            if (!string.IsNullOrEmpty(Message))
            {
                LogEntry logEntry = new LogEntry { ServiceID = ServiceID, Date = DateTime.Now, Message = Message };
                if (!string.IsNullOrEmpty(Category != null ? Category.ToString() : null))
                {
                    logEntry.Category = Category.ToString();
                }
                if (!string.IsNullOrEmpty(SubCategory != null ? SubCategory.ToString() : null))
                {
                    logEntry.SubCategory = SubCategory.ToString();
                }

                LogRepository.AddLogEntry(logEntry);
            }
        }

        public static void LogMessage(int ServiceID, object Category, object SubCategory, Exception ex, bool digOut = false)
        {
            if (ex != null)
            {
                string Message = ex.Message;
                if (digOut && ex.InnerException != null)
                {
                    Message = ex.InnerException.InnerException != null ? ex.InnerException.InnerException.Message : ex.InnerException.Message;
                }

                LogMessage(ServiceID, Category, SubCategory, Message);
            }
        }
    }
}
