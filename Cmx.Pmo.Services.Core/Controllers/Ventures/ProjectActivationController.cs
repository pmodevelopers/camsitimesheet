﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Enums.ProjectActivation;
using Cmx.Pmo.Services.Core.Model.ProjectActivation;
using Cmx.Pmo.SQL.Model.Ventures;
using Cmx.Pmo.SQL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Controllers.Ventures
{
    public class ProjectActivationController : BaseController
    {
        public ProjectActivationController()
        {
        }

        public ProjectActivationRepository GetRepository()
        {
            return new ProjectActivationRepository();
        }


        public ServiceResponse<bool> SaveProjectActivation(ProjectActivationSaveRequest request, SaveType saveType)
        {
            ProjectActivationRepository repository = this.GetRepository();
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false };

            if (request != null)
            {
                if(saveType == SaveType.ProjectLeader)
                {
                    ProjectActivation item = new ProjectActivation { ProjectLongID = request.ProjectLongID, ProjectLeaderUserIdInWeb = request.ProjectLeaderUserIdInWeb };

                    repository.SaveProjectActivation(item);
                }
                else if (saveType == SaveType.ProjectManager)
                {
                    ProjectActivation item = new ProjectActivation { ProjectLongID = request.ProjectLongID, ProjectManagerUserIdInWeb = request.ProjectManagerUserIdInWeb };

                    repository.SaveProjectActivation(item);
                }
            }
            else
            {
                response.ErrorMessage = "Bad request";
            }

            return response;
        }

    }
}
