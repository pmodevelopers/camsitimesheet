﻿using Cmx.Pmo.Services.Core.Constants;
using Cmx.Pmo.Services.Core.Enums;
using Cmx.Pmo.Services.Core.Model;
using Cmx.Pmo.Services.Core.Util;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Controllers
{
    public abstract class BaseController
    {
        private ControllerID controllerId = ControllerID.Base;
        public virtual ControllerID ControllerId
        {
            get
            {
                return controllerId;
            }
            set
            {
                controllerId = value;
            }
        }

        private ConfigurationResourceManagementServices configurationData = null;
        protected ConfigurationResourceManagementServices ConfigurationData
        {
            get
            {
                if (configurationData == null)
                {
                    configurationData = new ConfigurationResourceManagementServices();
                    if (!configurationData.IsValid())
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, "Configuration data not valid.");
                        configurationData = null;
                    }
                }

                return configurationData;
            }
        }

        //private SmtpClient cmxSmtpClient = null;
        //private SmtpClient CmxSmtpClient
        //{
        //    get
        //    {
        //        if (cmxSmtpClient == null)
        //        {
        //            if (this.ConfigurationData != null)
        //            {
        //                cmxSmtpClient = new SmtpClient(ConfigurationData.MailServer, ConfigurationData.MailPort.Value);
        //                cmxSmtpClient.UseDefaultCredentials = false;
        //                cmxSmtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationData.MailUsername, ConfigurationData.MailPassword);
        //                cmxSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
        //                cmxSmtpClient.EnableSsl = true;
        //            }
        //        }

        //        return cmxSmtpClient;
        //    }
        //}

        protected bool SendEmail(string From, List<string> To, List<string> Bcc, string Subject, bool IsHtml, string Body, object SubCategory = null)
        {
            //Logging
            StringBuilder toLogging = new StringBuilder();
            int cc = 0;
            foreach (string address in To)
            {
                if (cc < 5)
                {
                    toLogging.Append(address + ",");
                }
                else
                {
                    break;
                }
            }
            LogController.LogMessage((int)this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, SubCategory, string.Format("Emailing... {0}", toLogging.ToString()));

            SmtpClient cmxSmtpClient = null;
            try
            {
                //1. create client
                cmxSmtpClient = new SmtpClient(ConfigurationData.MailServer, ConfigurationData.MailPort.Value);
                cmxSmtpClient.UseDefaultCredentials = false;
                cmxSmtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationData.MailUsername, ConfigurationData.MailPassword);
                cmxSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                cmxSmtpClient.EnableSsl = true;

                //2. set message
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(From);
                foreach (string address in To)
                {
                    mail.To.Add(new MailAddress(address));
                }

                if (Bcc != null)
                {
                    foreach (string addressBcc in Bcc)
                    {
                        mail.Bcc.Add(new MailAddress(addressBcc));
                    }
                }

                mail.Subject = Subject;
                mail.IsBodyHtml = IsHtml;
                mail.Body = Body;

                cmxSmtpClient.Send(mail);

                return true;
            }
            catch (Exception mailEx)
            {
                LogController.LogMessage((int)this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, SubCategory, string.Format("Could not send email to: {0} >= {1}", toLogging.ToString(), mailEx.Message));

                return false;
            }
            finally
            {
                if(cmxSmtpClient != null)
                {
                    cmxSmtpClient.Dispose();
                }
            }
        }

    }
}
