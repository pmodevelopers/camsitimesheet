﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Constants;
using Cmx.Pmo.Services.Core.Enums;
using Cmx.Pmo.Services.Core.Model;
//using Cmx.Pmo.Services.Core.Model.ResourceManagement;
using Cmx.Pmo.Services.Core.Model.Timesheet;
using Cmx.Pmo.Services.Core.Util;
using Cmx.Pmo.Services.Core.Util.Model;
using Cmx.Pmo.SQL.Enums.Timesheet;
using Cmx.Pmo.SQL.Model.ResourceManagement;
//using Cmx.Pmo.SQL.Model.ResourceManagement;
using Cmx.Pmo.SQL.Model.Timesheet;
using Cmx.Pmo.SQL.Repository;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Web.Hosting;

namespace Cmx.Pmo.Services.Core.Controllers.Timesheet
{
    public class TimesheetController : BaseController
    {
        //TBD: from sharepoint??
        private decimal limitPerWeek = 168;
        private decimal limitPerDay = 24;
        //private int ServiceId = 1;
        private new ControllerID ControllerId = ControllerID.Timesheet;


        #region Email Formats

        #region Missing Weeks

        private string MissingWeekEmailFormat = @"
        <div style=""height: 60px; width: 100%; background-color: #002a59; border-bottom: 5px solid red; color: #ffffff;""> 
              <img width = ""100"" height=""50"" src=""https://eustdmyo001.blob.core.windows.net/pmo/CEMEX%20White.png"" alt="""" style=""border: 0px;""/> 
        </div>
        <p>{0},</p>
        <p>Our records show that you have failed to submit the following period{1}:</p>
        {2}
        <p>We will appreciate if you submit {3} as soon as you can.</p>
        <p>Thanks.</p>
        <br>
        <p style = ""background-color: #002a59; color: white; width: 100%; padding: 3px !important;"" > Copyright © 2018, CEMEX International Holding AG.All rights reserved.</p>";

        private string MissingIndividualWeekFormat = @"<a href=""{0}?w={1}"">{2} - {3}</a><br>";

        #endregion

        #region Pending Approval

        private string PendingApprovalEmailFormat = @"
        <div style=""height: 60px; width: 100%; background-color: #002a59; border-bottom: 5px solid red; color: #ffffff;""> 
              <img width = ""100"" height=""50"" src=""https://eustdmyo001.blob.core.windows.net/pmo/CEMEX%20White.png"" alt="""" style=""border: 0px;""/> 
        </div>
        <p>{0},</p>
        <p>Please see below the list of submitted timesheets pending for your approval:</p>
        {1}
        <p>In the following link you can review and process them at your earliest convenience.</p>
        <span><a href=""{2}"">TIMESHEET APPROVAL</a></span>
        <p>Thanks.</p>
        <br>
        <p style = ""background-color: #002a59; color: white; width: 100%; padding: 3px !important;"" > Copyright © 2018, CEMEX International Holding AG.All rights reserved.</p>";

        private string PendingIndividualWeekFormat = @"<span><b>{0}</b> : {1} - {2}</span><br/>";

        #endregion

        #region Rejected Week

        private string RejectionEmailFormat = @"
        <div style=""height: 60px; width: 100%; background-color: #002a59; border-bottom: 5px solid red; color: #ffffff;""> 
              <img width = ""100"" height=""50"" src=""https://eustdmyo001.blob.core.windows.net/pmo/CEMEX%20White.png"" alt="""" style=""border: 0px;""/> 
        </div>
        <p>{0},</p>
        <p>Your Resource Manager has rejected a period you previously submitted on the following grounds:</p>
        <span>Period:  <a href=""{1}?w={2}"">{3} - {4}</a></span><br/>
        <span>Reason: {5}</span><br/>
        <p>Please review your hours and make the appropiate corrections as soon as you can.</p>
        <p>Thanks.</p>
        <br>
        <p style = ""background-color: #002a59; color: white; width: 100%; padding: 3px !important;"" > Copyright © 2018, CEMEX International Holding AG.All rights reserved.</p>";

        private string RejectionEmailFormatOld = @"
        <html>
            <body>
                <table cellpadding = ""0"" cellspacing=""0"" border=""0"" style=""padding:0px;margin:0px;width:100%;"">
                    <tr>
                        <td>
                            {0},
                            <br/>
                            <br/>
                            Your Resource Manager has rejected a period you previously submitted on the following grounds:
                            <br/>
                            <br/>
                            <span>Period:  <a href=""{1}?w={2}"">{3} - {4}</a></span><br/>
                            <span>Reason: {5}</span><br/>
                            <br/>
                            Please review your hours and make the appropiate corrections as soon as you can.
                            <br/>
                            <br/>
                            Thanks,
                            <br/>
                            <br/>
                        </td>
                    </tr>
                    <tr>
                        <td style = ""color:#fff!important;padding-left:0px!important;margin:0px;"" >
                            <div class=""email-footer"">
                                <p style = ""padding-top:3px!important;"" >Copyright © 2018, CEMEX International Holding AG.All rights reserved.</p>
                            </div>
                        </td>
                    </tr>
                </table>
            </body>
        </html>
        ";

        #endregion

        #endregion

        public TimesheetController()
        {
        }

        public TimeTrackingRepository GetRepository()
        {
            return new TimeTrackingRepository();
        }

        /// <summary>
        /// sets appropiate status and triggers saving process
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ServiceResponse<bool> SaveWeek(TrackTimeSaveRequest request)
        {
            //validate data integrity
            if (this.ValidateRequest(request))
            {
                request.Week.Status = WeekStatus.Open;
                return this.ProcessSaveRequest(request);
            }

            return new ServiceResponse<bool> { Status = false, ErrorMessage = "The timesheet contains invalid data" };
        }

        /// <summary>
        /// sets appropiate status and triggers saving process
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ServiceResponse<bool> SubmitWeek(TrackTimeSaveRequest request)
        {
            //validate data integrity
            if (this.ValidateRequest(request))
            {
                request.Week.Status = WeekStatus.Submitted;
                return this.ProcessSaveRequest(request);
            }

            return new ServiceResponse<bool> { Status = false, ErrorMessage = "The timesheet contains invalid data" };
        }

        public ServiceResponse<bool> ApproveWeeks(TrackTimeApproveRequest request, bool isApprove)
        {
            //validate data integrity
            if (this.ValidateApproveRequest(request))
            {
                request.Status = isApprove ? WeekStatus.Approved : WeekStatus.Rejected;
                return this.ProcessApproveRequest(request);
            }

            return new ServiceResponse<bool> { Status = false, ErrorMessage = "Timesheet(s) contains invalid data" };
        }

        /// <summary>
        /// Gets from storage a set of Weeks for the requested headcount, each containing a set of tracking items 
        /// </summary>
        /// <param name="requestInfo"></param>
        /// <returns></returns>
        public ServiceResponse<TrackTimeWeeksResponse> GetWeek(TrackTimeRequestInfo requestInfo)
        {
            TimeTrackingRepository repository = this.GetRepository();

            ServiceResponse<TrackTimeWeeksResponse> response = new ServiceResponse<TrackTimeWeeksResponse> { Status = false };

            try
            {
                ConfigurationResourceManagementServices configurationData = new ConfigurationResourceManagementServices();

                DateTime futureWeek = this.GetFutureWeekLimit();
                int[] StartDateFutureWeek = new int[] { futureWeek.Year, futureWeek.Month - 1, futureWeek.Day };
                int[] StartDateADWeek = new int[] { configurationData.TimesheetDeployDate.Year, configurationData.TimesheetDeployDate.Month - 1, configurationData.TimesheetDeployDate.Day };
                DateTime startDate = new DateTime(requestInfo.StartDateUp[0], requestInfo.StartDateUp[1], requestInfo.StartDateUp[2]);

                /*
                DateTime? endDate = null;
                
                if (requestInfo.StartDate.HasValue && requestInfo.EndDate.HasValue)
                {
                    //if the request specifies a set of weeks, use those
                    startDate = new DateTime(requestInfo.StartDate.Value.Year, requestInfo.StartDate.Value.Month, requestInfo.StartDate.Value.Day);
                    endDate = new DateTime(requestInfo.EndDate.Value.Year, requestInfo.EndDate.Value.Month, requestInfo.EndDate.Value.Day);
                }
                else
                {
                    startDate = periodWeekDates[0];
                    endDate = periodWeekDates[1].AddDays(6);
                }
                */

                //call storage
                var dbResponse = repository.GetWeek(requestInfo.HeadcountID,
                    new DateTime(startDate.Year, startDate.Month, startDate.Day)
                    //new DateTime(endDate.Value.Year, endDate.Value.Month, endDate.Value.Day)
                    );

                List<WeekInfo> weeks = new List<WeekInfo>();

                //get Weeks grouping by startdate
                var weekItems = dbResponse.GroupBy(x => x.StartDate).Select(y => y);

                //for each Week
                foreach (var itemWeek in weekItems)
                {
                    //get week info from first item
                    var sampleWeekItem = itemWeek.FirstOrDefault();

                    WeekInfo week = new WeekInfo
                    {
                        StartDateDown = new int[] { sampleWeekItem.StartDate.Year, sampleWeekItem.StartDate.Month - 1, sampleWeekItem.StartDate.Day },
                        Status = sampleWeekItem.Status,
                        Comments = sampleWeekItem.Comments,
                        HeadcountID = sampleWeekItem.HeadcountID
                    };

                    week.TrackableItemsInfo = new List<TrackableItemInfo>();

                    //group items grouped in this week by itemDbID
                    var items = itemWeek.GroupBy(y => y.ItemDbID).Select(z => z);

                    //for each item
                    foreach (var item in items)
                    {
                        List<TimeTrackingItemWeekDay> itemsList = item.ToList();

                        //get item info from first item
                        var sampleItem = itemsList.FirstOrDefault();

                        TrackableItemInfo itemInfo = new TrackableItemInfo
                        {
                            ListItemID = sampleItem.ListItemID,
                            CountryID = sampleItem.CountryID,
                            Type = sampleItem.TrackingType,
                            Hours = this.GetHours(itemsList)
                        };

                        week.TrackableItemsInfo.Add(itemInfo);
                    }

                    weeks.Add(week);
                }

                response.Status = true;
                response.Response = new TrackTimeWeeksResponse
                {
                    Weeks = weeks,
                    StartDateADWeek = StartDateADWeek,
                    StartDateFutureWeek = StartDateFutureWeek
                };
            }
            catch (Exception ex)
            {
                LogController.LogMessage((int)this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ServiceResponse<List<TimeTrackingItemWeekDay>> GetWeekRaw(int HeadcountID, int[] StartDateUp)
        {
            ServiceResponse<List<TimeTrackingItemWeekDay>> response = new ServiceResponse<List<TimeTrackingItemWeekDay>> { Status = false };

            try
            {
                TimeTrackingRepository repository = this.GetRepository();
                DateTime startDate = new DateTime(StartDateUp[0], StartDateUp[1], StartDateUp[2]);

                var dbResponse = repository.GetWeek(HeadcountID, new DateTime(startDate.Year, startDate.Month, startDate.Day));

                response.Status = true;
                response.Response = dbResponse;
            }
            catch (Exception ex)
            {

            }

            return response;
        }

        public ServiceResponse<TrackTimeWeeksBasicResponse> GetWeeks(TrackTimeRequestInfo requestInfo)
        {
            TimeTrackingRepository repository = this.GetRepository();

            ServiceResponse<TrackTimeWeeksBasicResponse> response = new ServiceResponse<TrackTimeWeeksBasicResponse> { Status = false };

            try
            {
                //get the range of weeks to return
                DateTime? startDate = null;

                DateTime? endDate = null;

                if (requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3)
                {
                    startDate = new DateTime(requestInfo.StartDateUp[0], requestInfo.StartDateUp[1], requestInfo.StartDateUp[2]);
                }
                if (requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3)
                {
                    endDate = new DateTime(requestInfo.EndDateUp[0], requestInfo.EndDateUp[1], requestInfo.EndDateUp[2]);
                }



                //call storage
                var dbResponse = repository.GetWeeks(startDate, endDate,
                    (requestInfo.HeadcountID > 0 ? (int?)requestInfo.HeadcountID : null),
                    requestInfo.ProgramID, requestInfo.ProjectListItemID, requestInfo.LocationID, requestInfo.StatusID, requestInfo.Manager
                    );

                

                var complementaryResponse = this.GetHoursByDatesRange(new TrackTimeRequestInfo
                {
                    HeadcountID = requestInfo.HeadcountID,
                    StartDateUp = requestInfo.StartDateUp,
                    EndDateUp = requestInfo.EndDateUp,
                    AreaID = requestInfo.AreaID,
                    StatusID = new int[] { (int)WeekStatus.Approved } //the request for weeks can be any status, but avgs are calculated considering only approved status
                });

                List<WeekBasicInfo> weeks = new List<WeekBasicInfo>();

                //get Weeks grouping by startdate
                var weekItems = dbResponse.GroupBy(x => x.StartDate).Select(y => y);

                //for each Week
                foreach (var itemWeek in weekItems)
                {
                    //group now by HeadCountID (a headcount cannot have the same week more that once)
                    var itemsByHeadcount = itemWeek.GroupBy(y => y.HeadcountID).Select(z => z);

                    foreach (var itemHeadcount in itemsByHeadcount)
                    {
                        //get week info from first item
                        var sampleWeekItem = itemHeadcount.FirstOrDefault();

                        WeekBasicInfo week = new WeekBasicInfo
                        {
                            StartDateDown = new int[] { sampleWeekItem.StartDate.Year, sampleWeekItem.StartDate.Month, sampleWeekItem.StartDate.Day },
                            Status = sampleWeekItem.Status,
                            HeadcountID = sampleWeekItem.HeadcountID,
                            Hours = new decimal[7]
                        };

                        if (requestInfo.IncludeManager)
                        {
                            week.Manager = sampleWeekItem.Manager;
                        }

                        //group items grouped in this week by itemDbID
                        var items = itemHeadcount.GroupBy(y => y.ItemDbID).Select(z => z);

                        //for each item
                        foreach (var item in items)
                        {
                            List<TimeTrackingItemWeekDay> itemsList = item.ToList();
                            var hours = this.GetHours(itemsList);
                            week.TotalHours += hours.Sum(z => z);
                            for (int z = 0; z < week.Hours.Length; z++)
                            {
                                week.Hours[z] += hours[z];
                            }
                        }

                        weeks.Add(week);
                    }
                }

                response.Status = true;
                response.Response = new TrackTimeWeeksBasicResponse
                {
                    Weeks = weeks
                };
                if (complementaryResponse.Status)
                {
                    response.Response.WeeksAvg = complementaryResponse.Response.WeeksAvg;
                    response.Response.MonthsAvg = complementaryResponse.Response.MonthsAvg;
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                if (ex.InnerException != null) sb.Append(ex.InnerException.Message);
                sb.Append(ex.StackTrace);
                response.ErrorMessage = sb.ToString();
                
            }

            return response;
        }

        public ServiceResponse<TrackTimeWeeksBasicResponse> GetWeeksApproveOrReject(TrackTimeRequestInfo requestInfo)
        {
            TimeTrackingRepository repository = this.GetRepository();

            ServiceResponse<TrackTimeWeeksBasicResponse> response = new ServiceResponse<TrackTimeWeeksBasicResponse> { Status = false };

            try
            {
                //get the range of weeks to return
                DateTime? startDate = null;

                DateTime? endDate = null;

                if (requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3)
                {
                    startDate = new DateTime(requestInfo.StartDateUp[0], requestInfo.StartDateUp[1], requestInfo.StartDateUp[2]);
                }
                if (requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3)
                {
                    endDate = new DateTime(requestInfo.EndDateUp[0], requestInfo.EndDateUp[1], requestInfo.EndDateUp[2]);
                }

                //call storage
                List<TimeTrackingItemWeekDay> dbResponse = null;
                if (requestInfo.HeadcountsID != null && requestInfo.HeadcountsID.Length > 0)
                {
                    foreach (int _headcount in requestInfo.HeadcountsID)
                    {
                        //when passing array of Headcount (it means user used the filters to select headcounts) do not pass manager
                        //manager will still be able to request for its own headcounts, but PM will be able to request for any headcount
                        List<TimeTrackingItemWeekDay> dbResponseHeadcount = repository.GetWeeks(startDate, endDate,
                                        (int?)_headcount,
                                        requestInfo.ProgramID, requestInfo.ProjectListItemID, requestInfo.LocationID, requestInfo.StatusID, null);

                        //if(dbResponseHeadcount != null && dbResponseHeadcount.Count > 0)
                        //{
                            if(dbResponse == null)
                            {
                                dbResponse = dbResponseHeadcount;
                            }
                            else
                            {
                                dbResponse.AddRange(dbResponseHeadcount);
                            }
                        //}
                    }
                }
                else
                {
                    dbResponse = repository.GetWeeks(startDate, endDate,
                                (requestInfo.HeadcountID > 0 ? (int?)requestInfo.HeadcountID : null),
                                requestInfo.ProgramID, requestInfo.ProjectListItemID, requestInfo.LocationID, requestInfo.StatusID, requestInfo.Manager);
                }

                var complementaryResponse = this.GetHoursByDatesRange(new TrackTimeRequestInfo
                {
                    HeadcountID = requestInfo.HeadcountID,
                    StartDateUp = requestInfo.StartDateUp,
                    EndDateUp = requestInfo.EndDateUp,
                    AreaID = requestInfo.AreaID,
                    StatusID = new int[] { (int)WeekStatus.Approved } //the request for weeks can be any status, but avgs are calculated considering only approved status
                });

                List<WeekBasicInfo> weeks = new List<WeekBasicInfo>();

                //get Weeks grouping by startdate
                var weekItems = dbResponse.GroupBy(x => x.StartDate).Select(y => y);

                //for each Week
                foreach (var itemWeek in weekItems)
                {
                    //group now by HeadCountID (a headcount cannot have the same week more that once)
                    var itemsByHeadcount = itemWeek.GroupBy(y => y.HeadcountID).Select(z => z);

                    foreach (var itemHeadcount in itemsByHeadcount)
                    {
                        //get week info from first item
                        var sampleWeekItem = itemHeadcount.FirstOrDefault();

                        WeekBasicInfo week = new WeekBasicInfo
                        {
                            StartDateDown = new int[] { sampleWeekItem.StartDate.Year, sampleWeekItem.StartDate.Month, sampleWeekItem.StartDate.Day },
                            Status = sampleWeekItem.Status,
                            HeadcountID = sampleWeekItem.HeadcountID,
                            Hours = new decimal[7]
                        };

                        if (requestInfo.IncludeManager)
                        {
                            week.Manager = sampleWeekItem.Manager;
                        }

                        //group items grouped in this week by itemDbID
                        var items = itemHeadcount.GroupBy(y => y.ItemDbID).Select(z => z);

                        //for each item
                        foreach (var item in items)
                        {
                            List<TimeTrackingItemWeekDay> itemsList = item.ToList();
                            var hours = this.GetHours(itemsList);
                            week.TotalHours += hours.Sum(z => z);
                            for (int z = 0; z < week.Hours.Length; z++)
                            {
                                week.Hours[z] += hours[z];
                            }
                        }

                        weeks.Add(week);
                    }
                }

                response.Status = true;
                response.Response = new TrackTimeWeeksBasicResponse
                {
                    Weeks = weeks
                };
                if (complementaryResponse.Status)
                {
                    response.Response.WeeksAvg = complementaryResponse.Response.WeeksAvg;
                    response.Response.MonthsAvg = complementaryResponse.Response.MonthsAvg;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ServiceResponse<TrackTimeWeeksResponse> GetPopulateWeek(TrackTimeRequestInfo requestInfo)
        {
            TimeTrackingRepository repository = this.GetRepository();
            SharePointController spController = new SharePointController(); ;

            ServiceResponse<TrackTimeWeeksResponse> response = new ServiceResponse<TrackTimeWeeksResponse> { Status = false };

            try
            {
                requestInfo.StartDate = new DateTime(requestInfo.StartDateUp[0], requestInfo.StartDateUp[1], requestInfo.StartDateUp[2]);
                //get oldest week for headcount
                TimeTrackingWeek populateWeek = repository.GetPopulateWeek(requestInfo.HeadcountID, requestInfo.StartDate.Value);

                if (populateWeek != null)
                {
                    //get full week data
                    ServiceResponse<TrackTimeWeeksResponse> fullWeeks = this.GetWeek(
                        new TrackTimeRequestInfo
                        {
                            HeadcountID = requestInfo.HeadcountID,
                            StartDateUp = new int[] { populateWeek.StartDate.Year, populateWeek.StartDate.Month, populateWeek.StartDate.Day },
                            EndDate = populateWeek.EndDate
                        }
                    );

                    if (fullWeeks.Status)
                    {
                        #region possible server side validation - not tested
                        //validate tracking items
                        /*
                        if (fullWeeks.Response != null && fullWeeks.Response.Weeks != null && fullWeeks.Response.Weeks.Count > 0 && fullWeeks.Response.Weeks[0] != null && fullWeeks.Response.Weeks[0].TrackableItemsInfo != null)
                        {
                            List<TrackableItemInfo> validItems = new List<TrackableItemInfo>();

                            //DateTime today = DateTime.Today;
                            //DateTime currentWeekStartDate = today.AddDays(Convert.ToDouble(today.DayOfWeek) * -1);
                            DateTime currentWeekStartDate = requestInfo.StartDate.Value;

                            foreach (var trackItem in fullWeeks.Response.Weeks[0].TrackableItemsInfo)
                            {
                                if (trackItem.Type == TrackingType.Allocation)
                                {
                                    List<SPQueryFilter> filters = new List<SPQueryFilter> { new SPQueryFilter { FieldName = "ID", FieldType = SPFieldType.Integer, Value = trackItem.ListItemID.ToString(), ComparisonType = SPFieldComparisonType.Eq } };

                                    List<Model.ResourceManagement.AllocationItem> allocations = spController.GetAllocations(filters, new List<string> { "ID", "Finish_x0020_Date" }, null, false);

                                    if (allocations != null && allocations.Count > 0 && allocations[0] != null)
                                    {
                                        if (allocations[0].Finish_x0020_Date.HasValue)
                                        {
                                            if (allocations[0].Finish_x0020_Date >= currentWeekStartDate)
                                            {
                                                for(int z = 0; z < 7; z++)
                                                {
                                                    if(allocations[0].Finish_x0020_Date < currentWeekStartDate)
                                                    {
                                                        trackItem.Hours[z] = 0;
                                                    }

                                                    currentWeekStartDate = currentWeekStartDate.AddDays(1);
                                                }

                                                validItems.Add(trackItem);
                                            }
                                        }
                                    }
                                }
                            }

                            fullWeeks.Response.Weeks[0].TrackableItemsInfo = validItems;
                            response = fullWeeks;
                        }
                        else
                        {
                            response = fullWeeks;
                        }
                        */
                        #endregion

                        response = fullWeeks;
                    }
                    else
                    {
                        response.ErrorMessage = fullWeeks.ErrorMessage;
                    }
                }
                else
                {
                    //there are no previous weeks
                    response.Status = true;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }


        public ServiceResponse<TrackTimeHoursResponse> GetHeadcountHours(TrackTimeRequestInfo requestInfo)
        {
            TimeTrackingRepository repository = this.GetRepository();
            ServiceResponse<TrackTimeHoursResponse> response = new ServiceResponse<TrackTimeHoursResponse> { Status = false };

            try
            {
                List<TimeTrackingHours> headcountHrs = repository.GetHeadcountHours(requestInfo.HeadcountID, requestInfo.Year);

                response.Status = true;

                if (headcountHrs != null && headcountHrs.Count > 0)
                {
                    TrackTimeHeadcountResponse hrsResponse = new TrackTimeHeadcountResponse
                    {
                        HeadcountID = requestInfo.HeadcountID,
                        TrackableItemHours = new List<TrackableItemInfo>()
                    };

                    hrsResponse.MonthHours = new decimal[12];

                    //get totals per month
                    var months = headcountHrs.GroupBy(i => i.Month).Select(hh => new { Month = hh.Key, Total = hh.Sum(h => h.TotalHours) });
                    for (int z = 0; z <= 11; z++)
                    {
                        var targetMonth = months.Where(m => m.Month == (z + 1)).FirstOrDefault();
                        if (targetMonth != null)
                        {
                            hrsResponse.MonthHours[z] = targetMonth.Total;
                        }
                    }

                    //get totals by allocation
                    var types = headcountHrs.GroupBy(i => i.TrackingType);

                    foreach (var type in types)
                    {
                        var allocationsThisType = type.GroupBy(z => z.ListItemID);

                        foreach (var allocation in allocationsThisType)
                        {
                            TrackableItemInfo trackableItem = new TrackableItemInfo { Type = type.Key, ListItemID = allocation.Key, Hours = new decimal[12] };

                            //there wont be more than one month per allocation
                            var allocation_items = allocation.ToList();
                            for (int z = 0; z <= 11; z++)
                            {
                                var targetMonth = allocation_items.Where(m => m.Month == (z + 1)).FirstOrDefault();
                                if (targetMonth != null)
                                {
                                    trackableItem.Hours[z] = targetMonth.TotalHours;
                                }
                            }

                            hrsResponse.TrackableItemHours.Add(trackableItem);
                        }
                    }

                    response.Response = new TrackTimeHoursResponse { Headcounts = new List<TrackTimeHeadcountResponse> { hrsResponse } };
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ServiceResponse<TrackTimeHoursResponse> GetHoursByDatesRange(TrackTimeRequestInfo requestInfo)
        {
            TimeTrackingRepository repository = this.GetRepository();
            ServiceResponse<TrackTimeHoursResponse> response = new ServiceResponse<TrackTimeHoursResponse> { Status = false };

            try
            {
                DateTime startDate = new DateTime(requestInfo.StartDateUp[0], requestInfo.StartDateUp[1], requestInfo.StartDateUp[2]);
                DateTime endDate = new DateTime(requestInfo.EndDateUp[0], requestInfo.EndDateUp[1], requestInfo.EndDateUp[2]);

                int status = (int)WeekStatus.Approved;
                if (requestInfo.StatusID != null)
                {
                    status = requestInfo.StatusID[0];
                }

                List<TimeTrackingHours> hrsDbResponse = repository.GetHoursByDatesRange(startDate, endDate, requestInfo.HeadcountID, status, requestInfo.ProgramID, requestInfo.ProjectListItemID, requestInfo.LocationID);
                List<TimeTrackingWeekHours> weekDbResponse = repository.GetHoursByWeekRange(startDate, endDate, requestInfo.HeadcountID, requestInfo.ProgramID, requestInfo.ProjectListItemID, requestInfo.LocationID);
                //LogController.LogMessage(99, "Cat", null, string.Format("AreaID: {0}", requestInfo.AreaID));
                List<TimeTrackingWeekHours> weekAllHoursDbResponse = repository.GetAllHoursPerWeek(startDate, endDate, requestInfo.AreaID);
                List<TimeTrackingHours> hrsMonthDbResponse = repository.GetAllHoursPerMonth(startDate, endDate, requestInfo.AreaID);

                //repository throw exception if it fails
                response.Status = true;
                response.Response = new TrackTimeHoursResponse();

                //targetMonths months
                var arraySize = GetMonthDifference(startDate, endDate) + 1;
                List<KeyValuePair<int, int>> targetMonths = new List<KeyValuePair<int, int>>();
                DateTime initialDate = startDate;
                for (int z = 0; z < arraySize; z++)
                {
                    targetMonths.Add(new KeyValuePair<int, int>(initialDate.Year, initialDate.Month));
                    initialDate = initialDate.AddMonths(1);
                }

                response.Response.Headcounts = new List<TrackTimeHeadcountResponse>();
                if (hrsDbResponse != null && hrsDbResponse.Count > 0)
                {
                    //group by headcount

                    var headcountItems = hrsDbResponse.GroupBy(i => i.HeadcountID);

                    foreach (var item in headcountItems)
                    {
                        var itemsForThisHeadcount = item.ToList();

                        if (itemsForThisHeadcount.Count > 0)
                        {
                            TrackTimeHeadcountResponse headcountItemResponse = new TrackTimeHeadcountResponse
                            {
                                HeadcountID = itemsForThisHeadcount.FirstOrDefault().HeadcountID,
                                TrackableItemHours = new List<TrackableItemInfo>()
                            };

                            headcountItemResponse.MonthHours = new decimal[arraySize];

                            /*
                            List<KeyValuePair<int, int>> targetMonths = new List<KeyValuePair<int, int>>();
                            DateTime initialDate = startDate;
                            for (int z = 0; z < headcountItemResponse.MonthHours.Length; z++)
                            {
                                targetMonths.Add(new KeyValuePair<int, int>(initialDate.Year, initialDate.Month));
                                initialDate = initialDate.AddMonths(1);
                            }
                            */

                            //get totals per month
                            var yearItems = itemsForThisHeadcount.GroupBy(i => i.Year).OrderBy(k => k.Key);
                            foreach (var _year in yearItems)
                            {
                                var itemsForThisYear = _year.ToList();

                                var months = itemsForThisYear.GroupBy(i => i.Month).Select(hh => new { Month = hh.Key, Total = hh.Sum(h => h.TotalHours), Item = hh.FirstOrDefault() });
                                var index = 0;
                                foreach (var targetMonth in targetMonths)
                                {
                                    var _month = months.Where(m => (m.Item.Year == targetMonth.Key && m.Item.Month == targetMonth.Value)).FirstOrDefault();
                                    if (_month != null)
                                    {
                                        headcountItemResponse.MonthHours[index] = _month.Total;
                                    }
                                    index++;
                                }
                            }

                            //get totals by allocation                            
                            var types = itemsForThisHeadcount.GroupBy(i => i.TrackingType);

                            foreach (var type in types)
                            {
                                var allocationsThisType = type.GroupBy(z => z.ListItemID);

                                foreach (var allocation in allocationsThisType)
                                {
                                    TrackableItemInfo trackableItem = new TrackableItemInfo
                                    {
                                        Type = type.Key,
                                        ListItemID = allocation.Key,
                                        Hours = new decimal[arraySize]
                                    };

                                    //no need to sum cuz there wont be more than one month per allocation
                                    var allocation_items = allocation.ToList();

                                    if (type.Key == TrackingType.Allocation/* || type.Key == TrackingType.Special*/)//May10-2020: adding special separates hrs per country, which in the modal causes the impression on the user thare are duplicated items. Ti fixed a country column would need to be added to the modal 
                                    {
                                        var allocationFirstItem = allocation.FirstOrDefault();

                                        trackableItem.ProgramID = allocationFirstItem.ProgramID;
                                        trackableItem.ProjectID = allocationFirstItem.ProjectID;
                                        trackableItem.AreaID = allocationFirstItem.AreaID;
                                        trackableItem.Countries = new List<CountryInfo>();

                                        //group by country here
                                        var countries_thisAllocation = allocation.GroupBy(z => z.CountryID);
                                        foreach (var countryItem in countries_thisAllocation)
                                        {
                                            CountryInfo countryInfo = new CountryInfo { CountryID = countryItem.Key, Hours = new decimal[arraySize] };
                                            trackableItem.Countries.Add(countryInfo);
                                        }

                                        foreach (var countryItem in trackableItem.Countries)
                                        {
                                            var index = 0;
                                            foreach (var targetMonth in targetMonths)
                                            {
                                                var _month = allocation_items.Where(m => (m.Year == targetMonth.Key && m.Month == targetMonth.Value) && m.CountryID == countryItem.CountryID).FirstOrDefault();
                                                if (_month != null)
                                                {
                                                    countryItem.Hours[index] = _month.TotalHours;
                                                    //accumulate
                                                    trackableItem.Hours[index] += _month.TotalHours;
                                                }
                                                index++;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var index = 0;
                                        foreach (var targetMonth in targetMonths)
                                        {
                                            var _month = allocation_items.Where(m => (m.Year == targetMonth.Key && m.Month == targetMonth.Value));//.FirstOrDefault();
                                            var _monthItems = _month.ToList();
                                            if (_monthItems != null && _monthItems.Count > 0)
                                            {
                                                foreach (var _monthItem in _monthItems)
                                                {
                                                    trackableItem.Hours[index] += _monthItem.TotalHours;
                                                }
                                            }
                                            index++;
                                        }
                                    }

                                    headcountItemResponse.TrackableItemHours.Add(trackableItem);
                                }
                            }

                            response.Response.Headcounts.Add(headcountItemResponse);
                        }
                    }
                }

                response.Response.Weeks = new List<TrackTimeWeekHoursResponse>();
                if (weekDbResponse != null && weekDbResponse.Count > 0)
                {
                    foreach (var weekItem in weekDbResponse)
                    {
                        TrackTimeWeekHoursResponse item = new TrackTimeWeekHoursResponse
                        {
                            StartDateDown = new[] { weekItem.StartDate.Year, weekItem.StartDate.Month, weekItem.StartDate.Day },
                            TotalHours = weekItem.TotalHours
                        };

                        response.Response.Weeks.Add(item);
                    }
                }

                response.Response.WeeksAvg = new List<TrackTimeWeekHoursResponse>();
                if (weekAllHoursDbResponse != null && weekAllHoursDbResponse.Count > 0)
                {
                    var weeks = weekAllHoursDbResponse.GroupBy(i => i.StartDate).Select(w => new { StartDate = w.Key, TotalHours = w.Sum(h => h.TotalHours), TotalHeadcounts = w.ToList().Count });

                    foreach (var week in weeks)
                    {
                        TrackTimeWeekHoursResponse avgWeek = new TrackTimeWeekHoursResponse
                        {
                            StartDateDown = new int[] { week.StartDate.Year, week.StartDate.Month, week.StartDate.Day },
                            TotalHours = week.TotalHours / week.TotalHeadcounts
                        };

                        response.Response.WeeksAvg.Add(avgWeek);
                    }
                }

                response.Response.MonthsAvg = new TrackTimeHeadcountResponse
                {
                    MonthHours = new decimal[arraySize]
                };
                if (hrsMonthDbResponse != null && hrsMonthDbResponse.Count > 0)
                {

                    //get totals per month
                    var yearItems = hrsMonthDbResponse.GroupBy(i => i.Year).OrderBy(k => k.Key);
                    foreach (var _year in yearItems)
                    {
                        var itemsForThisYear = _year.ToList();

                        var months = itemsForThisYear.GroupBy(i => i.Month).Select(m => new { Month = m.Key, TotalHours = m.Sum(h => h.TotalHours), TotalHeadcounts = m.ToList().Count, Item = m.FirstOrDefault() });
                        var index = 0;
                        foreach (var targetMonth in targetMonths)
                        {
                            var _month = months.Where(m => (m.Item.Year == targetMonth.Key && m.Item.Month == targetMonth.Value)).FirstOrDefault();
                            if (_month != null)
                            {
                                response.Response.MonthsAvg.MonthHours[index] = _month.TotalHours / _month.TotalHeadcounts;
                            }
                            index++;
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                response.Status = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ServiceResponse<TrackTimeExportResponse> GetHoursForExport(TrackTimeRequestInfo requestInfo)
        {
            TimeTrackingRepository repository = this.GetRepository();
            ServiceResponse<TrackTimeExportResponse> response = new ServiceResponse<TrackTimeExportResponse> { Status = false };

            try
            {
                DateTime startDate = new DateTime(requestInfo.StartDateUp[0], requestInfo.StartDateUp[1], requestInfo.StartDateUp[2]);
                DateTime endDate = new DateTime(requestInfo.EndDateUp[0], requestInfo.EndDateUp[1], requestInfo.EndDateUp[2]);

                List<TimeTrackingItemWeekDay> hrsDbResponse = repository.GetHoursPerDayByDatesRange(startDate, endDate, requestInfo.HeadcountID, requestInfo.ProgramID, requestInfo.ProjectListItemID, requestInfo.LocationID);

                //repository throw exception if it fails
                response.Status = true;

                if (hrsDbResponse != null && hrsDbResponse.Count > 0)
                {
                    response.Response = new TrackTimeExportResponse { Items = new List<TrackTimeExportItemInfo>() };

                    foreach (var item in hrsDbResponse)
                    {
                        TrackTimeExportItemInfo newItem = new TrackTimeExportItemInfo
                        {
                            HeadcountID = item.HeadcountID,
                            ListItemID = item.ListItemID,
                            Type = item.TrackingType,
                            ProgramID = item.ProgramID,
                            ProjectID = item.ProjectID,
                            AreaID = item.AreaID,
                            CountryID = item.CountryID,
                            TrackedDate = item.TrackedDate,
                            Hours = item.Hours
                        };

                        response.Response.Items.Add(newItem);
                    }
                }

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public ServiceResponse<TrackTimeMissingWeeksResponse> GetMissingWeeks(TrackTimeRequestInfo requestInfo, bool GetAll = false)
        {
            TimeTrackingRepository repository = this.GetRepository();

            ServiceResponse<TrackTimeMissingWeeksResponse> response = new ServiceResponse<TrackTimeMissingWeeksResponse> { Status = false };

            if (base.ConfigurationData != null)
            {
                try
                {
                    //figure out if is Super RM
                    bool isSuperRM = false;
                    if (!GetAll)
                    {
                        SharePointController spController = new SharePointController();
                        isSuperRM = spController.IsMemberOfSharePointGroup(base.ConfigurationData.TimesheetPendingToReportManagementGroup, requestInfo.Manager);
                    }

                    if (isSuperRM || GetAll)
                    {
                        //remove Manager from parameters
                        requestInfo.Manager = null;
                    }

                    //get the range of weeks to return
                    DateTime? startDate = null;

                    DateTime? endDate = null;

                    if (requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3)
                    {
                        startDate = new DateTime(requestInfo.StartDateUp[0], requestInfo.StartDateUp[1], requestInfo.StartDateUp[2]);
                    }
                    if (requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3)
                    {
                        endDate = new DateTime(requestInfo.EndDateUp[0], requestInfo.EndDateUp[1], requestInfo.EndDateUp[2]);
                    }

                    //call storage
                    List<TimeTrackingMissingWeekItem> dbResponse = new List<TimeTrackingMissingWeekItem>();

                    if (GetAll)
                    {
                        dbResponse = repository.GetMissingWeeks(startDate, endDate,
                            (requestInfo.HeadcountID > 0 ? (int?)requestInfo.HeadcountID : null), requestInfo.Manager
                            );//Oct17-2018 requestInfo.Manager will always be passed null. it's being left so in case that changes in the future
                    }
                    else
                    {
                        var filteredDbResponse = repository.GetFilteredMissingWeeks(startDate, endDate,
                            (requestInfo.HeadcountID > 0 ? (int?)requestInfo.HeadcountID : null),
                            requestInfo.ProgramID, requestInfo.ProjectListItemID, requestInfo.LocationID, requestInfo.Manager
                            );

                        foreach (var item in filteredDbResponse)
                        {
                            dbResponse.Add(item as TimeTrackingMissingWeekItem);
                        }
                    }

                    List<MissingWeekInfo> headcounts = new List<MissingWeekInfo>();

                    //group by headcountId
                    var headcountItems = dbResponse.GroupBy(x => x.HeadcountID).Select(y => y);

                    //for each Week
                    foreach (var itemHeadcount in headcountItems)
                    {
                        MissingWeekInfo weekInfo = new MissingWeekInfo { HeadcountID = itemHeadcount.Key, Weeks = new List<int[]>() };
                        var lastWeek = repository.GetLastSubmittedWeek(itemHeadcount.Key);
                        if (lastWeek.Count > 0 && lastWeek[0] != null)
                        {
                            weekInfo.LastWeekSubmitted = new int[] { lastWeek[0].StartDate.Year, lastWeek[0].StartDate.Month - 1, lastWeek[0].StartDate.Day };
                        }

                        //get list of weeks
                        var weeksList = itemHeadcount.GroupBy(w => w.StartDate);

                        foreach (var itemWeek in weeksList)
                        {
                            weekInfo.Weeks.Add(new int[] { itemWeek.Key.Year, itemWeek.Key.Month - 1, itemWeek.Key.Day });
                        }

                        headcounts.Add(weekInfo);
                    }

                    response.Status = true;
                    response.Response = new TrackTimeMissingWeeksResponse
                    {
                        Status9 = !isSuperRM,
                        Headcounts = headcounts
                    };
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                }
            }

            return response;
        }

        public void InitializeHeadcounts()
        {
            //TimeTrackingRepository repository = this.GetRepository();
            LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Test {0}", 9));
            try
            {
                ConfigurationResourceManagementServices configurationData = new ConfigurationResourceManagementServices();

                List<HeadcountItem> items = GetAllHeadcountsFromSP(configurationData, new KeyValuePair<string, string>("MustReportHours", "1"), "Integer", new List<string> { "ID", "Resource_x0020_Manager", "Hire_x0020_Date", "Finish_x0020_Date" }, true, false);

                //TimeTrackingRepository repository = this.GetRepository();
                LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Initializing {0} headcounts", items.Count));

                foreach (var item in items)
                {

                    TrackTimeRequestInfo newItem = new TrackTimeRequestInfo { HeadcountID = item.ID };
                    ProcessHeadcount(newItem);
                    /*
                    if (!string.IsNullOrEmpty(item.ResourceManagerLoginName))
                    {
                        newItem.Manager = item.ResourceManagerLoginName;
                    }

                    try
                    {
                        repository.AddHeadcount(newItem);
                    }
                    catch (Exception ex)
                    {

                    }*/
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void ProcessHeadcount(TrackTimeRequestInfo requestInfo)
        {
            //logging
            LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                string.Format("Processing Headcount... {0}", requestInfo.HeadcountID));

            if (base.ConfigurationData != null)
            {
                try
                {
                    //get headcount
                    List<string> allFields = new List<string> { "ID", "Last_x0020_Name", "FirstName", "EMail", "Cemex_x0020_ID", "Contact_x0020_Number",
                    "Hire_x0020_Date","Finish_x0020_Date","Employee_x0020_Type","Headcount_x0020_Type","Financial_x0020_Type","CEMEX_x0020_or_x0020_NEORIS_x002",
                    "Monthly_x0020_Cost_x0020_KUSD","Resource_x0020_Status","Contract_x0020_Type","Hrs_x0020_per_x0020_day","Source_x0020_Company","HeadCount_x0020_Position",
                    "Pit_x0020_Area","Related_x0020_Role","Location_x0020_Country","Resource_x0020_Manager","DOB","Skills","SeniorityLevel",
                    "MustReportHours","MustReceiveNotifications","HourRate","ContractNumber","HeadcountAccount", "Rating", "ResourceAnexo", "ResourceFCC", "ResourceRITM", "ResourceCNTR"};


                    HeadcountItem headcount = GetHeadcountFromSP(base.ConfigurationData, requestInfo.HeadcountID, allFields, true, true, true);

                    if (headcount != null)
                    {
                        //1. sync Headcount Log

                        HeadcountRecord newRecord = headcount.CreateRecordInstance();
                        DateTime validUntil = newRecord.ValidFrom.AddSeconds(-1);

                        HeadcountLogRepository repository = new HeadcountLogRepository();

                        //update previous record
                        repository.UpdatePreviousHeadcountLogEntry(requestInfo.HeadcountID, validUntil);

                        //add new record
                        repository.AddLogEntry(newRecord);


                        //2. sync timesheet stuff
                        //if (headcount.MustReportHours)
                        {
                            TimeTrackingRepository repositoryTs = this.GetRepository();
                            //try get record from DB

                            TimeTrackingHeadcount existingItem = repositoryTs.GetHeadcount(headcount.ID);

                            if (existingItem != null)
                            {
                                //set new values
                                string fullName = string.Format("{0} {1}", !string.IsNullOrEmpty(headcount.FirstName) ? headcount.FirstName : string.Empty,
                                    !string.IsNullOrEmpty(headcount.Last_x0020_Name) ? headcount.Last_x0020_Name : string.Empty);

                                repositoryTs.UpdateHeadcount(headcount.ID, headcount.ResourceManagerLoginName, fullName, headcount.Pit_x0020_AreaID);

                                LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Headcount Updated... {0}", requestInfo.HeadcountID));
                            }
                            else
                            {
                                //add new record
                                TimeTrackingHeadcount newItem = new TimeTrackingHeadcount { HeadcountID = headcount.ID };

                                if (!string.IsNullOrEmpty(headcount.ResourceManagerLoginName))
                                {
                                    newItem.Manager = headcount.ResourceManagerLoginName;
                                }

                                if (!string.IsNullOrEmpty(headcount.FirstName) || !string.IsNullOrEmpty(headcount.Last_x0020_Name))
                                {
                                    newItem.FullName = string.Format("{0} {1}", !string.IsNullOrEmpty(headcount.FirstName) ? headcount.FirstName : string.Empty,
                                        !string.IsNullOrEmpty(headcount.Last_x0020_Name) ? headcount.Last_x0020_Name : string.Empty);
                                }

                                if (headcount.Pit_x0020_AreaID.HasValue)
                                {
                                    newItem.AreaID = headcount.Pit_x0020_AreaID.Value;
                                }

                                repositoryTs.AddHeadcount(newItem);

                                LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Headcount Added... {0}", requestInfo.HeadcountID));
                            }
                        }
                    }
                    else
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                            string.Format("Headcount ID not found... {0}", requestInfo.HeadcountID));
                    }
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Processing Headcount failed: {0} : {1}", requestInfo.HeadcountID, ex.Message));
                }
            }
        }

        public void CalculateTeamIDs(TrackTimeSaveRequest request)
        {
            LogController.LogMessage(ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("CalculateTeamIDs running... {0}", string.Empty));

            List<Model.ResourceManagement.AllocationTeamItem> itemsAlloc = new List<Model.ResourceManagement.AllocationTeamItem>();
            List<Model.ResourceManagement.TrackingCategory> itemsTrackings = new List<Model.ResourceManagement.TrackingCategory>();
            List<Model.ResourceManagement.TrackingCategoryItem> itemsTrackingItems = new List<Model.ResourceManagement.TrackingCategoryItem>();

            //Get configuration
            if (base.ConfigurationData != null)
            {
                //check if request contains Allocation of special tracking
                bool hasAllocation = false;
                foreach (var item in request.Week.TrackableItemsInfo)
                {
                    if (item.Type == TrackingType.Allocation || item.Type == TrackingType.Special)
                    {
                        hasAllocation = true;
                        break;
                    }
                }

                if (hasAllocation)
                {
                    //get Resource's allocations
                    using (ClientContext tenantContext = QueryHelper.GetClientContext(this.ConfigurationData.TenantUrl))
                    {
                        string query = string.Empty;

                        query = string.Format(@"
                                <View>
                                    <Query>
                                        <Where>
                                            <Eq>
                                                <FieldRef Name='Resource_x0020_Name'  LookupId='TRUE' />
                                                <Value Type='Text'>{0}</Value> 
                                            </Eq> 
                                        </Where>
                                    </Query>
                                </View>", request.HeadcountId);

                        List<string> fields = new List<string> { "ID", "Team", "Project_x0020_Assigned_x0020_Nam" };
                        ListItemCollection _itemsAlloc = QueryHelper.QuerySharepointList(tenantContext, "PITHeadcountAlloc", query, fields);

                        if (_itemsAlloc != null && _itemsAlloc.Count > 0)
                        {
                            foreach (var listItem in _itemsAlloc)
                            {
                                Model.ResourceManagement.AllocationTeamItem item = EntityHelper.GetInstance<Model.ResourceManagement.AllocationTeamItem>(listItem, fields);
                                itemsAlloc.Add(item);
                            }
                        }
                    }

                    //get TimesheetSpecialTrackingCategories
                    using (ClientContext tenantContext = QueryHelper.GetClientContext(this.ConfigurationData.TenantUrl))
                    {
                        string query = string.Empty;

                        query = string.Format(@"
                                <View>
                                    <Query>
                                        <Where>
                                            <Geq>
                                                <FieldRef Name='{0}' />
                                                <Value Type='{1}'>{2}</Value> 
                                            </Geq> 
                                        </Where>
                                    </Query>
                                </View>", "ID", "Integer", "0");

                        List<string> fields = new List<string> { "ID", "Initiative" };
                        ListItemCollection _itemsS = QueryHelper.QuerySharepointList(tenantContext, "TimesheetSpecialTrackingCategories", query, fields);

                        if (_itemsS != null && _itemsS.Count > 0)
                        {
                            foreach (var listItem in _itemsS)
                            {
                                Model.ResourceManagement.TrackingCategory item = EntityHelper.GetInstance<Model.ResourceManagement.TrackingCategory>(listItem, fields);
                                itemsTrackings.Add(item);
                            }
                        }
                    }

                    //get TimesheetSpecialTrackingCategories
                    using (ClientContext tenantContext = QueryHelper.GetClientContext(this.ConfigurationData.TenantUrl))
                    {
                        string query = string.Empty;

                        query = string.Format(@"
                                <View>
                                    <Query>
                                        <Where>
                                            <Geq>
                                                <FieldRef Name='{0}' />
                                                <Value Type='{1}'>{2}</Value> 
                                            </Geq> 
                                        </Where>
                                    </Query>
                                </View>", "ID", "Integer", "0");

                        List<string> fields = new List<string> { "ID", "Category" };
                        ListItemCollection _itemsTi = QueryHelper.QuerySharepointList(tenantContext, "TimesheetSpecialTracking", query, fields);

                        if (_itemsTi != null && _itemsTi.Count > 0)
                        {
                            foreach (var listItem in _itemsTi)
                            {
                                Model.ResourceManagement.TrackingCategoryItem item = EntityHelper.GetInstance<Model.ResourceManagement.TrackingCategoryItem>(listItem, fields);
                                itemsTrackingItems.Add(item);
                            }
                        }
                    }

                    TimesheetController controller = new TimesheetController();
                    //get week from Db
                    var svcResponse = controller.GetWeekRaw(request.HeadcountId, request.Week.StartDateUp);
                    if(svcResponse.Status)
                    {
                        TimeTrackingRepository repository = new TimeTrackingRepository();
                        //make associations
                        List<int> weeksDone = new List<int>();
                        foreach(var itemWeek in svcResponse.Response)
                        {
                            bool found = false;
                            foreach(var wid in weeksDone)
                            {
                                if(wid == itemWeek.WeekDbID)
                                {
                                    found = true;
                                    break;
                                }
                            }

                            if (!found)
                            {
                                if (itemWeek.TrackingType == TrackingType.Allocation || itemWeek.TrackingType == TrackingType.Special)
                                {
                                    //get tracking item
                                    var trackingCatItem = itemsTrackingItems.Where(a => a.ID == itemWeek.ListItemID).FirstOrDefault();
                                    if (trackingCatItem != null && trackingCatItem.Category != null && trackingCatItem.Category.LookupId > 0)
                                    {
                                        //get tracking item
                                        var trackingCat = itemsTrackings.Where(a => a.ID == trackingCatItem.Category.LookupId).FirstOrDefault();
                                        if (trackingCat != null)
                                        {
                                            //get allocation
                                            foreach (var itemAlloc in itemsAlloc)
                                            {
                                                if (itemAlloc.Project_x0020_Assigned_x0020_Nam != null && itemAlloc.Project_x0020_Assigned_x0020_Nam.LookupId > 0 &&
                                                    itemAlloc.Project_x0020_Assigned_x0020_Nam.LookupId == trackingCat.Initiative.LookupId)
                                                {
                                                    if (itemAlloc != null && itemAlloc.Team != null && itemAlloc.Team.LookupId > 0)
                                                    {
                                                        //persist TeamId
                                                        repository.SetTeamId(itemWeek.WeekDbID, itemAlloc.Team.LookupId);
                                                        LogController.LogMessage(ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Updated WeekDbId: {0}  - TeamID : {1}", itemWeek.WeekDbID, itemAlloc.Team.LookupId));

                                                        weeksDone.Add(itemWeek.WeekDbID);
                                                        break;
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //log
                        LogController.LogMessage(ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Error when getting GetWeekRaw", svcResponse.ErrorMessage));
                    }

                }

            }
            else
            {
                    LogController.LogMessage(ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("ConfigurationData invalid{0}", string.Empty));
            }
        }

        #region Private Methods

        private bool ValidateRequest(TrackTimeSaveRequest request)
        {
            //initial checkings
            if (request != null && request.HeadcountId > 0 && !string.IsNullOrEmpty(request.Sender) && request.Week != null)
            {
                try
                {
                    //check week start date
                    DateTime startDate = new DateTime(request.Week.StartDateUp[0], request.Week.StartDateUp[1], request.Week.StartDateUp[2]);

                    ConfigurationResourceManagementServices configurationData = new ConfigurationResourceManagementServices();

                    if (startDate.Date >= configurationData.TimesheetDeployDate.Date)
                    {
                        //check trackable items
                        if (request.Week.TrackableItemsInfo != null && request.Week.TrackableItemsInfo.Count > 0)
                        {
                            bool isOk = true;

                            //check for repeated items

                            //group by ListItemID
                            var listitemItems = request.Week.TrackableItemsInfo.GroupBy(x => x.ListItemID).Select(y => y);

                            foreach (var listitemItem in listitemItems)
                            {
                                var listitem_items = listitemItem.ToList();

                                //if there is more than one ListItemID, they have to be of different type
                                if (listitem_items.Count > 1)
                                {
                                    //group by type
                                    var typeItems_currentListitem = listitem_items.GroupBy(x => x.Type).Select(y => y);

                                    foreach (var typeItem in typeItems_currentListitem)
                                    {
                                        var typeItem_items = typeItem.ToList();

                                        //if there is more than one Type, they have to be of different country
                                        if (typeItem_items.Count > 1)
                                        {
                                            //group by country
                                            var countryItems_currentType = typeItem_items.GroupBy(x => x.CountryID).Select(y => y);

                                            foreach (var countryItem in countryItems_currentType)
                                            {
                                                var countryItem_items = countryItem.ToList();

                                                //if there is more then one, they are repeated
                                                if (countryItem_items.Count > 1)
                                                {
                                                    isOk = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //continue checking if still Ok
                            if (isOk)
                            {
                                foreach (var trackableItem in request.Week.TrackableItemsInfo)
                                {
                                    bool itemOk = false;

                                    if (trackableItem.ListItemID > 0)
                                    {
                                        if (trackableItem.Type == TrackingType.Service
                                            || trackableItem.Type == TrackingType.OrderTaking
                                            || trackableItem.Type == TrackingType.DeliveryTracking
                                            || trackableItem.Type == TrackingType.Others
                                            || trackableItem.Type == TrackingType.CemexOnline
                                            || trackableItem.Type == TrackingType.CrossTracks
                                            || trackableItem.Type == TrackingType.AuthorizedInitiatives
                                            || trackableItem.Type == TrackingType.Special
                                            || trackableItem.Type == TrackingType.Administrative
                                            || trackableItem.Type == TrackingType.NonProductive
                                            || (trackableItem.Type == TrackingType.Allocation && (trackableItem.ProjectID != null || trackableItem.AreaID != null))
                                            || (trackableItem.Type == TrackingType.Allocation && trackableItem.ProjectID != null && trackableItem.ProjectID.HasValue && trackableItem.ProjectID.Value > 0
                                                    && trackableItem.CountryID.HasValue && trackableItem.CountryID.Value > 0)
                                            || (trackableItem.Type == TrackingType.Allocation && trackableItem.AreaID != null && trackableItem.AreaID.HasValue && trackableItem.AreaID.Value > 0)
                                        )
                                        {
                                            if (IsSharePointConsistent(request.HeadcountId, startDate, trackableItem))
                                            {
                                                if (trackableItem.Hours != null && trackableItem.Hours.Length == 7)
                                                {
                                                    bool allHrsOk = true;

                                                    //hours must be >= Zero
                                                    for (int z = 0; z <= 6; z++)
                                                    {
                                                        if (trackableItem.Hours[z] < 0)
                                                        {
                                                            allHrsOk = false;
                                                            break;
                                                        }
                                                    }

                                                    itemOk = (allHrsOk == true);
                                                }
                                            }
                                        }
                                    }

                                    if (!itemOk)
                                    {
                                        isOk = false;
                                        break;
                                    }
                                }
                            }

                            //if still ok, check limits per week & day
                            var totalHrs = request.Week.TrackableItemsInfo.Sum(i => i.TotalHours);
                            if (totalHrs > this.limitPerWeek)
                            {
                                isOk = false;
                            }

                            if (isOk)
                            {
                                for (int z = 0; z <= 6; z++)
                                {
                                    var totalPerDay = request.Week.TrackableItemsInfo.Sum(i => i.Hours[z]);

                                    if (totalPerDay > limitPerDay)
                                    {
                                        isOk = false;
                                        break;
                                    }
                                }
                            }

                            return (isOk == true);
                        }
                    }
                    else
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, "ValidateRequest", request != null ? request.HeadcountId.ToString() : "n/a",
                            string.Format("DeployDate validation failed. StartDate: {0}", startDate));
                    }
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, "ValidateRequest", request != null ? request.HeadcountId.ToString() : "n/a", ex.Message);
                }
            }

            return false;
        }

        private bool ValidateApproveRequest(TrackTimeApproveRequest request)
        {
            bool response = false;

            if (!string.IsNullOrEmpty(request.Approver) && request.Weeks != null && request.Weeks.Count > 0)
            {
                //logging
                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Validating approval request. Approver:{0} IsRejected:{1} Comments:{2}: WeeksCount: {3}", request.Approver, request.IsRejected, request.Comments ?? "#NONE#", request.Weeks.Count.ToString()));

                if (!request.IsRejected || (request.IsRejected && !string.IsNullOrEmpty(request.Comments)))
                {
                    try
                    {
                        var allWeeksOk = true;

                        //Individual Week checks moved to just-before approval, to avoid returning the whole request with an error when just one week has inconsistent data

                        /*
                        foreach (var weekData in request.Weeks)
                        {
                            var weekOk = false;
                            if (weekData.Length == 4)
                            {
                                DateTime dt = new DateTime(weekData[0], weekData[1], weekData[2]);
                                //get week from DB
                                ServiceResponse<TrackTimeWeeksResponse> weekFromDbResponse = this.GetWeek(new TrackTimeRequestInfo
                                {
                                    HeadcountID = weekData[3],
                                    StartDateUp = new int[] { weekData[0], weekData[1], weekData[2] }
                                });

                                if (weekFromDbResponse.Status && weekFromDbResponse.Response.Weeks != null && weekFromDbResponse.Response.Weeks.Count == 1 && weekFromDbResponse.Response.Weeks[0].Status == WeekStatus.Submitted)
                                {
                                    weekOk = true;
                                }
                                else
                                {
                                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Inconsistent Data: More than 1 week or week not submitted: Approver: {0}", request.Approver));
                                }
                            }

                            if (!weekOk)
                            {
                                allWeeksOk = false;
                                break;
                            }
                        }
                        */

                        response = allWeeksOk;
                    }
                    catch (Exception ex)
                    {
                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Approver: {0} => {1}", request.Approver, ex.Message));
                        response = false;
                    }
                }
                else
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Request to reject has no comments: Approver: {0}", request.Approver));
                }
            }
            else
            {
                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, "No approver or no Weeks");
            }

            return response;
        }

        private ServiceResponse<bool> ProcessSaveRequest(TrackTimeSaveRequest request)
        {
            //used to identify the same set of records for deleting purposes if need be
            string modifiedKey = string.Format("{0}{1}{2}", request.HeadcountId.ToString(), "-", System.Guid.NewGuid().ToString());
            TimeTrackingRepository repository = this.GetRepository();
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = true };
            string headcountType = string.Empty;

            try
            {
                //check if Headcount Exists
                TimeTrackingHeadcount headcount = repository.GetHeadcount(request.HeadcountId);
                request.Week.StartDateParsed = new DateTime(request.Week.StartDateUp[0], request.Week.StartDateUp[1], request.Week.StartDateUp[2]);

                //LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                //    "QueryHelper.GetClientContext");

                //get Headcount from SP
                using (ClientContext tenantContext = QueryHelper.GetClientContext(this.ConfigurationData.TenantUrl))
                {
                    //LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    //    "QueryHelper.GetClientContext DONE");

                    string query = string.Empty;

                    query = string.Format(@"
                                <View>
                                    <Query>
                                        <Where>
                                            <Eq>
                                                <FieldRef Name='ID' />
                                                <Value Type='Number'>{0}</Value> 
                                            </Eq> 
                                        </Where>
                                    </Query>
                                    <RowLimit>1</RowLimit>
                                </View>", request.HeadcountId);

                    List<string> fields = new List<string> { "ID", "Financial_x0020_Type" };
                    ListItemCollection itemsH = QueryHelper.QuerySharepointList(tenantContext, "HeadCount", query, fields);

                    if (itemsH != null && itemsH.Count == 1)
                    {
                        foreach (var listItem in itemsH)
                        {
                            if (listItem["Financial_x0020_Type"] != null)
                            {
                                headcountType = listItem["Financial_x0020_Type"].ToString();
                            }
                        }
                    }

                }

                if (headcount != null)
                {

                    //check if each trackingItem/country? already exists
                    if (request.Week.TrackableItemsInfo != null)
                    {
                        foreach (var trackingItemRequest in request.Week.TrackableItemsInfo)
                        {
                            #region Deprecated
                            //int teamId = 0;
                            //get allocation
                            //if (trackingItemRequest.Type == TrackingType.Allocation || trackingItemRequest.Type == TrackingType.Special)
                            //{
                            //    int targetAllocationId = trackingItemRequest.Type == TrackingType.Special &&  trackingItemRequest.AllocationID.HasValue ? trackingItemRequest.AllocationID.Value : trackingItemRequest.ListItemID;

                            //    using (ClientContext tenantContext = QueryHelper.GetClientContext(this.ConfigurationData.TenantUrl))
                            //    {
                            //        string query = string.Empty;

                            //        query = string.Format(@"
                            //    <View>
                            //        <Query>
                            //            <Where>
                            //                <Eq>
                            //                    <FieldRef Name='ID' />
                            //                    <Value Type='Number'>{0}</Value> 
                            //                </Eq> 
                            //            </Where>
                            //        </Query>
                            //        <RowLimit>1</RowLimit>
                            //    </View>", targetAllocationId);

                            //        //ListItemCollection items = QueryHelper.QuerySharepointList(tenantContext, headcountListTitle, query, headcountFields);


                            //        List<string> fields = new List<string> { "ID", "Team" };
                            //        ListItemCollection itemsH = QueryHelper.QuerySharepointList(tenantContext, "PITHeadcountAlloc", query, fields);

                            //        if (itemsH != null && itemsH.Count == 1)
                            //        {
                            //            foreach (var listItem in itemsH)
                            //            {
                            //                if (listItem["Team"] != null)
                            //                {
                            //                    FieldLookupValue team = listItem["Team"] as FieldLookupValue;
                            //                    teamId = team.LookupId;
                            //                }
                            //            }
                            //        }

                            //    }

                            //}
                            #endregion

                            //try to get tracking item from Storage
                            TimeTrackingItem trackingItem = repository.GetTrackingItem(request.HeadcountId, trackingItemRequest.ListItemID, trackingItemRequest.CountryID == 0 ? null : trackingItemRequest.CountryID);

                            if (trackingItem != null)
                            {
                                //check if week exists and its status
                                if (trackingItem.TimeTrackingWeeks != null && trackingItem.TimeTrackingWeeks.Count > 0)
                                {
                                    var targetWeek = trackingItem.TimeTrackingWeeks.Where(w => w.StartDate.Date == request.Week.StartDateParsed.Date).FirstOrDefault();

                                    if (targetWeek != null)
                                    {
                                        //week exists, check status
                                        if (targetWeek.Status == SQL.Enums.Timesheet.WeekStatus.Open || targetWeek.Status == SQL.Enums.Timesheet.WeekStatus.Rejected)
                                        {
                                            //status ok, proceed to save
                                            TimeTrackingWeek updatedWeek = this.GetUpdatedTimeTrackingWeek(targetWeek, trackingItemRequest, request.Week, request.Sender, modifiedKey, headcountType);
                                            repository.UpdateTrackingItemWeek(updatedWeek);
                                        }
                                        else
                                        {
                                            throw new Exception("Week is already submitted or approved");
                                        }
                                    }
                                    else
                                    {
                                        //trackingItem/country does not have this week, proceed to save
                                        TimeTrackingWeek newWeek = this.GetNewTimeTrackingWeek(trackingItemRequest, request.Week, request.Sender, modifiedKey, headcountType);
                                        repository.AddTrackingItemWeek(newWeek, trackingItem.Id);
                                    }
                                }
                                else
                                {
                                    //trackingItem/country does not have any weeks, proceed to save
                                    TimeTrackingWeek newWeek = this.GetNewTimeTrackingWeek(trackingItemRequest, request.Week, request.Sender, modifiedKey, headcountType);
                                    repository.AddTrackingItemWeek(newWeek, trackingItem.Id);
                                }
                            }
                            else
                            {
                                //trackingItem/country does not exist for this headcount, proceed to save
                                trackingItem = this.GetNewTimeTrackingItem(trackingItemRequest, request.Week, request.Sender, modifiedKey, headcountType);
                                repository.AddTrackingItem(trackingItem, headcount.HeadcountID);
                            }
                        }
                    }

                    //get current trackings for the week
                    List<TimeTrackingItemWeekDay> currentHeadcountWeeks = repository.GetWeeks(request.Week.StartDateParsed, request.Week.StartDateParsed.AddDays(6), headcount.HeadcountID);

                    // Mar 6,2019 Fix for "Inconsistent Data" bug.
                    //bug ocurred when the Timesheet was rejected, as only "Open" status was allowed to be deleted at the Repository
                    //now also "Rejected" status is allowed at the Repository
                    //Checking for the status of the Request is also discarded, as the Status could also be "Submitted"
                    //it's safe because this is the entry point in the entire service that deletes a week
                    //if (request.Week.Status == WeekStatus.Open)
                    {
                        //previous saved weeks have a different ModifiedKey than the one processed
                        //get them, and delete them
                        var weeksToDelete = currentHeadcountWeeks.Where(w => w.ModifiedKey != modifiedKey).GroupBy(y => y.WeekDbID);
                        foreach (var weekToDelete in weeksToDelete)
                        {
                            repository.DeleteTrackingItemWeek(weekToDelete.Key);
                        }
                    }
                }
                else
                {
                    //headcount does not exist, any further validation is unnecesary, proceed to save
                    headcount = this.GetNewTimeTrackingHeadcount(request, modifiedKey, headcountType);
                    repository.AddHeadcount(headcount);
                }

                if (request.Week.Status == WeekStatus.Submitted)
                {
                    //fire async task
                    HostingEnvironment.QueueBackgroundWorkItem(ct => CalculateMissingWeeks(false, request.HeadcountId));
                }

                CalculateTeamIDs(request);
            }
            catch (Exception ex)
            {
                response.Status = false;
                StringBuilder sb = new StringBuilder();
                sb.Append(ex.Message);
                if (ex.InnerException != null) sb.Append(ex.InnerException.Message);
                sb.Append(ex.StackTrace);
                response.ErrorMessage = sb.ToString();
            }

            return response;
        }

        /*private ServiceResponse<bool> ProcessApproveRequest(TrackTimeApproveRequest request)
        {
            TimeTrackingRepository repository = this.GetRepository();
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = true };

            try
            {
                List<int> headcountIds = new List<int>();

                foreach (var weekData in request.Weeks)
                {
                    DateTime startDate = new DateTime(weekData[0], weekData[1], weekData[2]);
                    repository.SetWeekStatus(weekData[3], startDate, (int)request.Status, request.Approver, request.Comments);

                    //send notification
                    //fire async task
                    if (request.Status == WeekStatus.Rejected)
                    {
                        //SendRejectNotification(weekData[3], startDate, request.Comments);
                        HostingEnvironment.QueueBackgroundWorkItem(ct => SendRejectNotification(weekData[3], startDate, request.Comments));
                    }

                    headcountIds.Add(weekData[3]);
                }

                //fire Queue Item
                var distinctHeadcounts = headcountIds.ToArray().Distinct();
                foreach (int headcountId in distinctHeadcounts)
                {
                    //fire async task
                    HostingEnvironment.QueueBackgroundWorkItem(ct => CalculateMissingWeeks(false, headcountId));
                }
            }
            catch (Exception ex)
            {
                //logging
                LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                response.Status = false;
            }

            return response;
        }*/

        private ServiceResponse<bool> ProcessApproveRequest(TrackTimeApproveRequest request)
        {
            TimeTrackingRepository repository = this.GetRepository();
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = true };

            if (request.Weeks != null && request.Weeks.Count > 0)
            {
                List<int> headcountIds = new List<int>();
                int weeksProcessed = 0;

                foreach (var weekData in request.Weeks)
                {
                    try
                    {
                        if (weekData.Length == 4)
                        {
                            DateTime startDate = new DateTime(weekData[0], weekData[1], weekData[2]);
                            //get week from DB
                            ServiceResponse<TrackTimeWeeksResponse> weekFromDbResponse = this.GetWeek(new TrackTimeRequestInfo
                            {
                                HeadcountID = weekData[3],
                                StartDateUp = new int[] { weekData[0], weekData[1], weekData[2] }
                            });
                            if (weekFromDbResponse.Status && weekFromDbResponse.Response.Weeks != null && weekFromDbResponse.Response.Weeks.Count == 1 )
                            //if (weekFromDbResponse.Status && weekFromDbResponse.Response.Weeks != null && weekFromDbResponse.Response.Weeks.Count == 1 && weekFromDbResponse.Response.Weeks[0].Status == WeekStatus.Submitted)
                            {
                                repository.SetWeekStatus(weekData[3], startDate, (int)request.Status, request.Approver, request.Comments);

                                //send notification
                                //fire async task
                                if (request.Status == WeekStatus.Rejected)
                                {
                                    //SendRejectNotification(weekData[3], startDate, request.Comments);
                                    HostingEnvironment.QueueBackgroundWorkItem(ct => SendRejectNotification(weekData[3], startDate, request.Comments));
                                }

                                headcountIds.Add(weekData[3]);
                                weeksProcessed++;
                            }
                            else
                            {
                                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Inconsistent Data: More than 1 week or week not submitted: Approver: {0}, Headcount: {1}, Week: {2}", request.Approver, weekData[3], startDate.ToString()));
                            }
                        }
                        else
                        {
                            LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Week with invalid data: {0}", request.Approver));
                        }
                    }
                    catch (Exception ex)
                    {
                        response.Status = false;
                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                    }
                }

                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Weeks processded for Approver: {0} => {1}", request.Approver, weeksProcessed.ToString()));

                if (request.Weeks.Count != weeksProcessed)
                {
                    response.Status = false;
                    response.ErrorMessage = "It seems there is at least one timesheet(s) which data does not make sense. Please report it.";
                }

                //fire Queue Item
                var distinctHeadcounts = headcountIds.ToArray().Distinct();
                foreach (int headcountId in distinctHeadcounts)
                {
                    //fire async task
                    HostingEnvironment.QueueBackgroundWorkItem(ct => CalculateMissingWeeks(false, headcountId));
                }
            }

            return response;
        }

        private bool IsSharePointConsistent(int headcountID, DateTime startDate, TrackableItemInfo trackableItem)
        {
            //validation should reject allocation that are Areas
            return true;
        }

        private DateTime GetFutureWeekLimit()
        {
            DateTime today = DateTime.Today;
            DateTime currentWeek = today.AddDays(Convert.ToDouble(today.DayOfWeek) * -1);
            DateTime futureWeek = currentWeek.AddDays(7 * 4); //1 month or so

            return futureWeek;
        }

        private decimal[] GetHours(List<TimeTrackingItemWeekDay> days)
        {
            List<decimal> results = new List<decimal>();

            var sortedList = days.OrderBy(x => x.TrackedDate).Select(y => y);

            foreach (var item in sortedList)
            {
                results.Add(item.Hours);
            }

            return results.ToArray();
        }

        private TimeTrackingHeadcount GetNewTimeTrackingHeadcount(TrackTimeSaveRequest request, string modifiedKey, string headcountType)
        {
            TimeTrackingHeadcount headcount = new TimeTrackingHeadcount();
            headcount.HeadcountID = request.HeadcountId;
            headcount.Manager = request.Manager;
            headcount.TimeTrackingItems = new List<TimeTrackingItem>();

            foreach (var trackingItem in request.Week.TrackableItemsInfo)
            {
                #region deprecated
                //int teamId = 0;
                //get allocation
                //if (trackingItem.Type == TrackingType.Allocation)
                //{
                //    using (ClientContext tenantContext = QueryHelper.GetClientContext(this.ConfigurationData.TenantUrl))
                //    {
                //        string query = string.Empty;

                //        query = string.Format(@"
                //                <View>
                //                    <Query>
                //                        <Where>
                //                            <Eq>
                //                                <FieldRef Name='ID' />
                //                                <Value Type='Number'>{0}</Value> 
                //                            </Eq> 
                //                        </Where>
                //                    </Query>
                //                    <RowLimit>1</RowLimit>
                //                </View>", trackingItem.ListItemID);

                //        List<string> fields = new List<string> { "ID", "Team" };
                //        ListItemCollection itemsH = QueryHelper.QuerySharepointList(tenantContext, "PITHeadcountAlloc", query, fields);

                //        if (itemsH != null && itemsH.Count == 1)
                //        {
                //            foreach (var listItem in itemsH)
                //            {
                //                if (listItem["Team"] != null)
                //                {
                //                    FieldLookupValue team = listItem["Team"] as FieldLookupValue;
                //                    teamId = team.LookupId;
                //                }
                //            }
                //        }
                //    }
                //}
                #endregion
                TimeTrackingItem item = new TimeTrackingItem
                {
                    TrackingType = trackingItem.Type,
                    ListItemID = trackingItem.ListItemID,
                    CountryID = trackingItem.CountryID,
                    ProgramID = trackingItem.ProgramID,
                    ProjectID = trackingItem.ProjectID,
                    AreaID = trackingItem.AreaID,
                                    //TeamId = teamId,
                    HeadcountType = headcountType
                };

                item.TimeTrackingWeeks = new List<TimeTrackingWeek>();

                TimeTrackingWeek week = new TimeTrackingWeek
                {
                    Status = request.Week.Status,
                    StartDate = request.Week.StartDateParsed,
                    EndDate = request.Week.StartDateParsed.AddDays(6),
                    Modified = DateTime.Now,
                    ModifiedBy = request.Sender,
                    TimeTrackingDays = new List<TimeTrackingDay>(),
                    ModifiedKey = modifiedKey,
                    ApprovedOn = null,
                    //TeamId = teamId,
                    HeadcountType = headcountType
                };

                for (int z = 0; z < 7; z++)
                {
                    TimeTrackingDay day = new TimeTrackingDay
                    {
                        TrackedDate = request.Week.StartDateParsed.AddDays(z),
                        Hours = trackingItem.Hours[z]
                    };

                    week.TimeTrackingDays.Add(day);
                }

                item.TimeTrackingWeeks.Add(week);

                headcount.TimeTrackingItems.Add(item);
            }

            return headcount;
        }

        private TimeTrackingItem GetNewTimeTrackingItem(TrackableItemInfo trackingItem, WeekInfo weekInfo, string sender, string modifiedKey, string headcountType)
        {
            TimeTrackingItem newTrackingItem = new TimeTrackingItem
            {
                TrackingType = trackingItem.Type,
                ListItemID = trackingItem.ListItemID,
                CountryID = trackingItem.CountryID,
                ProgramID = trackingItem.ProgramID,
                ProjectID = trackingItem.ProjectID,
                AreaID = trackingItem.AreaID,
                //TeamId = teamId,
                HeadcountType = headcountType
            };

            newTrackingItem.TimeTrackingWeeks = new List<TimeTrackingWeek>();

            TimeTrackingWeek week = new TimeTrackingWeek
            {
                Status = weekInfo.Status,
                StartDate = weekInfo.StartDateParsed,
                EndDate = weekInfo.StartDateParsed.AddDays(6),
                Modified = DateTime.Now,
                ModifiedBy = sender,
                TimeTrackingDays = new List<TimeTrackingDay>(),
                ModifiedKey = modifiedKey,
                ApprovedOn = null,
                //TeamId = teamId,
                HeadcountType = headcountType
            };

            for (int z = 0; z < 7; z++)
            {
                TimeTrackingDay day = new TimeTrackingDay
                {
                    TrackedDate = weekInfo.StartDateParsed.AddDays(z),
                    Hours = trackingItem.Hours[z]
                };

                week.TimeTrackingDays.Add(day);
            }

            newTrackingItem.TimeTrackingWeeks.Add(week);

            return newTrackingItem;
        }

        private TimeTrackingWeek GetNewTimeTrackingWeek(TrackableItemInfo trackingItem, WeekInfo weekInfo, string sender, string modifiedKey, string headcountType)
        {
            TimeTrackingWeek newWeek = new TimeTrackingWeek
            {
                Status = weekInfo.Status,
                StartDate = weekInfo.StartDateParsed,
                EndDate = weekInfo.StartDateParsed.AddDays(6),
                Modified = DateTime.Now,
                ModifiedBy = sender,
                TimeTrackingDays = new List<TimeTrackingDay>(),
                ModifiedKey = modifiedKey,
                ApprovedOn = null,
                //TeamId = teamId,
                HeadcountType = headcountType

            };

            for (int z = 0; z < 7; z++)
            {
                TimeTrackingDay day = new TimeTrackingDay
                {
                    TrackedDate = weekInfo.StartDateParsed.AddDays(z),
                    Hours = trackingItem.Hours[z]
                };

                newWeek.TimeTrackingDays.Add(day);
            }

            return newWeek;
        }

        private TimeTrackingWeek GetUpdatedTimeTrackingWeek(TimeTrackingWeek existingTrackingWeek, TrackableItemInfo trackingItem, WeekInfo weekInfo, string sender, string modifiedKey, string headcountType)
        {
            existingTrackingWeek.Status = weekInfo.Status;
            existingTrackingWeek.Modified = DateTime.Now;
            existingTrackingWeek.ModifiedBy = sender;
            existingTrackingWeek.ModifiedKey = modifiedKey;
            existingTrackingWeek.Comments = null;

            //existingTrackingWeek.TeamId = teamId;
            existingTrackingWeek.HeadcountType = headcountType;

            int index = 0;
            foreach (TimeTrackingDay day in existingTrackingWeek.TimeTrackingDays)
            {
                day.Hours = trackingItem.Hours[index];
                index++;
            }

            return existingTrackingWeek;
        }

        private int GetMonthDifference(DateTime startDate, DateTime endDate)
        {
            int monthsApart = 12 * (startDate.Year - endDate.Year) + startDate.Month - endDate.Month;
            return Math.Abs(monthsApart);
        }

        private HeadcountItem GetHeadcountFromSP(ConfigurationResourceManagementServices configurationData, int HeadcountID, List<string> headcountFields, bool getResourceManagerDetails = false, bool getHeadcountDetails = false, bool getEditorDetails = false)
        {
            string headcountListTitle = configurationData.HeadcountListTitle;

            //get ALL non-INACTIVE from headcount
            using (ClientContext tenantContext = QueryHelper.GetClientContext(configurationData.TenantUrl))
            {
                try
                {
                    string query = string.Empty;

                    query = string.Format(@"
                                <View>
                                    <Query>
                                        <Where>
                                            <Eq>
                                                <FieldRef Name='ID' />
                                                <Value Type='Number'>{0}</Value> 
                                            </Eq> 
                                        </Where>
                                    </Query>
                                    <RowLimit>1</RowLimit>
                                </View>", HeadcountID);

                    ListItemCollection items = QueryHelper.QuerySharepointList(tenantContext, headcountListTitle, query, headcountFields);
                    //LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    //string.Format("GetHeadcountFromSP::Query Sharepoint List Done {0}", HeadcountID));

                    if (items != null)
                    {
                        List<HeadcountItem> headcountItems = new List<HeadcountItem>();

                        foreach (ListItem listItem in items)
                        {
                            HeadcountItem headcountItem = EntityHelper.GetInstance<HeadcountItem>(listItem, headcountFields);

                            if (getResourceManagerDetails)
                            {
                                if (headcountItem.Resource_x0020_Manager != null && !string.IsNullOrEmpty(headcountItem.Resource_x0020_Manager.Email))
                                {
                                    ClientResult<PrincipalInfo> principal = Utility.ResolvePrincipal(tenantContext, tenantContext.Web, headcountItem.Resource_x0020_Manager.Email, PrincipalType.User, PrincipalSource.All, tenantContext.Web.SiteUsers, true);
                                    tenantContext.ExecuteQuery();

                                    if (principal.Value != null)
                                    {
                                        headcountItem.ResourceManagerLoginName = principal.Value.LoginName.Replace(GlobalConstants.LoginNameString, string.Empty);
                                    }
                                    else
                                    {
                                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                                        string.Format("Could not resolve ResourceManager Login Name {0}", headcountItem.Resource_x0020_Manager.Email));
                                    }
                                }
                            }

                            if (getHeadcountDetails)
                            {
                                if (headcountItem.HeadcountAccount != null && !string.IsNullOrEmpty(headcountItem.HeadcountAccount.Email))
                                {
                                    headcountItem.EmailSecondary = headcountItem.HeadcountAccount.Email;
                                    ClientResult<PrincipalInfo> principal = Utility.ResolvePrincipal(tenantContext, tenantContext.Web, headcountItem.EmailSecondary, PrincipalType.User, PrincipalSource.All, tenantContext.Web.SiteUsers, true);
                                    tenantContext.ExecuteQuery();

                                    if (principal.Value != null)
                                    {
                                        headcountItem.LoginName = principal.Value.LoginName.Replace(GlobalConstants.LoginNameString, string.Empty);
                                    }
                                    else
                                    {
                                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                                        string.Format("Could not resolve Headcount Login Name {0}", headcountItem.HeadcountAccount.Email));
                                    }
                                }
                            }

                            if (getEditorDetails)
                            {
                                if (headcountItem.Editor != null && !string.IsNullOrEmpty(headcountItem.Editor.Email))
                                {
                                    ClientResult<PrincipalInfo> principal = Utility.ResolvePrincipal(tenantContext, tenantContext.Web, headcountItem.Editor.Email, PrincipalType.User, PrincipalSource.All, tenantContext.Web.SiteUsers, true);
                                    tenantContext.ExecuteQuery();

                                    if (principal.Value != null)
                                    {
                                        headcountItem.EditorLoginName = principal.Value.LoginName.Replace(GlobalConstants.LoginNameString, string.Empty);
                                    }
                                    else
                                    {
                                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                                        string.Format("Could not resolve Editor Login Name {0}", headcountItem.Editor.Email));
                                    }
                                }
                            }

                            headcountItems.Add(headcountItem);
                        }

                        if (headcountItems.Count > 0)
                        {
                            return headcountItems[0];
                        }
                    }

                }
                catch (Exception ex)
                {
                    //Log
                    throw ex;
                }
            }

            return null;
        }

        private List<HeadcountItem> GetAllHeadcountsFromSP(ConfigurationResourceManagementServices configurationData, KeyValuePair<string, string> filter, string filterType, List<string> headcountFields, bool getResourceManagerDetails, bool getHeadcountDetails)
        {
            string headcountListTitle = configurationData.HeadcountListTitle;

            //get ALL non-INACTIVE from headcount
            using (ClientContext tenantContext = QueryHelper.GetClientContext(configurationData.TenantUrl))
            {
                try
                {
                    string query = string.Empty;

                    query = string.Format(@"
                                <View>
                                    <Query>
                                        <Where>
                                            <Eq>
                                                <FieldRef Name='{0}' />
                                                <Value Type='{1}'>{2}</Value> 
                                            </Eq> 
                                        </Where>
                                    </Query>
                                </View>", filter.Key, filterType, filter.Value);

                    ListItemCollection items = QueryHelper.QuerySharepointList(tenantContext, headcountListTitle, query, headcountFields);

                    if (items != null)
                    {
                        List<HeadcountItem> headcountItems = new List<HeadcountItem>();

                        foreach (ListItem listItem in items)
                        {
                            HeadcountItem headcountItem = EntityHelper.GetInstance<HeadcountItem>(listItem, headcountFields);
                            if (getResourceManagerDetails)
                            {
                                if (headcountItem.Resource_x0020_Manager != null && !string.IsNullOrEmpty(headcountItem.Resource_x0020_Manager.Email))
                                {
                                    ClientResult<PrincipalInfo> principal = Utility.ResolvePrincipal(tenantContext, tenantContext.Web, headcountItem.Resource_x0020_Manager.Email, PrincipalType.User, PrincipalSource.All, tenantContext.Web.SiteUsers, true);
                                    tenantContext.ExecuteQuery();

                                    if (principal.Value != null)
                                    {
                                        headcountItem.ResourceManagerLoginName = principal.Value.LoginName.Replace(GlobalConstants.LoginNameString, string.Empty);
                                    }
                                    else
                                    {
                                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                                        string.Format("Could not resolve ResourceManager Login Name {0}", headcountItem.Resource_x0020_Manager.Email));
                                    }
                                }
                            }

                            if (getHeadcountDetails)
                            {
                                if (headcountItem.HeadcountAccount != null && !string.IsNullOrEmpty(headcountItem.HeadcountAccount.Email))
                                {
                                    headcountItem.EmailSecondary = headcountItem.HeadcountAccount.Email;
                                    ClientResult<PrincipalInfo> principal = Utility.ResolvePrincipal(tenantContext, tenantContext.Web, headcountItem.EmailSecondary, PrincipalType.User, PrincipalSource.All, tenantContext.Web.SiteUsers, true);
                                    tenantContext.ExecuteQuery();

                                    if (principal.Value != null)
                                    {
                                        headcountItem.LoginName = principal.Value.LoginName.Replace(GlobalConstants.LoginNameString, string.Empty);
                                    }
                                    else
                                    {
                                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                                        string.Format("Could not resolve Headcount Login Name {0}", headcountItem.HeadcountAccount.Email));
                                    }
                                }
                            }

                            headcountItems.Add(headcountItem);
                        }

                        return headcountItems;
                    }

                }
                catch (Exception ex)
                {
                    //Log
                    throw ex;
                }
            }

            return null;
        }

        private List<DateTime> CalculatePreviousWeek()
        {
            DateTime today = DateTime.Today;
            DateTime currentWeek = today.AddDays(Convert.ToDouble(today.DayOfWeek) * -1);

            List<DateTime> result = new List<DateTime>();

            //start date
            DateTime previousWeekStart = currentWeek.AddDays(-7); //1 month or so
            result.Add(previousWeekStart);
            //end date
            result.Add(previousWeekStart.AddDays(6));

            return result;
        }

        #endregion

        #region Week Tasks

        public void ProcessWeekTasks(bool calculateMissingWeeks, bool notifyUsers, bool notifyRMs, bool IsActive, int? StartNotifyingFromHeadcountID = null)
        {
            //logging
            LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                string.Format("ProcessWeekTasks running... IsACTIVE:{0} => CalculatingMissingWeeks:{1} -- NotifyingUsers:{2} --  NotifyingRMs:{3} -- StartNotifyingFromHeadcountID : {4}", IsActive, calculateMissingWeeks, notifyUsers, notifyRMs, StartNotifyingFromHeadcountID));

            if (!IsActive)
            {
                LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, "Temporary deactivated.");
            }

            //1. Calculate missing weeks for eligible Headcounts
            if (calculateMissingWeeks && IsActive)
            {
                CalculateMissingWeeks(true);
            }

            //2. Send notifications for headcounts with missing weeks
            if (notifyUsers && IsActive)
            {
                ServiceResponse<TrackTimeMissingWeeksResponse> missingWeeksOperation = GetMissingWeeks(new TrackTimeRequestInfo(), true);
                if (missingWeeksOperation.Status)
                {
                    NotifyUsers(missingWeeksOperation.Response.Headcounts, StartNotifyingFromHeadcountID);
                }
                else
                {
                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Could not get Missing Weeks: {0}", missingWeeksOperation.ErrorMessage));
                }
            }

            //3. Send notifications for RM with weeks pending to approve
            if (notifyRMs && IsActive)
            {
                NotifyRMs();
            }

            //logging
            if (IsActive)
            {
                LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("ProcessWeekTasks running DONE."));
            }
        }

        public void CalculateMissingWeeks(bool processAll, int? HeadcountID = null)
        {
            //logging
            LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, "Calculating missing weeks...");

            ConfigurationResourceManagementServices configurationData = new ConfigurationResourceManagementServices();

            if (configurationData.IsValid())
            {
                //get headcount's contract dates
                List<HeadcountItem> headcounts = new List<HeadcountItem>();

                TimeTrackingRepository repository = this.GetRepository();

                if (processAll)
                {
                    //clear storage for current headcount
                    repository.DeleteAllMissingWeeks();

                    headcounts = GetAllHeadcountsFromSP(configurationData, new KeyValuePair<string, string>("MustReportHours", "1"), "Integer", new List<string> { "ID", "Resource_x0020_Manager", "Hire_x0020_Date", "Finish_x0020_Date", "Resource_x0020_Status" }, true, false);
                }
                else
                {
                    HeadcountItem headcount = GetHeadcountFromSP(configurationData, HeadcountID.Value, new List<string> { "ID", "Hire_x0020_Date", "Finish_x0020_Date", "Resource_x0020_Status" });
                    if (headcount != null)
                    {
                        repository.DeleteMissingWeeks(headcount.ID);
                        headcounts.Add(headcount);
                    }
                }

                if (headcounts.Count > 0)
                {
                    foreach (var headcount in headcounts)
                    {
                        if (!string.IsNullOrEmpty(headcount.Resource_x0020_Status) && headcount.Resource_x0020_Status.ToLower() == "active")
                        {
                            //
                            //calculate DateRange headcount needs to comply with
                            //
                            List<DateTime> previousWeek = CalculatePreviousWeek();
                            //this is a past date, consider the most recent one
                            DateTime initialDate = headcount.Hire_x0020_Date.HasValue && (headcount.Hire_x0020_Date.Value.Date >= configurationData.TimesheetDeployDate.Date) ? headcount.Hire_x0020_Date.Value.Date : configurationData.TimesheetDeployDate.Date;
                            //adjust initial to the inmediate past sunday if it's not sunday already
                            if (initialDate.DayOfWeek != DayOfWeek.Sunday)
                            {
                                initialDate = initialDate.AddDays(Convert.ToDouble(initialDate.DayOfWeek) * -1);
                            }

                            //this has to be also a past date, consider the one that is more distant in the past
                            DateTime endDate = headcount.Finish_x0020_Date.HasValue && (headcount.Finish_x0020_Date.Value.Date < previousWeek[1].Date) ? headcount.Finish_x0020_Date.Value.Date : previousWeek[1].Date;
                            //adjust end date to be a Saturday if it's not already
                            if (endDate.DayOfWeek != DayOfWeek.Saturday)
                            {
                                endDate = endDate.AddDays(6 - Convert.ToDouble(endDate.DayOfWeek)); //6 = Saturday
                            }

                            if (endDate.Date > initialDate.Date)
                            {
                                //get all week's submitted within that range
                                //TimeTrackingRepository repository = this.GetRepository();

                                try
                                {
                                    //call storage
                                    var dbResponse = repository.GetSubmittedWeeks(headcount.ID, initialDate.Date, endDate.Date);

                                    List<HeadcountWeek> missingWeeks = new List<HeadcountWeek>();

                                    while (initialDate.Date < endDate.Date)
                                    {
                                        var _week = dbResponse.Where(w => w.StartDate.Date == initialDate.Date).FirstOrDefault();
                                        if (_week == null)
                                        {
                                            missingWeeks.Add(new HeadcountWeek { HeadcountID = headcount.ID, StartDate = initialDate.Date });
                                        }
                                        initialDate = initialDate.AddDays(7);
                                    }

                                    //clear storage for current headcount
                                    //repository.DeleteMissingWeeks(headcount.ID);

                                    //persist missing weeks
                                    foreach (HeadcountWeek missingWeek in missingWeeks)
                                    {
                                        var _endDate = missingWeek.StartDate.AddDays(6);
                                        TimeTrackingMissingWeek _missingWeek = new TimeTrackingMissingWeek { StartDate = missingWeek.StartDate.Date, EndDate = _endDate };
                                        repository.AddMissingWeek(_missingWeek, headcount.ID);
                                    }

                                    //log
                                    if (missingWeeks.Count > 0)
                                    {
                                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, headcount.ID,
                                            string.Format("Added {0} Missing Weeks for HeadcountID:{1}", missingWeeks.Count, headcount.ID));
                                    }
                                    else
                                    {
                                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, headcount.ID,
                                            string.Format("No Missing Weeks for HeadcountID:{0}", headcount.ID));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, headcount.ID,
                                        string.Format("Headcount ID: {0} Error Message: {1}", headcount.ID, ex.Message));
                                }
                            }
                            else
                            {
                                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, headcount.ID,
                                    string.Format("Headcount ID: {0} EndDate is before InitialDate", headcount.ID));
                            }
                        }
                    }
                }
                else
                {
                    LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, HeadcountID != null && HeadcountID.Value > 0 ? HeadcountID.Value.ToString() : "All",
                        string.Format("SP Headcount not found: {0}", HeadcountID != null && HeadcountID.Value > 0 ? HeadcountID.Value.ToString() : "All"));
                }
            }
            else
            {
                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, HeadcountID != null && HeadcountID.Value > 0 ? HeadcountID.Value.ToString() : "All",
                    string.Format("Configuration data not valid for HeadcountId: {0}", HeadcountID != null && HeadcountID.Value > 0 ? HeadcountID.Value.ToString() : "All"));
            }
        }

        private void NotifyUsers(List<MissingWeekInfo> content, int? StartNotifyingFromHeadcountID = null)
        {
            //Logging
            LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, "Notifying users...");

            int startNotifyingFromHeadcountID = StartNotifyingFromHeadcountID != null && StartNotifyingFromHeadcountID.HasValue ? StartNotifyingFromHeadcountID.Value : 0;

            if (base.ConfigurationData != null)
            {
                try
                {
                    //get all headcounts from SP
                    List<HeadcountItem> headcounts = GetAllHeadcountsFromSP(base.ConfigurationData, new KeyValuePair<string, string>("MustReportHours", "1"), "Integer", new List<string> { "ID", "HeadcountAccount", "FirstName", "Hire_x0020_Date", "Finish_x0020_Date", "EMail" }, false, false);

                    List<List<KeyValuePair<string, object>>> insertionsData = new List<List<KeyValuePair<string, object>>>();
                    string jobID = Guid.NewGuid().ToString();

                    //int emailCounter = 0;
                    foreach (var item in content)
                    {
                        try
                        {
                            if (item.HeadcountID >= startNotifyingFromHeadcountID/* && item.HeadcountID == 331*/)
                            {
                                var headcountItem = headcounts.Where(h => h.ID == item.HeadcountID).FirstOrDefault();

                                if (headcountItem != null /*&& (!string.IsNullOrEmpty(headcountItem.EMail) || !string.IsNullOrEmpty(headcountItem.EmailSecondary))*/)
                                {
                                    //build addresses list
                                    //List<string> addresses = new List<string>();

                                    //if (!string.IsNullOrEmpty(headcountItem.EMail))
                                    //{
                                    //    addresses.Add(headcountItem.EMail);
                                    //}
                                    //if (!string.IsNullOrEmpty(headcountItem.EmailSecondary) && (string.IsNullOrEmpty(headcountItem.EMail) || !headcountItem.EmailSecondary.ToLower().Equals(headcountItem.EMail.ToLower())))
                                    //{
                                    //    addresses.Add(headcountItem.EmailSecondary);
                                    //}

                                    string body = BuildEmailMessageMissingWeeks(item, headcountItem, base.ConfigurationData, MissingWeekEmailFormat);

                                    //List<string> bcc = null;
                                    //if (base.ConfigurationData.DebugTimesheetEmails)
                                    //{
                                    //    bcc = new List<string> { base.ConfigurationData.DebugTimesheetEmailAddress };
                                    //}

                                    //dejando de usar la cuenta resourcemanagementcemex.com a partir de Junio 2019
                                    //bool result = base.SendEmail(base.ConfigurationData.MailFromAddress, addresses, bcc, "Timesheet. Missing Periods: ACTION REQUIRED", true, body, headcountItem.ID);

                                    List<KeyValuePair<string, object>> insertData = new List<KeyValuePair<string, object>>();
                                    insertData.Add(new KeyValuePair<string, object>("Title", "Missing Periods"));
                                    //insertData.Add(new KeyValuePair<string, string>("To", headcountItem.HeadcountAccount.LookupId.ToString()));
                                    insertData.Add(new KeyValuePair<string, object>("Subject", "Timesheet. Missing Periods: ACTION REQUIRED"));
                                    insertData.Add(new KeyValuePair<string, object>("Body", body));
                                    insertData.Add(new KeyValuePair<string, object>("IsCreatedByService", "1"));
                                    insertData.Add(new KeyValuePair<string, object>("JobID", jobID));
                                    //insertData.Add(new KeyValuePair<string, string>("Author", "298;#i:0#.f|membership|miguel.zavala@ext.cemex.com"));
                                    //insertData.Add(new KeyValuePair<string, string>("Editor", "298;#i:0#.f|membership|miguel.zavala@ext.cemex.com"));

                                    //to
                                    FieldUserValue[] oLookupValues = new FieldUserValue[1];
                                    FieldUserValue oLookupValue = new FieldUserValue();
                                    oLookupValue.LookupId = headcountItem.HeadcountAccount.LookupId;
                                    oLookupValues[0] = oLookupValue;
                                    insertData.Add(new KeyValuePair<string, object>("To", oLookupValues));

                                    insertionsData.Add(insertData);

                                    //wait
                                    //System.Threading.Thread.Sleep(1000);

                                    //if (result)
                                    //{
                                    //    emailCounter++;
                                    //}
                                }
                            }
                        }
                        catch (Exception exUsr)
                        {
                            LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                            string.Format("Error ocurred when notifying user: HeadID {0}:{1}", item.HeadcountID, exUsr.Message));
                        }
                    }

                    //Se inserta en Lista de Notificaciones
                    if(insertionsData.Count > 0)
                    {
                        SharePointController spController = new SharePointController();

                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Intentando insertar {0} items en Lista de Notificaciones", insertionsData.Count.ToString()));

                        spController.InsertListItems("TimesheetNotifications", insertionsData);
                    }

                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Proceso de inserción en Lista de Notificaciones terminado. Se insertaron {0} items..", insertionsData.Count));

                    string flowUrl = "https://prod-08.westus.logic.azure.com:443/workflows/21bbb6a3f6c248429879e4b97183068f/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=uVxPWJLHNSH5nV2_EBZGTuFh19Xi2v8hkBeUy2y4F00";
                    Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
                    //requestHeaders.Add("Content-Length", "0");
                    List<int> validResponseCodes = new List<int> { 200, 202 };
                    string requestBody = "{ 'jobID': '" + jobID + "', 'jobType':'Headcounts' }";

                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Intentando llamada a Flow <Timesheet email notifications>.. JobID: {0}", jobID));

                    ExternalServiceResponse<ParsedHttpResponse> actionRequest = HttpHelper.DoRequest(flowUrl, "POST", requestHeaders, null,null, requestBody, validResponseCodes);

                    if (actionRequest.Success)
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Flow <Timesheet email notifications> respondio exitosamente.", string.Empty));
                    }
                    else
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Flow <Timesheet email notifications> respondio con errores: <{0}>", actionRequest.Exception.Message));
                    }

                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Notification process done.{0}", string.Empty));
                }
                catch (Exception ex)
                {
                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Error ocurred when notifying users: {0}", ex.Message));
                }
            }
        }

        private void NotifyRMs()
        {
            //Logging
            LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, "Notifying RMs...");

            if (base.ConfigurationData != null)
            {
                TrackTimeRequestInfo requestInfo = new TrackTimeRequestInfo { StatusID = new int[] { 3 }, IncludeManager = true }; //Pending for Approve
                var response = GetWeeks(requestInfo);

                if (response.Status)
                {
                    //get all headcounts from SP
                    List<HeadcountItem> headcounts = GetAllHeadcountsFromSP(base.ConfigurationData, new KeyValuePair<string, string>("MustReportHours", "1"), "Integer", new List<string> { "ID", "HeadcountAccount", "FirstName", "Hire_x0020_Date", "Finish_x0020_Date", "EMail", "Resource_x0020_Manager", "Full_x0020_Name" }, true, true);
                    List<HeadcountItem> managers = GetAllHeadcountsFromSP(base.ConfigurationData, new KeyValuePair<string, string>("MustReceiveNotifications", "1"), "Integer", new List<string> { "ID", "HeadcountAccount", "FirstName", "Hire_x0020_Date", "Finish_x0020_Date", "EMail", "Resource_x0020_Manager", "Full_x0020_Name" }, true, true);

                    //test
                    //List<HeadcountItem> headcounts = GetAllHeadcountsFromSP(base.ConfigurationData, new KeyValuePair<string, string>("ID", "331"), "Integer", new List<string> { "ID", "HeadcountAccount", "FirstName", "Hire_x0020_Date", "Finish_x0020_Date", "EMail", "Resource_x0020_Manager", "Full_x0020_Name" }, true, true);
                    //List<HeadcountItem> managers = GetAllHeadcountsFromSP(base.ConfigurationData, new KeyValuePair<string, string>("ID", "331"), "Integer", new List<string> { "ID", "HeadcountAccount", "FirstName", "Hire_x0020_Date", "Finish_x0020_Date", "EMail", "Resource_x0020_Manager", "Full_x0020_Name" }, true, true);

                    List<List<KeyValuePair<string, object>>> insertionsData = new List<List<KeyValuePair<string, object>>>();
                    string jobID = Guid.NewGuid().ToString();

                    //int emailCounter = 0;

                    //group by manager
                    var managerItems = response.Response.Weeks.GroupBy(w => w.Manager);
                    foreach (var managerItem in managerItems)
                    {
                        try
                        {
                            //find manager in list items 
                            var managerHeadcount = managers.Where(m => m.LoginName != null && m.LoginName.ToLower().Equals(managerItem.Key.ToLower())).FirstOrDefault();
                            if (managerHeadcount != null/*&& managerHeadcount.Resource_x0020_Manager.LookupId == 298 && (!string.IsNullOrEmpty(managerHeadcount.EMail) || !string.IsNullOrEmpty(managerHeadcount.EmailSecondary))*/)
                            {
                                var managerWeeks = managerItem.ToList();

                                ////build addresses list
                                //List<string> addresses = new List<string>();

                                //if (!string.IsNullOrEmpty(managerHeadcount.EMail))
                                //{
                                //    addresses.Add(managerHeadcount.EMail);
                                //}
                                //if (!string.IsNullOrEmpty(managerHeadcount.EmailSecondary) && !managerHeadcount.EmailSecondary.ToLower().Equals(managerHeadcount.EMail.ToLower()))
                                //{
                                //    addresses.Add(managerHeadcount.EmailSecondary);
                                //}

                                string body = BuildEmailMessagePendingWeeks(managerWeeks, managerHeadcount, headcounts, base.ConfigurationData, PendingApprovalEmailFormat);

                                if (body != null)
                                {
                                    //List<string> bcc = null;
                                    //if (base.ConfigurationData.DebugTimesheetEmails)
                                    //{
                                    //    bcc = new List<string> { base.ConfigurationData.DebugTimesheetEmailAddress };
                                    //}

                                    //bool result = base.SendEmail(base.ConfigurationData.MailFromAddress, addresses, bcc, "Timesheet. Periods pending approval: ACTION REQUIRED", true, body, managerHeadcount.ID);

                                    //if (result)
                                    //{
                                    //    emailCounter++;
                                    //}

                                    //dejando de usar la cuenta resourcemanagementcemex.com a partir de Junio 2019
                                    List<KeyValuePair<string, object>> insertData = new List<KeyValuePair<string, object>>();
                                    insertData.Add(new KeyValuePair<string, object>("Title", "Pending Approval"));
                                    //insertData.Add(new KeyValuePair<string, string>("To", headcountItem.HeadcountAccount.LookupId.ToString()));
                                    insertData.Add(new KeyValuePair<string, object>("Subject", "Timesheet. Periods pending approval: ACTION REQUIRED"));
                                    insertData.Add(new KeyValuePair<string, object>("Body", body));
                                    insertData.Add(new KeyValuePair<string, object>("IsCreatedByService", "1"));
                                    insertData.Add(new KeyValuePair<string, object>("JobID", jobID));
                                    //insertData.Add(new KeyValuePair<string, string>("Author", "298;#i:0#.f|membership|miguel.zavala@ext.cemex.com"));
                                    //insertData.Add(new KeyValuePair<string, string>("Editor", "298;#i:0#.f|membership|miguel.zavala@ext.cemex.com"));

                                    //to
                                    FieldUserValue[] oLookupValues = new FieldUserValue[1];
                                    FieldUserValue oLookupValue = new FieldUserValue();
                                    oLookupValue.LookupId = managerHeadcount.HeadcountAccount.LookupId;
                                    oLookupValues[0] = oLookupValue;
                                    insertData.Add(new KeyValuePair<string, object>("To", oLookupValues));

                                    insertionsData.Add(insertData);
                                }
                                else
                                {
                                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, managerHeadcount.ID,
                                    string.Format("Manager has not periods pending to approve: HID => {0} ", managerHeadcount.ID));
                                }
                            }
                        }
                        catch (Exception hEx)
                        {
                            LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                            string.Format("Error ocurred when notifying RM: {0}", hEx.Message));
                        }
                    }

                    //Se inserta en Lista de Notificaciones
                    if (insertionsData.Count > 0)
                    {
                        SharePointController spController = new SharePointController();

                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Intentando insertar {0} items en Lista de Notificaciones", insertionsData.Count.ToString()));

                        spController.InsertListItems("TimesheetNotifications", insertionsData);
                    }

                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Proceso de inserción en Lista de Notificaciones terminado. Se insertaron {0} items..", insertionsData.Count));

                    string flowUrl = "https://prod-08.westus.logic.azure.com:443/workflows/21bbb6a3f6c248429879e4b97183068f/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=uVxPWJLHNSH5nV2_EBZGTuFh19Xi2v8hkBeUy2y4F00";
                    Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
                    //requestHeaders.Add("Content-Length", "0");
                    List<int> validResponseCodes = new List<int> { 200, 202 };
                    string requestBody = "{ 'jobID': '" + jobID + "', 'jobType':'Managers' }";

                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Intentando llamada a Flow <Timesheet email notifications>.. JobID: {0}", jobID));

                    ExternalServiceResponse<ParsedHttpResponse> actionRequest = HttpHelper.DoRequest(flowUrl, "POST", requestHeaders, null, null, requestBody, validResponseCodes);

                    if (actionRequest.Success)
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Flow <Timesheet email notifications> respondio exitosamente.", string.Empty));
                    }
                    else
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Flow <Timesheet email notifications> respondio con errores: <{0}>", actionRequest.Exception.Message));
                    }

                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Notification process RMs done.{0}", string.Empty));
                }
                else
                {
                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null, response.ErrorMessage);
                }
            }
        }

        private void SendRejectNotification(int HeadcountID, DateTime StartDate, string Comments)
        {
            //get headcount email
            if (base.ConfigurationData != null)
            {
                //get headcount
                List<string> allFields = new List<string> { "ID", "HeadcountAccount", "FirstName", "Hire_x0020_Date", "Finish_x0020_Date", "EMail" };
                HeadcountItem headcountItem = GetHeadcountFromSP(base.ConfigurationData, HeadcountID, allFields);

                List<List<KeyValuePair<string, object>>> insertionsData = new List<List<KeyValuePair<string, object>>>();
                string jobID = Guid.NewGuid().ToString();

                if (headcountItem != null && (!string.IsNullOrEmpty(headcountItem.EMail) || !string.IsNullOrEmpty(headcountItem.EmailSecondary)))
                {
                    string body = BuildEmailMessageRejection(headcountItem, base.ConfigurationData, StartDate, Comments, RejectionEmailFormat);

                    if (body != null)
                    {
                        //dejando de usar la cuenta resourcemanagementcemex.com a partir de Junio 2019
                        List<KeyValuePair<string, object>> insertData = new List<KeyValuePair<string, object>>();
                        insertData.Add(new KeyValuePair<string, object>("Title", "Rejected Week"));
                        insertData.Add(new KeyValuePair<string, object>("Subject", "Timesheet. Period Rejected: ACTION REQUIRED"));
                        insertData.Add(new KeyValuePair<string, object>("Body", body));
                        insertData.Add(new KeyValuePair<string, object>("IsCreatedByService", "1"));
                        insertData.Add(new KeyValuePair<string, object>("JobID", jobID));

                        //to
                        FieldUserValue[] oLookupValues = new FieldUserValue[1];
                        FieldUserValue oLookupValue = new FieldUserValue();
                        oLookupValue.LookupId = headcountItem.HeadcountAccount.LookupId;
                        oLookupValues[0] = oLookupValue;
                        insertData.Add(new KeyValuePair<string, object>("To", oLookupValues));

                        insertionsData.Add(insertData);
                    }
                    else
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, headcountItem.ID,
                        string.Format("Headcount has not rejection body: HID => {0} ", headcountItem.ID));
                    }

                    //Se inserta en Lista de Notificaciones
                    if (insertionsData.Count > 0)
                    {
                        SharePointController spController = new SharePointController();

                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Intentando insertar {0} items en Lista de Notificaciones", insertionsData.Count.ToString()));

                        spController.InsertListItems("TimesheetNotifications", insertionsData);
                    }

                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Proceso de inserción en Lista de Notificaciones terminado. Se insertaron {0} items..", insertionsData.Count));

                    string flowUrl = "https://prod-08.westus.logic.azure.com:443/workflows/21bbb6a3f6c248429879e4b97183068f/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=uVxPWJLHNSH5nV2_EBZGTuFh19Xi2v8hkBeUy2y4F00";
                    Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
                    List<int> validResponseCodes = new List<int> { 200, 202 };
                    string requestBody = "{ 'jobID': '" + jobID + "', 'jobType':'RejectWeek' }";

                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Intentando llamada a Flow <Timesheet email notifications>.. JobID: {0}", jobID));

                    ExternalServiceResponse<ParsedHttpResponse> actionRequest = HttpHelper.DoRequest(flowUrl, "POST", requestHeaders, null, null, requestBody, validResponseCodes);

                    if (actionRequest.Success)
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Flow <Timesheet email notifications> respondio exitosamente.", string.Empty));
                    }
                    else
                    {
                        LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Flow <Timesheet email notifications> respondio con errores: <{0}>", actionRequest.Exception.Message));
                    }

                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Notification Rejected Week done.{0}", string.Empty));
                }
                else
                {
                    LogController.LogMessage(GlobalConstants.TimesheetServiceID, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                        string.Format("Headcount ID or Email not found... {0}", HeadcountID));
                }
            }
        }

        private string BuildEmailMessageMissingWeeks(MissingWeekInfo info, HeadcountItem headcount, ConfigurationResourceManagementServices configurationData, string emailFormat)
        {
            string to = !string.IsNullOrEmpty(headcount.FirstName) ? "Dear " + headcount.FirstName : "Hello";
            string noWeeks = info.Weeks.Count > 1 ? "them" : "it";
            string plural = info.Weeks.Count > 1 ? "s" : "";

            StringBuilder body = new StringBuilder();
            foreach (var week in info.Weeks)
            {
                DateTime startDate = new DateTime(week[0], week[1] + 1, week[2]);
                DateTime endDate = startDate.AddDays(6);
                body.Append(string.Format(MissingIndividualWeekFormat, configurationData.TimesheetNewUrl, startDate.ToString("MM-dd-yyyy"), startDate.ToString("MMMM d, yyyy"), endDate.ToString("MMMM d, yyyy")));
            }

            return string.Format(emailFormat, to, plural, body.ToString(), noWeeks);
        }

        private string BuildEmailMessagePendingWeeks(List<WeekBasicInfo> info, HeadcountItem managerHeadcount, List<HeadcountItem> headcounts, ConfigurationResourceManagementServices configurationData, string emailFormat)
        {
            string to = !string.IsNullOrEmpty(managerHeadcount.FirstName) ? "Dear " + managerHeadcount.FirstName : "Hello";
            bool atLeastOnePeriodListed = false;

            StringBuilder body = new StringBuilder();
            foreach (var week in info)
            {
                var headcountItem = headcounts.Where(h => h.ID == week.HeadcountID).FirstOrDefault();
                if (headcountItem != null)
                {
                    DateTime startDate = new DateTime(week.StartDateDown[0], week.StartDateDown[1], week.StartDateDown[2]);
                    DateTime endDate = startDate.AddDays(6);
                    body.Append(string.Format(PendingIndividualWeekFormat, headcountItem.Full_x0020_Name, startDate.ToString("MMMM d, yyyy"), endDate.ToString("MMMM d, yyyy")));
                    atLeastOnePeriodListed = true;
                }
                else
                {
                    //if headcount is not found, it means it not required to Report hours, therefore even if it has missing weeks calculated (calculation was done when the headcount was
                    //required to report hours) it won't be included, for email purposes, in the listed pending periods
                    //body.Append(string.Format(PendingIndividualWeekFormat, string.Empty, startDate.ToString("MMMM d, yyyy"), endDate.ToString("MMMM d, yyyy")));
                }
            }

            if (atLeastOnePeriodListed)
            {
                return string.Format(emailFormat, to, body.ToString(), configurationData.TimesheetApproveUrl);
            }

            return null;
        }

        private string BuildEmailMessageRejection(HeadcountItem headcount, ConfigurationResourceManagementServices configurationData, DateTime StartDate, string Comments, string emailFormat)
        {
            string to = !string.IsNullOrEmpty(headcount.FirstName) ? "Dear " + headcount.FirstName : "Hello";
            DateTime endDate = StartDate.AddDays(6);

            StringBuilder body = new StringBuilder();
            body.Append(string.Format(emailFormat, to, configurationData.TimesheetNewUrl, StartDate.ToString("MM-dd-yyyy"), StartDate.ToString("MMMM d, yyyy"), endDate.ToString("MMMM d, yyyy"), Comments));

            return body.ToString();
        }

        public ServiceResponse<bool> SetOnlyWeekStatus(TrackTimeApproveRequest request)
        {
            TimeTrackingRepository repository = this.GetRepository();
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = true };

            if (request.Weeks != null && request.Weeks.Count > 0)
            {
                List<int> headcountIds = new List<int>();
                int weeksProcessed = 0;

                foreach (var weekData in request.Weeks)
                {
                    try
                    {
                        if (weekData.Length == 4)
                        {
                            DateTime startDate = new DateTime(weekData[0], weekData[1], weekData[2]);
                            //get week from DB
                            ServiceResponse<TrackTimeWeeksResponse> weekFromDbResponse = this.GetWeek(new TrackTimeRequestInfo
                            {
                                HeadcountID = weekData[3],
                                StartDateUp = new int[] { weekData[0], weekData[1], weekData[2] }
                            });
                            if (weekFromDbResponse.Status && weekFromDbResponse.Response.Weeks != null && weekFromDbResponse.Response.Weeks.Count == 1)
                            {
                                repository.SetOnlyWeekStatus(weekData[3], startDate, (int)request.Status);

                                headcountIds.Add(weekData[3]);
                                weeksProcessed++;
                            }
                            else
                            {
                                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Inconsistent Data: More than 1 week or week not submitted: Approver: {0}, Headcount: {1}, Week: {2}", request.Approver, weekData[3], startDate.ToString()));
                            }
                        }
                        else
                        {
                            LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Week with invalid data: {0}", request.Approver));
                        }
                    }
                    catch (Exception ex)
                    {
                        response.Status = false;
                        LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, ex, true);
                    }
                }

                LogController.LogMessage(this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null,
                    string.Format("Weeks processded: {0} ", weeksProcessed.ToString()));

                //if (request.Weeks.Count != weeksProcessed)
                //{
                //    response.Status = false;
                //    response.ErrorMessage = "It seems there is at least one timesheet(s) which data does not make sense. Please report it.";
                //}

                //fire Queue Item (as of Fev25,2020 inactivated)
                //var distinctHeadcounts = headcountIds.ToArray().Distinct();
                //foreach (int headcountId in distinctHeadcounts)
                //{
                //    //fire async task
                //    HostingEnvironment.QueueBackgroundWorkItem(ct => CalculateMissingWeeks(false, headcountId));
                //}
            }

            return response;
        }

        public void TestHttp()
        {
            string flowUrl = "https://prod-08.westus.logic.azure.com:443/workflows/21bbb6a3f6c248429879e4b97183068f/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=uVxPWJLHNSH5nV2_EBZGTuFh19Xi2v8hkBeUy2y4F00";
            Dictionary<string, string> requestHeaders = new Dictionary<string, string>();
            requestHeaders.Add("Content-Length", "0");

            ExternalServiceResponse<ParsedHttpResponse> actionRequest = HttpHelper.DoRequest(flowUrl, "POST", requestHeaders);
        }

    #endregion


    #region testing

    public ServiceResponse<bool> GetSharePointData()
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false };

            if (base.ConfigurationData != null)
            {
                try
                {
                    using (ClientContext tenantContext = QueryHelper.GetClientContext(base.ConfigurationData.TenantUrl))
                    {
                        try
                        {
                            string query = string.Empty;

                            query = string.Format(@"
                                <View>
                                    <Query>
                                        <Where>
                                            <Eq>
                                                <FieldRef Name='Resource_x0020_Status' />
                                                <Value Type='Choice'>Active</Value> 
                                            </Eq> 
                                        </Where>
                                    </Query>
                                    <RowLimit>{0}</RowLimit>
                                </View>", 10);

                            ListItemCollection items = QueryHelper.QuerySharepointList(tenantContext, base.ConfigurationData.HeadcountListTitle, query, new List<string> { "ID", "Contract_x0020_Type", "Employee_x0020_Type", "Financial_x0020_Type", "Headcount_x0020_Type", "Resource_x0020_Status" });

                            response.Status = true;
                            response.Response = true;
                        }
                        catch (Exception ex)
                        {
                            //Log
                            throw ex;
                        }
                    }
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                }
            }
            else
            {
                response.ErrorMessage = "Configuration Invalid";
            }

            return response;
        }

        public ServiceResponse<double> DoWork()
        {
            ServiceResponse<double> response = new ServiceResponse<double> { Status = false };

            if (base.ConfigurationData != null)
            {
                try
                {
                    double limit = 90000000;
                    double a = 0;
                    double start = DateTime.Now.Millisecond;
                    for (double z = 0; z < limit; z++)
                    {
                        var y = z + 56 * 89 / 48;
                        a = Math.Sqrt(y);
                        var d = Math.Pow(a, 8);
                    }

                    response.Status = true;
                    response.Response = a * start;
                }
                catch (Exception ex)
                {
                    response.ErrorMessage = ex.Message;
                }
            }
            else
            {
                response.ErrorMessage = "Configuration Invalid";
            }

            return response;
        }

        #endregion

    }
}