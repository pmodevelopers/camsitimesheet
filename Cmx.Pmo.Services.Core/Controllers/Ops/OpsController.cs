﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Constants;
using Cmx.Pmo.Services.Core.Enums;
using Cmx.Pmo.Services.Core.Model;
using Cmx.Pmo.Services.Core.Model.Timesheet;
using Cmx.Pmo.Services.Core.Util;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Controllers.Ops
{
    public class OpsController : BaseController
    {
        public new ControllerID ControllerId = ControllerID.Operations;

        public ServiceResponse<string[]> GetVendorsByLoginName(string loginName)
        {
            //logging
            LogController.LogMessage((int)this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Calculating HireVendors for: {0}", loginName));

            ServiceResponse<string[]> response = new ServiceResponse<string[]> { Status = false, ErrorMessage = "No permissions data found" };

            ConfigurationResourceManagementServices configurationData = new ConfigurationResourceManagementServices();

            if (configurationData.IsValid())
            {
                List<RequestToHireVendorItem> items = GetRequestToHireVendorsFromSP(configurationData, new List<string> { "ID", "Title", "Vendor_x0020_Security_x0020_Grou" });

                if (items != null)
                {
                    //get distinct groups
                    var groups = (items.Where(y => !string.IsNullOrEmpty(y.VendorGroup)).Select(x => x.VendorGroup)).Distinct().ToArray();

                    //get user
                    User user = QueryHelper.GetUserByLoginName(configurationData.TenantUrl, loginName, true);

                    if (user != null && user.Groups != null)
                    {
                        List<string> resultGroups = new List<string>();

                        foreach (var userGroup in user.Groups)
                        {
                            var vendorGrp = groups.Where(g => g.ToLower().Equals(userGroup.Title.ToLower())).FirstOrDefault();

                            if (vendorGrp != null)
                            {
                                resultGroups.Add(vendorGrp);
                            }
                        }

                        List<string> resultVendors = new List<string>();
                        foreach (var resultGroup in resultGroups)
                        {
                            foreach (var vendorItem in items)
                            {
                                if (!string.IsNullOrEmpty(vendorItem.VendorGroup) && vendorItem.VendorGroup.Equals(resultGroup))
                                {
                                    resultVendors.Add(vendorItem.Title);
                                }                           
                            }
                        }

                        response.Status = true;
                        response.ErrorMessage = string.Empty;

                        if (resultVendors.Count > 0)
                        {
                            response.Response = (resultVendors.Where(y => !string.IsNullOrEmpty(y)).Select(x => x)).Distinct().ToArray();
                        }

                        LogController.LogMessage((int)this.ControllerId, System.Reflection.MethodBase.GetCurrentMethod().Name, null, string.Format("Resulting HireVendors for: {0} => {1}", loginName, String.Join(",", resultGroups)));
                    }
                }
            }

            return response;
        }

        private List<RequestToHireVendorItem> GetRequestToHireVendorsFromSP(ConfigurationResourceManagementServices configurationData, List<string> fields)
        {
            using (ClientContext tenantContext = QueryHelper.GetClientContext(configurationData.TenantUrl))
            {
                try
                {
                    string query = string.Empty;

                    query = string.Format(@"
                                <View>
                                    <Query>{0}
                                    </Query>
                                </View>", string.Empty);

                    ListItemCollection items = QueryHelper.QuerySharepointList(tenantContext, configurationData.RequestToHireVendorsListTitle, query, fields);

                    if (items != null)
                    {
                        List<RequestToHireVendorItem> entityItems = new List<RequestToHireVendorItem>();

                        foreach (ListItem listItem in items)
                        {
                            RequestToHireVendorItem newItem = EntityHelper.GetInstance<RequestToHireVendorItem>(listItem, fields);

                            if (newItem.Vendor_x0020_Security_x0020_Grou != null && !string.IsNullOrEmpty(newItem.Vendor_x0020_Security_x0020_Grou.LookupValue))
                            {
                                newItem.VendorGroup = newItem.Vendor_x0020_Security_x0020_Grou.LookupValue;
                            }

                            entityItems.Add(newItem);
                        }

                        return entityItems;
                    }

                }
                catch (Exception ex)
                {
                    //Log
                    throw ex;
                }
            }

            return null;
        }

    }
}
