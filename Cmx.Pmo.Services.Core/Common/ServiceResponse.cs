﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Common
{
    public class ServiceResponse<T>
    {
        /// Whether the response is valid
        public bool Status { get; set; }

        /// The data collected from the external service
        public T Response { get; set; }

        /// If ocurred, the related exception
        public Exception Exception { get; set; }

        /// Error Message
        public string ErrorMessage { get; set; }
    }
}
