﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.Services.Core.Constants
{
    public class GlobalConstants
    {
        public const string TenantUrlAppSetting = "TenantUrl";
        public const string ClientIdAppSetting = "ClientId";
        public const string ClientSecretAppSetting = "ClientSecret";
        public const string RealmIdAppSetting = "Realm";

        public const string HeadcountListTitleAppSetting = "HeadcountListTitle";
        public const string RequestsListTitleAppSetting = "RequestsListTitle";
        public const string AllocationsListTitleAppSetting = "AllocationsListTitle";
        public const string SectionsListTitleAppSetting = "SectionsListTitle";
        public const string TimesheetPendingToReportManagementGroupAppSetting = "TimesheetPendingToReportManagementGroup";

        public const string TimesheetDeployDateAppSetting = "TimesheetDeployDate";

        public const string SystemAccount = "System Account";
        public const string LoginNameString = "i:0#.f|membership|";

        public const int TimesheetServiceID = 1;



    }
}
