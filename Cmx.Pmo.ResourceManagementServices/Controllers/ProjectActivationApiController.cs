﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Controllers.Timesheet;
using Cmx.Pmo.Services.Core.Controllers.Ventures;
using Cmx.Pmo.Services.Core.Enums.ProjectActivation;
using Cmx.Pmo.Services.Core.Model.ProjectActivation;
using Cmx.Pmo.Services.Core.Model.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace Cmx.Pmo.Api.Controllers
{
    [RoutePrefix("api")]
    public class ProjectActivationApiController: ApiController
    {
        [Route("ops/splF4531ABC-1BBA-4A40-BFFD-E00857AFA44A")]
        [HttpPost]
        public ServiceResponse<bool> SaveProjectActivationLeader(ProjectActivationSaveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && string.IsNullOrEmpty(requestInfo.ProjectLongID) && string.IsNullOrEmpty(requestInfo.LoginName) && requestInfo.ProjectLeaderUserIdInWeb > 0)
            {
                ProjectActivationController controller = new ProjectActivationController();
                response = controller.SaveProjectActivation(requestInfo, SaveType.ProjectLeader);
            }

            return response;
        }

        [Route("ops/spmD01B0FA5-68C9-4C55-A4B5-012FD08CB97A")]
        [HttpPost]
        public ServiceResponse<bool> SaveProjectActivationManager(ProjectActivationSaveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && string.IsNullOrEmpty(requestInfo.ProjectLongID) && string.IsNullOrEmpty(requestInfo.LoginName) && requestInfo.ProjectManagerUserIdInWeb > 0)
            {
                ProjectActivationController controller = new ProjectActivationController();
                response = controller.SaveProjectActivation(requestInfo, SaveType.ProjectManager);
            }

            return response;
        }

        [Route("ops/spd11E28116-FCD2-4810-98EA-80416004EEC7")]
        [HttpPost]
        public ServiceResponse<bool> SaveProjectActivationDocument(ProjectActivationSaveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && string.IsNullOrEmpty(requestInfo.ProjectLongID) && string.IsNullOrEmpty(requestInfo.LoginName)
                && string.IsNullOrEmpty(requestInfo.DocumentCategory) && string.IsNullOrEmpty(requestInfo.DocumentName) && requestInfo.DocumentContents != null)
            {
                ProjectActivationController controller = new ProjectActivationController();
                response = controller.SaveProjectActivation(requestInfo, SaveType.Document);
            }

            return response;
        }

        [Route("ops/spt3FD1701E-1CD9-43BF-ABCA-20473418DF96")]
        [HttpPost]
        public ServiceResponse<bool> SaveProjectActivationTrack(ProjectActivationSaveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && string.IsNullOrEmpty(requestInfo.ProjectLongID) && string.IsNullOrEmpty(requestInfo.LoginName)
                && requestInfo.TrackItemId > 0 && string.IsNullOrEmpty(requestInfo.TrackName))
            {
                ProjectActivationController controller = new ProjectActivationController();
                response = controller.SaveProjectActivation(requestInfo, SaveType.Track);
            }

            return response;
        }

        [Route("ops/spb59FF502C-4F71-4FA5-920D-0456811356FC")]
        [HttpPost]
        public ServiceResponse<bool> SaveProjectActivationBudgetItem(ProjectActivationSaveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && string.IsNullOrEmpty(requestInfo.ProjectLongID) && string.IsNullOrEmpty(requestInfo.LoginName)
                && string.IsNullOrEmpty(requestInfo.BudgetItemType) && string.IsNullOrEmpty(requestInfo.BudgetItemName) && requestInfo.BudgetYear > 0 && requestInfo.BudgetMonth > -1)
            {
                ProjectActivationController controller = new ProjectActivationController();
                response = controller.SaveProjectActivation(requestInfo, SaveType.BudgetItemYear);
            }

            return response;
        }

    }

}