﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Controllers.Timesheet;
using Cmx.Pmo.Services.Core.Model.Timesheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;

namespace Cmx.Pmo.Timesheet.Controllers
{
    [RoutePrefix("api")]
    public class TimesheetApiController: ApiController
    {
        [Route("timesheet/4AA26556-76C4-4CD9-8FDA-DBE1F33685E8")]
        [HttpPost]
        public ServiceResponse<bool> SaveWeek(TrackTimeSaveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.SaveWeek(requestInfo);
            }

            return response;
        }

        [Route("timesheet/B022854C-818B-4594-907A-B5A265E4882A")]
        [HttpPost]
        public ServiceResponse<bool> SubmitWeek(TrackTimeSaveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.SubmitWeek(requestInfo);
            }

            return response;
        }

        [Route("timesheet/EBDE356E-B681-46DF-B5D7-94F86DDC7EE2")]
        [HttpPost]
        public ServiceResponse<TrackTimeWeeksResponse> GetWeek(TrackTimeRequestInfo requestInfo)
        {
            ServiceResponse<TrackTimeWeeksResponse> response = new ServiceResponse<TrackTimeWeeksResponse> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && requestInfo.HeadcountID > 0 && requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.GetWeek(requestInfo);
            }

            return response;
        }

        [Route("timesheet/CC4FDC8C-5F49-47D6-86FE-EFA7CC599E9B")]
        [HttpPost]
        public ServiceResponse<TrackTimeWeeksBasicResponse> GetWeeks(TrackTimeRequestInfo requestInfo)
        {
            ServiceResponse<TrackTimeWeeksBasicResponse> response = new ServiceResponse<TrackTimeWeeksBasicResponse> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3 && requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.GetWeeks(requestInfo);
            }

            return response;
        }

        //For compatibility as of Mar16-2020 -- delete ASAP
        [Route("timesheet/31CCBF8A-1F2E-4005-918A-0DCB1F8F9FAC")]
        [HttpPost]
        public ServiceResponse<TrackTimeWeeksBasicResponse> GetWeeksPendingToApprove(TrackTimeRequestInfo requestInfo)
        {
            ServiceResponse<TrackTimeWeeksBasicResponse> response = new ServiceResponse<TrackTimeWeeksBasicResponse> { Status = false, ErrorMessage = "Bad request" };
            //if (requestInfo != null && requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3 && requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3 && !string.IsNullOrEmpty(requestInfo.Manager))
            if (requestInfo != null && !string.IsNullOrEmpty(requestInfo.Manager))
            {
                requestInfo.HeadcountID = 0;
                requestInfo.StatusID = new int[] { 3 }; //Pending for Approve
                TimesheetController controller = new TimesheetController();
                response = controller.GetWeeks(requestInfo);
            }

            return response;
        }

        [Route("timesheet/24C1F9D3-6888-4853-9235-DB7C78D25D5F")]
        [HttpPost]
        public ServiceResponse<TrackTimeWeeksBasicResponse> GetWeeksPendingToApproveOrApproved(TrackTimeRequestInfo requestInfo)
        {
            ServiceResponse<TrackTimeWeeksBasicResponse> response = new ServiceResponse<TrackTimeWeeksBasicResponse> { Status = false, ErrorMessage = "Bad request" };
            //if (requestInfo != null && requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3 && requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3 && !string.IsNullOrEmpty(requestInfo.Manager))
            if (requestInfo != null && !string.IsNullOrEmpty(requestInfo.Manager) && requestInfo.StatusID != null && requestInfo.StatusID.Length == 1 && (requestInfo.StatusID[0] == 3 || requestInfo.StatusID[0] == 4))
            {
                //requestInfo.HeadcountID = 0;
                //requestInfo.StatusID = new int[] { 3 }; //Pending for Approve
                TimesheetController controller = new TimesheetController();
                response = controller.GetWeeksApproveOrReject(requestInfo);
            }

            return response;
        }

        [Route("timesheet/70241F2D-328A-4848-8773-7C88DFB52E08")]
        [HttpPost]
        public ServiceResponse<TrackTimeWeeksResponse> GetPopulateWeek(TrackTimeRequestInfo requestInfo)
        {
            ServiceResponse<TrackTimeWeeksResponse> response = new ServiceResponse<TrackTimeWeeksResponse> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && requestInfo.HeadcountID > 0 && requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.GetPopulateWeek(requestInfo);
            }

            return response;
        }

        [Route("timesheet/45AD2334-CD6A-4FD1-99C0-0FBC33565908")]
        [HttpPost]
        public ServiceResponse<TrackTimeHoursResponse> GetHours(TrackTimeRequestInfo requestInfo)
        {
            ServiceResponse<TrackTimeHoursResponse> response = new ServiceResponse<TrackTimeHoursResponse> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3 && requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.GetHoursByDatesRange(requestInfo);
            }

            return response;
        }

        [Route("timesheet/A5A84409-B98B-4902-9097-4D8C7E99B42D")]
        [HttpPost]
        public ServiceResponse<TrackTimeExportResponse> GetExportHours(TrackTimeRequestInfo requestInfo)
        {
            ServiceResponse<TrackTimeExportResponse> response = new ServiceResponse<TrackTimeExportResponse> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3 && requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.GetHoursForExport(requestInfo);
            }

            return response;
        }

        [Route("timesheet/546FEED5-5A7A-4F51-98BA-29EAC7F8B8A7")]
        [HttpPost]
        public ServiceResponse<bool> ApproveWeeks(TrackTimeApproveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.ApproveWeeks(requestInfo, true);
            }

            return response;
        }

        [Route("timesheet/9190D145-4217-44A6-8312-085DA1A4F59B")]
        [HttpPost]
        public ServiceResponse<bool> RejectWeeks(TrackTimeApproveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.ApproveWeeks(requestInfo, false);
            }

            return response;
        }

        [Route("timesheet/E51AB817-475A-4BAF-B821-F7F3C408A6D4")]
        [HttpPost]
        public ServiceResponse<TrackTimeMissingWeeksResponse> GetMissingWeeks(TrackTimeRequestInfo requestInfo)
        {
            ServiceResponse<TrackTimeMissingWeeksResponse> response = new ServiceResponse<TrackTimeMissingWeeksResponse> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && !string.IsNullOrEmpty(requestInfo.Manager))
            {
                TimesheetController controller = new TimesheetController();
                response = controller.GetMissingWeeks(requestInfo);
            }

            return response;
        }

        [Route("timesheet/wt87463FDE-6E0E-4C7B-85A1-A12151FAF96D")]
        [HttpPost]
        public void DoWeekTasks(WeekTaskRequestInfo requestInfo)
        {
            if (requestInfo != null)
            {
                TimesheetController controller = new TimesheetController();
                HostingEnvironment.QueueBackgroundWorkItem(ct => controller.ProcessWeekTasks(requestInfo.CalculateMissingWeeks, requestInfo.NotifyUsers, requestInfo.NotifyRMs, requestInfo.IsActive, requestInfo.StartNotifyingFromHeadcountID));                
            }
        }

        [Route("timesheet/phF0944933-1922-4878-AF6A-30662BCA5EC1")]
        [HttpPost]
        public void ProcessHeadcount(TrackTimeRequestInfo requestInfo)
        {
            if (requestInfo != null && requestInfo.HeadcountID > 0)
            {
                TimesheetController controller = new TimesheetController();
                controller.ProcessHeadcount(requestInfo);
            }
        }

        [Route("timesheet/sowe1A26518C-4921-412A-A367-BD73B1596039")]
        [HttpPost]
        public ServiceResponse<bool> SetOnlyWeekStatus(TrackTimeApproveRequest requestInfo)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null)
            {
                TimesheetController controller = new TimesheetController();
                response = controller.SetOnlyWeekStatus(requestInfo);
            }

            return response;
        }

        //[Route("ventures/cti7C53514D-7C0A-464F-8FA4-F7379250A0BD")]
        //[HttpPost]
        //public void CalculateTeamIDs(TrackTimeSaveRequest requestInfo)
        //{
        //    if (requestInfo != null && requestInfo.HeadcountId > 0)
        //    {
        //        TimesheetController controller = new TimesheetController();
        //        HostingEnvironment.QueueBackgroundWorkItem(_controller => controller.CalculateTeamIDs(requestInfo));
        //    }
        //}


        #region Tests

        ////read test
        //[Route("timesheet/9DC63976-DBA8-4BAB-9E83-D02EDE3569BC")]
        //[HttpPost]
        //public ServiceResponse<TrackTimeWeeksBasicResponse> ReadTest(TrackTimeRequestInfo requestInfo)
        //{
        //    ServiceResponse<TrackTimeWeeksBasicResponse> response = new ServiceResponse<TrackTimeWeeksBasicResponse> { Status = false, ErrorMessage = "Bad request" };
        //    if (requestInfo != null && requestInfo.StartDateUp != null && requestInfo.StartDateUp.Length == 3 && requestInfo.EndDateUp != null && requestInfo.EndDateUp.Length == 3)
        //    {
        //        TimesheetController controller = new TimesheetController();
        //        response = controller.GetWeeks(requestInfo);
        //    }

        //    return response;
        //}

        ////write test
        //[Route("timesheet/95110048-AE81-4F9F-B4DB-B72295175ABC")]
        //[HttpPost]
        //public ServiceResponse<bool> WriteTest(TrackTimeSaveRequest requestInfo)
        //{
        //    ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
        //    if (requestInfo != null)
        //    {
        //        TimesheetController controller = new TimesheetController();
        //        response = controller.SaveWeek(requestInfo);
        //    }

        //    return response;
        //}

        ////SharePoint Test
        //[Route("timesheet/sptest")]
        //[HttpPost]
        //public ServiceResponse<bool> SharePointTest()
        //{
        //    ServiceResponse<bool> response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
        //    //if (requestInfo != null)
        //    {
        //        TimesheetController controller = new TimesheetController();
        //        response = controller.GetSharePointData();
        //    }

        //    return response;
        //}

        ////Internal Test
        //[Route("timesheet/cputest")]
        //[HttpPost]
        //public ServiceResponse<double> InternalTest()
        //{
        //    ServiceResponse<double> response = new ServiceResponse<double> { Status = false, ErrorMessage = "Bad request" };
        //    //if (requestInfo != null)
        //    {
        //        TimesheetController controller = new TimesheetController();
        //        response = controller.DoWork();
        //    }

        //    return response;
        //}

        #endregion

    }

}