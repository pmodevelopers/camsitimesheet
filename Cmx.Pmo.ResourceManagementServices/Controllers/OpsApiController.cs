﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Controllers;
using Cmx.Pmo.Services.Core.Controllers.Ops;
using Cmx.Pmo.Services.Core.Model.Ops;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web;

namespace Cmx.Pmo.ResourceManagementServices.Controllers
{
    [RoutePrefix("api")]
    public class OpsApiController : ApiController
    {
        [Route("ops/vg03BFC2E9-F4F0-4663-A1DB-2EF06AE1B561")]
        [HttpPost]
        public ServiceResponse<string[]> GetVendorsByLoginName(OpsRequest requestInfo)
        {
            ServiceResponse<string[]> response = new ServiceResponse<string[]> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null)
            {
                OpsController controller = new OpsController();
                response = controller.GetVendorsByLoginName(requestInfo.LoginName);
            }

            return response;
        }

        //mantis: protect this endpoint
        [Route("ops/lmEFA06B4E-C15C-4968-87C3-19E84C65F722")]
        [HttpPost]
        public void LogMessage(OpsRequest requestInfo)
        {
            if (requestInfo != null && !string.IsNullOrEmpty(requestInfo.Page) && !string.IsNullOrEmpty(requestInfo.Method) && !string.IsNullOrEmpty(requestInfo.Message) && !string.IsNullOrEmpty(requestInfo.LoginName))
            {
                HostingEnvironment.QueueBackgroundWorkItem(ct => LogController.LogMessage(0, requestInfo.Page, requestInfo.Method, string.Format("{0} => {1}", requestInfo.LoginName, requestInfo.Message)));
            }
        }

        [Route("ops/ufDE234022-2EEA-46EE-9F13-730248FEBFFF")]
        [HttpPost]
        public async Task<HttpResponseMessage> UploadFile()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/rms");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
            //    // Read the form data.
                await Request.Content.ReadAsMultipartAsync(provider);

            //    // This illustrates how to get the file names.
            //    foreach (MultipartFileData file in provider.FileData)
            //    {
            //        Trace.WriteLine(file.Headers.ContentDisposition.FileName);
            //        Trace.WriteLine("Server file path: " + file.LocalFileName);
            //    }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

        }

    }
}
