﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Controllers.ResourceManagement;
using Cmx.Pmo.Services.Core.Enums.ResourceManagement;
using Cmx.Pmo.Services.Core.Model.ResourceManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Hosting;
using System.Web.Http;

namespace Cmx.Pmo.ResourceManagementServices.Controllers
{
    [RoutePrefix("api")]
    public class ResourceManagementApiController : ApiController
    {
        [Route("dashboard/getHeadcountCounters")]
        [HttpGet]
        public ServiceResponse<int[]> GetHeadcountCounters()
        {
            DashboardController controller = new DashboardController();
            ServiceResponse<int[]>  response = controller.GetHeadcountCounters();
            return response;
        }

        [Route("dashboard/getProgramCounters")]
        [HttpPost]
        public ServiceResponse<int[]> GetProgramCounters(ResourceRequestInfo resourceRequestInfo)
        {
            ServiceResponse<int[]> response = new ServiceResponse<int[]> { Status = false, ErrorMessage = "Bad request" };
            if (resourceRequestInfo != null && resourceRequestInfo.programName != null)
            {
                DashboardController controller = new DashboardController();
                response = controller.GetProgramCounters(resourceRequestInfo.programName);
            }

            return response;
        }

        [Route("dashboard/getAllocationCounters")]
        [HttpGet]
        public ServiceResponse<AllocationResponse> GetAllocationCounters()
        {
            DashboardController controller = new DashboardController();
            ServiceResponse<AllocationResponse> response = controller.GetAllocationCounters();
            return response;
        }

        [Route("evals/re0AE85E29-AA24-438A-BAD6-33DA19B446F0")]
        [HttpPost]
        public void DoAllocationTask(AllocationTaskRequestInfo requestInfo)
        {
            if (requestInfo != null)
            {
                EvaluationController controller = new EvaluationController();
                HostingEnvironment.QueueBackgroundWorkItem(ct => controller.RunAllocationTasks(requestInfo.ProcessFinishedAllocations, requestInfo.NotifyEvaluators, requestInfo.NotifyAllocationsExpirations));
            }
        }

        [Route("evals/ge9CD1C8C1-B855-4DAE-B2F1-32A065005214")]
        [HttpPost]
        public ServiceResponse<EvaluationsResponse> GetEvaluationsByViewer(ResourceRequestInfo requestInfo)
        {
            var response = new ServiceResponse<EvaluationsResponse> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && !string.IsNullOrEmpty(requestInfo.LoginName))
            {
                EvaluationController controller = new EvaluationController();
                response = controller.GetEvaluations(requestInfo);
            }

            return response;
        }

        [Route("evals/gse5454A649-EB5B-4778-A909-CEEB46A4410E")]
        [HttpPost]
        public ServiceResponse<EvaluationsResponse> GetSubmittedEvaluations(ResourceRequestInfo requestInfo)
        {
            var response = new ServiceResponse<EvaluationsResponse> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && !string.IsNullOrEmpty(requestInfo.LoginName) && requestInfo.HeadcountID > 0)
            {
                EvaluationController controller = new EvaluationController();
                ResourceRequestInfo newRequest = new ResourceRequestInfo { LoginName = requestInfo.LoginName, HeadcountID = requestInfo.HeadcountID, StatusID = new int[] { 2 } /*Submitted*/};
                response = controller.GetEvaluations(newRequest);
            }

            return response;
        }

        [Route("evals/seE81D7C99-9A5F-47F3-980F-F965F1721224")]
        [HttpPost]
        public ServiceResponse<bool> SubmitEvaluation(ResourceRequestInfo requestInfo)
        {
            var response = new ServiceResponse<bool> { Status = false, ErrorMessage = "Bad request" };
            if (requestInfo != null && requestInfo.EvaluationItems != null && requestInfo.EvaluationItems.Length == 8 && !string.IsNullOrEmpty(requestInfo.LoginName) && requestInfo.EvaluationID > 0
                && requestInfo.ListItemID > 0 && requestInfo.HeadcountID > 0)
            {
                EvaluationController controller = new EvaluationController();
                response = controller.SubmitEvaluation(requestInfo);
            }

            return response;
        }
    }
}
