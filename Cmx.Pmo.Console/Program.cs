﻿using Cmx.Pmo.Services.Core.Common;
using Cmx.Pmo.Services.Core.Controllers.ResourceManagement;
using Cmx.Pmo.Services.Core.Model.ResourceManagement;
using Cmx.Pmo.Services.Core.Model.Timesheet;
using Cmx.Pmo.Services.Core.Util.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Cmx.Pmo.ConsoleCmx
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Dashboard

          //  Services.Core.Controllers.ResourceManagement.DashboardController dashboardController = new Services.Core.Controllers.ResourceManagement.DashboardController();

            //ServiceResponse<int[]> x = dashboardController.GetHeadcountCounters();
            //ServiceResponse<int[]> y = controller.GetProgramCounters("CEMEX Go Online Stores");
            //ServiceResponse<AllocationResponse> result = controller.GetAllocationCounters();

            #endregion

            Services.Core.Controllers.Timesheet.TimesheetController timeController = new Services.Core.Controllers.Timesheet.TimesheetController();

            //  Cmx.Pmo.Services.Core.Controllers.Ops.OpsController opsController = new Cmx.Pmo.Services.Core.Controllers.Ops.OpsController();

            //timeController.GetWeek(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { HeadcountID = 393, StartDateUp = new int[] { 2018, 11, 18 }, });
            //timeController.GetPopulateWeek(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { HeadcountID =331, StartDateUp = new[] { 2019,4,14 } });
            //timeController.GetHeadcountHours(new Services.Core.Model.Timesheet.HeadcountInfo { HeadcountID = 302, Year = 2018 });
            //timeController.GetHours(new Services.Core.Model.Timesheet.TrackTimeHoursRequest { Year = 2018, ProjectListItemID = 29 });
            //     timeController.GetHoursByDatesRange(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { StartDateUp = new int[] { 2019, 6, 1 }, EndDateUp = new int[] { 2020, 5, 31 }, ProjectListItemID = new int[] { 3082 } });
            //timeController.GetHoursByDatesRange(new Services.Core.Model.Timesheet.TrackTimeHoursRequest { Year = 2018, StartDateUp = new int[] { 2017, 8, 1 }, EndDateUp = new int[] { 2018, 7, 31 }, HeadcountID = 1 });
            //timeController.GetHoursByDatesRange(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { HeadcountID = 331, StartDateUp = new int[] { 2019, 2, 1 }, EndDateUp = new int[] { 2020, 1, 31 } });
            //timeController.GetHoursByDatesRange(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { ProjectListItemID = new int[] { 3082 }, StartDateUp = new int[] { 2019, 4, 1 }, EndDateUp = new int[] { 2020, 3, 31 } });
            //imeController.GetWeeks(new Services.Core.Model.Timesheet.RequestInfo { StartDateUp = new int[] { 2017, 10, 1 }, EndDateUp = new int[] { 2018, 9, 30 }, Manager = "E-MGZAVALA@ext.cemex.com" });
            //timeController.GetHoursForExport(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { ProjectListItemID = new int[] { 788 }, StartDateUp = new int[] { 2017, 11, 11 }, EndDateUp = new int[] { 2018, 10, 31 } });

            //Pending for Approve
            //var requestInfo = new Services.Core.Model.Timesheet.TrackTimeRequestInfo { Manager = "oscar.robless@ext.cemex.com" };
            var requestInfo = new Services.Core.Model.Timesheet.TrackTimeRequestInfo { StartDateUp = new int[] { 2017, 10, 1 }, EndDateUp = new int[] { 2018, 9, 30 }, Manager = "E-MGZAVALA@ext.cemex.com" };
            // requestInfo.HeadcountID = 0;
            //requestInfo.StatusID = new int[] { 3 };
            //timeController.GetWeeks(requestInfo);

            #region Approve

            //Services.Core.Model.Timesheet.TrackTimeApproveRequest request = new Services.Core.Model.Timesheet.TrackTimeApproveRequest();
            //request.IsRejected = false;
            //request.Approver = "e-mgzavala@ext.cemex.com";
            //request.Weeks = new List<int[]>();
            //int[] week1 = new int[] { 2018, 11, 25, 331 };

            //request.Weeks.Add(week1);

            //timeController.ApproveWeeks(request, true);

            #endregion

            //SQL.Repository.TimeTrackingRepository repo = new SQL.Repository.TimeTrackingRepository();
            //repo.GetTrackingItem(166, 29, 35);

            //DateTime today = DateTime.Today;
            //DateTime currentWeek = today.AddDays(Convert.ToDouble(today.DayOfWeek) * -1);
            //DateTime first = currentWeek.AddDays(7 * 8 * -1);
            //DateTime last = currentWeek.AddDays(7 * 2);

            #region Pending To approve

            //var r1 = timeController.GetMissingWeeks(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { Manager = "oscar.robless@ext.cemex.com" });

            #endregion

            #region Weekly tasks

            //timeController.InitializeHeadcounts();

            //timeController.ProcessWeekTasks(false, false, true, true, 0);
            //timeController.TestHttp();
            //timeController.ProcessHeadcount(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { HeadcountID = 331 });//807 633
            //timeController.InitializeHeadcounts();

            //timeController.CalculateMissingWeeks(false, 433);

            #endregion

            #region mail

            /*Services.Core.Model.ConfigurationResourceManagementServices configurationData = new Services.Core.Model.ConfigurationResourceManagementServices();

            SmtpClient smtpClient = new SmtpClient(configurationData.MailServer, configurationData.MailPort.Value);
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(configurationData.MailUsername, configurationData.MailPassword);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = true;

            try
            {
                //2. set message
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(configurationData.MailFromAddress);
                //mail.To.Add(new MailAddress(headcountItem.Email));
                mail.To.Add(new MailAddress("carlosfrancisco.rocha@cemex.com"));
                mail.Subject = string.Format("Order 498");
                mail.IsBodyHtml = true;
                mail.Body = string.Format(@"<p>Your {0} posted on {1} is due to be reviewed on {2}.</p> <p>Please review and update either the document or the review date to avoid your content being taken down from the Intranet.</p> <p><a href=""{3}"">{3}</a></p>", "1", "2", "3", "4");

                smtpClient.Send(mail);
            }
            catch (Exception mailEx)
            {
            }
            */
            #endregion

            #region MyTimesheets

            Services.Core.Model.Timesheet.TrackTimeRequestInfo requestMyTime = new Services.Core.Model.Timesheet.TrackTimeRequestInfo();
            requestMyTime.HeadcountID = 2817;
            //requestMyTime.AreaID = 12;
            requestMyTime.StartDateUp = new int[] { 2021, 1, 31 };
            //    requestMyTime.EndDateUp = new int[] { 2019, 2, 2 };

            //var result = timeController.GetWeek(requestMyTime);
            //var result = timeController.GetWeekRaw(2817, requestMyTime.StartDateUp);

            #endregion

            #region Ops

            //opsController.GetVendorsByLoginName("E-MGZAVALA@ext.cemex.com");
            //opsController.GetVendorsByLoginName("helenmarlen.cazarezosuna@ext.cemex.com");
            //opsController.GetVendorListForUser("carlosfrancisco.rocha@cemex.com");
            #endregion

            #region Helpers

            //Services.Core.Util.QueryHelper.BuildQuery(new List<SPQueryFilter> {
            //    new SPQueryFilter {  FieldName = "ID", FieldType = SPFieldType.Number, Value = "10", ComparisonType = SPFieldComparisonType.Eq }
            //    , new SPQueryFilter {  FieldName = "EvalCreated", FieldType = SPFieldType.Integer, Value = "1", ComparisonType = SPFieldComparisonType.Eq }
            //    , new SPQueryFilter {  FieldName = "FinishDate", FieldType = SPFieldType.DateTime, Value = "<Today/>",  ComparisonType = SPFieldComparisonType.Lt }
            //}, QueryCondition.And, 5000);

            #endregion

            #region Evaluation

            //EvaluationController evalController = new EvaluationController();

            ////evalController.RunAllocationTasks(true, true, false);

            //ResourceRequestInfo requestEval = new ResourceRequestInfo();
            //requestEval.LoginName = "E-MGzaVALA@ext.cemex.com";
            //requestEval.HeadcountID = 331;
            //requestEval.EvaluationID = 1;
            ////requestEval.AreaID = 12;
            //requestEval.ListItemID = 140;
            //requestEval.EvaluationItems = new int[] { 5, 4, 4, 4, 3, 3, 2, 1 };
            //requestEval.Comments = "none commments";
            ////requestEval.StartDateUp = new int[] { 2018, 12, 10 };
            ////requestEval.EndDateUp = new int[] { 2018, 11, 30 };
            ////requestEval.ProjectListItemID = new int[] { 788 };            

            ////evalController.GetEvaluations(requestEval);
            ////evalController.SubmitEvaluation(requestEval);
            ////evalController.RunEvaluationCreationTask(false, false, true);

            #endregion

            #region Stress

            //timeController.GetSharePointData();

            //timeController.DoWork();

            #endregion

            #region Timesheet Change Status

            //Services.Core.Model.Timesheet.TrackTimeApproveRequest requestMantis = new Services.Core.Model.Timesheet.TrackTimeApproveRequest();
            //requestMantis.Weeks = new List<int[]>();
            //requestMantis.Status = SQL.Enums.Timesheet.WeekStatus.Rejected;

            ////    List<int[]> weeks = new List<int[]> {
            ////    new int[] { 2019, 12, 19 },
            ////    new int[] { 2020, 1, 5 },
            ////    new int[] { 2020, 1, 12 },
            ////    new int[] { 2020, 1, 19 },
            ////    new int[] { 2020, 1, 26 },
            ////    new int[] { 2020, 2, 2 },
            ////    new int[] { 2020, 2, 9 },
            ////    new int[] { 2020, 2, 16 },
            ////    new int[] { 2020, 2, 23}
            ////};

            //List<int[]> weeks = new List<int[]> {
            //    new int[] { 2020, 2, 23},
            //    new int[] { 2020, 3, 1},
            //    new int[] { 2020, 3, 8},
            //    new int[] { 2020, 3, 15},
            //    new int[] { 2020, 3, 22},
            //};

            //int[] headcounts = new int[] { 331 }; //633
            ////int[] headcounts = new int[] { 1387, 878, 620, 886, 975, 783, 1399, 1009, 1326, 932, 867, 1122, 2818, 1366, 1148, 1491, 1012, 572, 1291, 925, 741, 2817, 1050, 1055, 1469, 1200, 1292, 807, 992, 1060, 694, 789, 985, 879, 1052, 624, 3090 }; 

            //foreach (int[] weekInt in weeks)
            //{
            //    foreach (int _headID in headcounts)
            //    {
            //        requestMantis.Weeks.Add(new int[] { weekInt[0], weekInt[1], weekInt[2], _headID });
            //    }
            //}

            ////timeController.SetOnlyWeekStatus(requestMantis);

            //#endregion

            //#region SaveWeek

            TrackTimeSaveRequest requestWeek = new TrackTimeSaveRequest
            {
                HeadcountId = 1195,//331,
                Manager = "mauricio.cantu@ext.cemex.com",//"carlosfrancisco.rocha@cemex.com",
                Sender = "luis.arraizb@ext.cemex.com"// "miguela.zavalar@ext.cemex.com"
            };

            requestWeek.Week = new WeekInfo
            {
                StartDateUp = new int[] { 2021, 10, 3 },
                TrackableItemsInfo = new List<TrackableItemInfo> { new TrackableItemInfo {
                    AreaID = null,
                    ListItemID = 175,
                    Type = SQL.Enums.Timesheet.TrackingType.Service,
                    CountryID = 100,
                    //ProgramID = ,
                    //ProjectID = ,
                    //AllocationID = 4926,
                    Hours = new decimal[] { 1,0,0,0,0,0,0}
                }
                // new TrackableItemInfo {
                //    ListItemID = 1,
                //    Type = SQL.Enums.Timesheet.TrackingType.Special,
                //    CountryID = 8,
                //    ProgramID = 61,
                //    ProjectID = 3040,
                //    Hours = new decimal[] { 0,0,5,0,0,0,0}
                //},
                //  new TrackableItemInfo {
                //    ListItemID = 37,
                //    Type = SQL.Enums.Timesheet.TrackingType.Special,
                //    CountryID = 4,
                //    ProgramID = 25,
                //    ProjectID = 3078,
                //    Hours = new decimal[] { 0,0,0,0,7,0,0}
                //}
                }
            };

            timeController.SaveWeek(requestWeek);

            #endregion

            #region ApproveReject

            //timeController.GetWeeksApproveOrReject(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { Manager = "miguel.zavala@ext.cemex.com", StatusID = new int[] { 4 }, HeadcountsID = new int[] { 329, 315 } });
            //timeController.GetWeeksApproveOrReject(new Services.Core.Model.Timesheet.TrackTimeRequestInfo { Manager = "miguel.zavala@ext.cemex.com", StatusID = new int[] { 3 }});


            #endregion
        }
    }
}
